README v 1.9.1
=================
TABLE OF CONTENTS
=================
1. REQUIREMENTS
2. UPDATES
3. PROJECT SETUP
4. RUNNING THE PROGRAM
    1. CONFIGURATION FILES
    2. COMMANDS
    3. OSM FILES

1. REQUIREMENTS
================
*Java 8
Download from: http://www.oracle.com/technetwork/java/javase/downloads/index.html
Note: Make sure to download the JDK! JREs are known to be quirky.

* git - for version control. It's also the most convenient way to download the source code.
Download from: http://git-scm.com/downloads

* maven - For dependency management and automated builds (In order to have a jar file ready with a simple command)
Download from: http://maven.apache.org/download.cgi

* eclipse (optional)
Download from: https://www.eclipse.org/downloads/
Suggestion: Make sure it is Eclipse 4.4 (Luna) onwards. Earlier versions of eclipse don't have built-in Java 8 support

2. UPDATES
===========
You can get updated versions of this program from:
https://bitbucket.org/droxisla/osm-converter/overview

You can contribute by submitting an issue, or even a bugfix! 
There is also a wiki you can contribute!

You may also contact the author at:
leandro.t.isla@gmail.com
Please preface the subject with OSC:

3. PROJECT SETUP
=================
1) Open the project folder in a terminal. E.g. /home/drox/Documents/workspace/osm-converter
2) mvn clean eclipse:eclipse
	- This will create the eclipse project and download all the dependencies declared in the pom.xml. Make sure to be connected to the internet!
3) A good way of validating that everything works as expected is to look through the tests. They are likely to be in {$PROJECT_HOME}/src/test/java , where $PROJECT_HOME is the name of the project. To run the tests, just right-click the test folder -> run as -> J-Unit tests. There should be a green bar appearing in the left side of the screen, indicating that all tests are successful. If at least one test fails, the bar should be red.
	- Try to get into the habit of writing the tests first before the code. This is a process known as Test Driven Development (TDD). It's very hard to maintain and seemingly a waste of time... until the next time you write a large chunk of code and have to debug it for hours.
4) To create an executable jar, just type:
	mvn package
	- You can find the jar at {$PROJECT_HOME}/target/osm-converter-X.X.X-SNAPSHOT-jar-with-dependencies.jar
	- X.X.X is the version declared in the pom.xml. An example would be 1.9.1

4. RUNNING THE PROGRAM
=======================
The jar file requires several configuration files:

4.1. CONFIGURATION FILES
=========================
1) log4j.properties - contains logging-related information. Currently set to output at out.log
2) compress.txt - contains the tags that will be treated as buildings.
3) used.txt - contains the tags that will be treated as highways.
4) usedIgnore.txt - contains highway tags that will be ignored. This is only used if used.txt contains highway=*

4.2. COMMANDS
==============
All commands must be preceded by java -jar <jarname>.jar , where <jarname> is the name of the jarfile

* -a createAdjacencyList(args);
Creates an adjacency list from nodes and roadlinks. The process is already incorporated into cleaning.
Sample usage:
-a input/nodes.txt input/roadlinks.txt output/adj-list.txt

* -an getANDegree(args);
Unstable. Experimental. Tests are not complete. Intended to retrieve the number of nodes that are adjacent to each node.
Sample usage:
-an input/nodes.txt input/roadlinks.txt
outputs to target/output/an-degrees.txt

* -c listComponentsToFile(args);
Lists The Strongly Connected Components of a given Graph
Sample Usage:
-c input/nodes.txt input/roadlinks.txt output/components.txt

* -con printConnectivity(args);
Prints the connectivity of the graph given the nodes and the roadlinks.
Sample Usage:
-con input/nodes.txt input/roadlinks.txt

* -d toKilometers(args);
Converts the values on roadlinks from coordinate-distance to kilometers. Originally created as a means to convert old roadlink data to kilometers. This process has since been incorporated into the cleaning process.
Sample Usage:
-d input/nodes.txt input/roadlinks.txt
It modifies the input roadlinks file

This series of commands were intended for running experiments on the Graphs.
The repair priority is printed out to output/REPAIR.txt
The results are collated to result.out

* -er random(args);
Primary Parameters:
radius: The radius of the target area
threshold: percent of target map to destroy until disconnected.
id: execution id
Sample Usage:
-er input/nodes.txt input/roadlinks.txt 1 0.05 1

* -erx randomMultiple(args);
Runs -er {x} number of times
Primary Parameters:
radius: The radius of the target area
threshold: percent of target map to destroy until disconnected.
runs: The number of times it will be executed
Sample Usage:
-erx input/nodes.txt input/roadlinks.txt 1 0.05 5

* -erxf randomMultipleFromFile(args);
Reads from file and executes -erx according to the parameters in the file.
Sample Usage:
-erxf input/nodes.txt input/roadlinks.txt input/params.csv

* -erxp randomAllRadii(args)
For a given threshold, it will run through 1,2,5,10 km as radius
Primary Parameters:
threshold: percent of target map to destroy until disconnected.
runs: The number of times it will be executed
Sample Usage:
-erxp input/nodes.txt input/roadlinks.txt 0.05 5

* -erxpa randomAll(args);
Runs the entire suite of experiments. 
Primary parameter:
runs: The number of times it will be executed
Sample Usage:
-erxpa input/nodes.txt input/roadlinks.txt 5

* -i interactiveMode(System.in, args);
Intended to be the backbone of a future GUI. Currently low-priority
Sample Usage:
-i
This will open some sub-options. It also prints out a description of what each suboption does.

* -j formatToJson(args);
Formats the nodes and roadlinks into JSON. Follows specs that were requested.
Sample Usage:
-j input/nodes.txt input/roadlinks.txt input/adj-list.txt output/json.txt
Note that while each line is JSON compliant, the entire file is not. If the file was not broken down to lines, it would become one very long line, which would kill most text editors.

* -kp purgeSmallerComponents(args);
Removes all but the largest Strongly Connected Component. This is required for the experiment. By convention, purged files are appended with "-c"
Sample Usage:
-kp input/nodes.txt input/roadlinks.txt output/nodes-c.txt output/roadlinks-c.txt

* -r repairPriority(args);
Prototype version of -er. Takes a specific list of broken links.
Sample Usage:
-r input/nodes.txt input/roadlinks.txt input/brokenlinks.txt output/repair.txt
Note that the links in brokenlinks.txt must have their nodes declared in nodes.txt

* -s cleanNoBuildings(args);
Extracts only the roads from a map.
Sample Usage:
-s manila.osm output/nodes.txt output/roadlinks.txt output/adj-list.txt

* -str straighten(args);
Unstable. Experimental means of reducing the size of the map. Not tested. No guarantees
Sample Usage:
-str input/nodes.txt input/roadlinks.txt output/nodes.txt output/roadlinks.txt

* no flags 
By default, the program will execute a full processing of the osm file's roads and buildings
clean(args);
Sample Usage:
manila.osm
Will output at
target/output/nodes.txt
target/output/roadlinks.txt
target/output/adj-list.txt

4.3. OSM FILES
===============
You will also require an osm file for cleaning-related actions. You can download osm files from OpenStreetMap.