package osm.cleaner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import osm.Node;
import osm.cleaner.RoadLink.Highway;

public class Road implements Comparable<Road> {
	private static final int NO_PARTNER_FOUND = -1;
	private List<RoadLink> roadLinks;
	private List<Node> nodes;

	public Road(RoadLink roadLink) {
		setUpNodesAndRoadLinks(roadLink);
	}

	public Road(RoadLink roadLink, RoadLink reverseRoadLink) {
		setUpNodesAndRoadLinks(roadLink);
		addPartner(reverseRoadLink);
	}

	private void setUpNodesAndRoadLinks(RoadLink roadLink) {
		nodes = new ArrayList<>(2);
		nodes.add(roadLink.getStartNode());
		nodes.add(roadLink.getEndNode());
		roadLinks = new ArrayList<>(2);
		roadLinks.add(roadLink);
	}

	public static List<Road> toRoads(List<RoadLink> roadLinks) {
		List<Road> roads = new ArrayList<>();
		List<Road> unpairedRoads = new ArrayList<>();
		for (int i = 0; i < roadLinks.size(); i++) {
			RoadLink currentRoadLink = roadLinks.get(i);
			int indexOfPartner = indexOfPartner(currentRoadLink, unpairedRoads);
			if (indexOfPartner == NO_PARTNER_FOUND) {
				unpairedRoads.add(new Road(currentRoadLink));
			} else {
				Road partnerRoad = unpairedRoads.get(indexOfPartner);
				partnerRoad.addPartner(currentRoadLink);
				roads.add(partnerRoad);
				unpairedRoads.remove(partnerRoad);
			}
		}
		roads.addAll(unpairedRoads);
		return roads;
	}

	private void addPartner(RoadLink partner) {
		validate(partner);
		roadLinks.add(partner);
		Collections.sort(roadLinks);
	}

	private void validate(RoadLink partner) {
		StringBuilder exceptionMessage = new StringBuilder();
		exceptionMessage.append(validateNumberOfLinks());
		exceptionMessage.append(validateLink(partner));
		boolean isInvalid = exceptionMessage.length() > 0;
		if (isInvalid) {
			exceptionMessage.append("\nTried to add: ").append(partner);
			throw new RoadException(exceptionMessage.toString());
		}
	}

	private String validateLink(RoadLink partner) {
		StringBuilder exceptionMessage = new StringBuilder(); 
		if (!isPartnerOf(partner)) {
			exceptionMessage.append("Can only add a link from ")
					.append(nodes.get(1)).append(" to ").append(nodes.get(0));
		}
		return exceptionMessage.toString();
	}

	private String validateNumberOfLinks() {
		StringBuilder exceptionMessage = new StringBuilder();
		if (roadHasTwoLinks()) {
			exceptionMessage.append(
					"A Road can only have up to two links. Current links: ")
					.append(roadLinks);
		}
		return exceptionMessage.toString();
	}

	private boolean roadHasTwoLinks() {
		return roadLinks.size() >= 2;
	}

	protected static int indexOfPartner(RoadLink roadLink,
			List<Road> unpairedRoads) {
		for (int i = 0; i < unpairedRoads.size(); i++) {
			if (unpairedRoads.get(i).isPartnerOf(roadLink)) {
				return i;
			}
		}
		return NO_PARTNER_FOUND;
	}

	public boolean isPartnerOf(RoadLink roadLink) {
		return nodes.get(1).equals(roadLink.getStartNode())
				&& nodes.get(0).equals(roadLink.getEndNode());
	}

	public List<RoadLink> getRoadLinks() {
		return roadLinks;
	}

	public BigDecimal getDistance() {
		return roadLinks.get(0).getDistance();
	}

	public Highway getHighwayType() {
		return roadLinks.get(0).getHighwayType();
	}

	@Override
	public String toString() {
		String d = ";";
		StringBuilder builder = new StringBuilder();
		RoadLink roadLink = getRoadLinks().get(0);
		String start = roadLink.getStartNode().getId();
		String end = roadLink.getEndNode().getId();
		String isTwoWay = getRoadLinks().size() == 2 ? "Y" : "N";
		String name = roadLink.getName();
		String distance = roadLink.getDistance().toString();
		String lanes = "" + roadLink.getLanes();
		builder.append(start).append(d).append(end).append(d).append(isTwoWay)
				.append(d).append(name).append(d).append(distance).append(d)
				.append(lanes);
		if (roadLink.getHighwayType() != null) {
			String highwayType = roadLink.getHighwayType().toString();
			builder.append(d).append(highwayType);
		}
		return builder.toString();
	}

	@Override
	public int compareTo(Road o) {
		return roadLinks.get(0).compareTo(o.getRoadLinks().get(0));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((roadLinks.get(0) == null) ? 0 : roadLinks.get(0).hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Road other = (Road) obj;
		if (roadLinks.get(0) == null) {
			if (other.roadLinks.get(0) != null)
				return false;
		} else if (!roadLinks.get(0).equals(other.roadLinks.get(0)))
			return false;
		return true;
	}
}