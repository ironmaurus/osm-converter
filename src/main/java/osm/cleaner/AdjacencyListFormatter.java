package osm.cleaner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import osm.Node;

public class AdjacencyListFormatter {
	public static String formatAdjString(List<Node> nodes, List<RoadLink> roads) {
		List<Node> nodeIds = new ArrayList<>(nodes);
		Collections.sort(nodeIds);
		List<RoadLink> roadLinks = new ArrayList<>(roads);
		Collections.sort(roadLinks);
		StringBuilder builder = new StringBuilder();

		int counter = 0;
		for (int i = 0; i < nodeIds.size(); i++) {
			Node currentNode = nodeIds.get(i);
			builder.append(currentNode.getId()).append(";");
			while (counter < roadLinks.size()
					&& currentNode
							.equals(roadLinks.get(counter).getStartNode())) {
				RoadLink currentRoad = roadLinks.get(counter);
				if (counter == 0
						|| !currentNode.equals(roadLinks.get(counter - 1)
								.getStartNode())) {
					builder.append(currentRoad.getEndNode().getId());
				} else {
					builder.append(",")
							.append(currentRoad.getEndNode().getId());
				}
				counter++;
			}
			if (i != nodeIds.size() - 1) {
				builder.append("\n");
			}
		}
		return builder.toString();
	}
}
