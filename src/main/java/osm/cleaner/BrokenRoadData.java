package osm.cleaner;

import java.util.List;

public class BrokenRoadData {
	private List<Road> brokenRoads;
	private double actualThreshold;
	private double targetThreshold;

	public List<Road> getBrokenRoads() {
		return brokenRoads;
	}

	public void setBrokenRoads(List<Road> brokenRoads) {
		this.brokenRoads = brokenRoads;
	}

	public double getActualThreshold() {
		return actualThreshold;
	}

	public void setActualThreshold(double actualThreshold) {
		this.actualThreshold = actualThreshold;
	}

	public double getTargetThreshold() {
		return targetThreshold;
	}

	public void setTargetThreshold(double targetThreshold) {
		this.targetThreshold = targetThreshold;
	}

}
