package osm.cleaner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import osm.Bounds;
import osm.Nd;
import osm.Node;
import osm.Tag;
import osm.Way;
import osm.cleaner.RoadLink.Highway;
import osm.cleaner.distance.DistanceCalculator;
import osm.cleaner.distance.HalversineDistanceCalculator;

public class DataCleaner {
	private static final String TARGET_OUTPUT_MISSING_BUILDING_NODES_TXT = "target/output/missing-building-nodes.txt";
	private static final String TARGET_OUTPUT_MISSING_NODES_TXT = "target/output/missing-nodes.txt";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DataCleaner.class);
	private static final Tag ONEWAY = new Tag("oneway", "yes");
	private static final String ROADLINKS_TXT = "target/output/roadlinks.txt";
	private static final String NODES_TXT = "target/output/nodes.txt";
	private static final String ADJ_LIST_TXT = "target/output/adj-list.txt";
	private static final double DEFAULT_SIDE = 0.02;
	private List<String> centroidIds;
	private Bounds bounds;
	private double side;
	private Box[][] boxes;
	private DistanceCalculator calc;
	private WaysClassifier waysClassifier;

	public DataCleaner(Bounds bounds) {
		this(bounds, DEFAULT_SIDE);
	}

	public DataCleaner(Bounds bounds, double side) {
		this(bounds, side, new WaysClassifier());
	}

	public DataCleaner(Bounds bounds, WaysClassifier waysClassifier) {
		this(bounds, DEFAULT_SIDE, waysClassifier);
	}

	public DataCleaner(Bounds bounds, double side, WaysClassifier waysClassifier) {
		this.bounds = bounds;
		this.side = side;
		centroidIds = new ArrayList<>();
		this.waysClassifier = waysClassifier;
		calc = new HalversineDistanceCalculator();
	}

	public void clean(List<Way> ways, List<Node> nodes) {
		clean(ways, nodes, ROADLINKS_TXT, NODES_TXT, ADJ_LIST_TXT);
	}

	public void clean(List<Way> ways, List<Node> nodes,
			String roadLinksFilename, String nodesFilename,
			String adjListFilename) {
		clean(ways, nodes, roadLinksFilename, nodesFilename, adjListFilename,
				"");
	}

	public void clean(List<Way> ways, List<Node> nodes,
			String roadLinksFilename, String nodesFilename,
			String adjListFilename, String configFolder) {
		configureUsedTags(configFolder);
		makeDirs("target/output/");
		LOGGER.info("Sorting Nodes");
		Collections.sort(nodes);
		LOGGER.info("Calculating Centroids");
		List<Node> centroids = calculateAllCentroids(ways, nodes);
		LOGGER.info("Compressing Buildings");
		compressBuildings(ways, nodes, centroids);
		LOGGER.info("Extracting RoadLinks");
		List<RoadLink> roadLinks = extractAndBoxRoadLinks(ways, nodes);
		LOGGER.info("Connecting Centroids to nearest Road Link");
		List<RoadLink> buildingLinks = connectCentroidsToNearestLink(roadLinks,
				centroids);
		roadLinks.addAll(buildingLinks);
		LOGGER.info("Purging unused Nodes");
		nodes = purgeIsolatedNodes(nodes, roadLinks);
		LOGGER.info("Writing to Output");
		LOGGER.info("Writing RoadLinks");
		writeRoadLinksToFile(roadLinksFilename, roadLinks);
		LOGGER.info("Writing Nodes");
		writeNodesToFile(nodesFilename, nodes);
		LOGGER.info("Writing Adjacency List");
		writeAdjListToFile(adjListFilename, roadLinks, nodes);
		LOGGER.info("FINISHED");
	}
	
	public List<Node> calculateAllCentroids(List<Way> ways, List<Node> nodes) {
		List<Node> centroids = new ArrayList<>();
		Set<String> missingNodes = new HashSet<>();
		for (Way w : ways) {
			if (isCompress(w) && !w.getName().isEmpty()) {
				LOGGER.info("Processing Way id: " + w.getId());
				List<Node> nodeList = w.getNodeList();
				List<Node> actualNodes = new ArrayList<>();
				for (Node n : nodeList) {
					int index = Collections.binarySearch(nodes, n);
					if (index > -1) {
						Node actualNode = nodes.get(index);
						LOGGER.info("Found Node: {}", actualNode.toString());
						actualNodes.add(actualNode);
					} else {
						LOGGER.warn("Cannot find: {}", n.getId());
						missingNodes.add(n.getId());
					}
				}
				Node centroid = calculateCentroid(actualNodes);
				ArrayList<Tag> tagList = new ArrayList<>();
				tagList.add(new Tag("name", w.getName()));
				centroid.setTagList(tagList);
				centroids.add(centroid);
				LOGGER.info("Centroid: {}", centroid.toString());
				centroidIds.add(centroid.getId());
			}
		}
		writeMissingBuildingNodesToFile(missingNodes);
		return centroids;
	}
	
	public Node calculateCentroid(List<Node> nodes) {
		double sumLat = 0;
		double sumLon = 0;
		for (Node n : nodes) {
			sumLat += Double.parseDouble(n.getLat());
			sumLon += Double.parseDouble(n.getLon());
		}
		sumLat = sumLat / nodes.size();
		int latGridNo = getGridNo(sumLat,
				Double.parseDouble(bounds.getMinlat()),
				Double.parseDouble(bounds.getMaxlat()), side);
		sumLon = sumLon / nodes.size();
		int lonGridNo = getGridNo(sumLon,
				Double.parseDouble(bounds.getMinlon()),
				Double.parseDouble(bounds.getMaxlon()), side);
		String lat = String.format("%.7f", sumLat);
		String lon = String.format("%.7f", sumLon);
		String id = getUniqueId(nodes);
		Node node = new Node(id, lat, lon);
		node.setLatGridNo(latGridNo);
		node.setLonGridNo(lonGridNo);
		return node;
	}
	
	private String getUniqueId(List<Node> nodes) {
		String id = "";
		for (Node n : nodes) {
			id = n.getId();
			if (!centroidIds.contains(id)) {
				break;
			}
		}
		return id;
	}
	
	public List<Way> compressBuildings(List<Way> ways, List<Node> nodes,
			List<Node> centroids) {
		LOGGER.info("Adding Centroids");
		List<Way> buildings = filterBuildings(ways);
		addCentroids(buildings, nodes, centroids);
		LOGGER.info("Sorting Nodes");
		Collections.sort(nodes);
		return ways;
	}

	private List<Way> filterBuildings(List<Way> ways) {
		List<Way> buildings = new ArrayList<>();
		for (Way w : ways) {
			if (isCompress(w)) {
				buildings.add(w);
			}
		}
		return buildings;
	}
	
	public List<RoadLink> extractAndBoxRoadLinks(List<Way> ways,
			List<Node> nodes) {
		List<RoadLink> roadLinks = new ArrayList<>();
		initializeBoxes();
		for (Way w : ways) {
			if (isUsed(w)) {
				extractAndBoxLink(nodes, roadLinks, w);
			}
		}
		return roadLinks;
	}

	private void extractAndBoxLink(List<Node> nodes, List<RoadLink> roadLinks,
			Way currentWay) {
		LOGGER.info("Processing Way:{}", currentWay.toString());
		List<Nd> nds = currentWay.getNdList();
		for (int i = 0; i < nds.size() - 1; i++) {
			String currentId = nds.get(i).getRef();
			String nextId = nds.get(i + 1).getRef();
			Node currentNode = findNode(currentId, nodes);
			Node nextNode = findNode(nextId, nodes);
			if (!currentNode.isEmpty() && !nextNode.isEmpty()) {
				placeNodeInGrid(currentNode);
				placeNodeInGrid(nextNode);
				RoadLink roadLink = createRoadLink(currentWay, currentNode,
						nextNode);
				LOGGER.info("Extracted RoadLink:{}", roadLink.toString());
				roadLinks.add(roadLink);
				if (!isOneway(currentWay)) {
					RoadLink reverseRoadLink = roadLink.reverse();
					LOGGER.info("Extracted RoadLink:{}",
							reverseRoadLink.toString());
					roadLinks.add(reverseRoadLink);
				}
			} else {
				if (currentNode.isEmpty()) {
					logMissingNode(currentId);
				}
				if (nextNode.isEmpty()) {
					logMissingNode(nextId);
				}
			}
		}
	}

	private void configureUsedTags(String configFolder) {
		String usedTagsFile = configFolder + "used.txt";
		String usedIgnoreTagsFile = configFolder + "usedIgnore.txt";
		String compressTagsFile = configFolder + "compress.txt";
		waysClassifier = new WaysClassifier(usedTagsFile, usedIgnoreTagsFile,
				compressTagsFile);
	}

	private void makeDirs(String roadLinksFilename) {
		File file = new File(roadLinksFilename);
		file.mkdirs();
	}

	public void extractRoadNetwork(List<Way> ways, List<Node> nodes) {
		extractRoadNetwork(ways, nodes, "target/output/nodes.txt",
				"target/output/roadlinks.txt", "target/output/adj-list.txt");
	}

	public void extractRoadNetwork(List<Way> ways, List<Node> nodes,
			String nodesFilename, String roadLinksFilename,
			String adjListFilename) {
		extractRoadNetwork(ways, nodes, nodesFilename, roadLinksFilename,
				adjListFilename, "");
	}

	public void extractRoadNetwork(List<Way> ways, List<Node> nodes,
			String nodesFilename, String roadLinksFilename,
			String adjListFilename, String configFolder) {
		configureUsedTags(configFolder);
		LOGGER.info("Sorting Nodes");
		Collections.sort(nodes);
		LOGGER.info("Extracting RoadLinks");
		List<RoadLink> roadLinks = extractRoadLinks(ways, nodes);
		// TODO: COMPRESS
		LOGGER.info("Purging unused Nodes");
		nodes = purgeIsolatedNodes(nodes, roadLinks);
		LOGGER.info("Writing to Output");
		LOGGER.info("Writing RoadLinks");
		writeRoadLinksToFile(roadLinksFilename, roadLinks);
		LOGGER.info("Writing Nodes");
		writeNodesToFile(nodesFilename, nodes);
		LOGGER.info("Writing Adjacency List");
		writeAdjListToFile(adjListFilename, roadLinks, nodes);
		LOGGER.info("FINISHED");
	}

	protected void clearCentroidIds() {
		centroidIds.clear();
	}

	protected void deleteBuildingNodes(List<Way> ways, List<Node> nodes) {
		for (Way w : ways) {
			if (isCompress(w)) {
				List<Node> nodeList = w.getNodeList();
				for (Node n : nodeList) {
					LOGGER.info("Deleting node id:{}", n.getId());
					nodes.remove(n);
				}
				w.clearNdList();
			}
		}
	}

	protected void addCentroids(List<Way> ways, List<Node> nodes,
			List<Node> centroids) {
		for (int i = 0; i < centroids.size(); i++) {
			Node centroid = centroids.get(i);
			// Remove duplicated node id.
			nodes.remove(centroid);
			nodes.add(centroid);
			LOGGER.info("Added centroid:{}", centroid.toString());
			Way way = ways.get(i);
			ArrayList<Nd> ndList = new ArrayList<>();
			ndList.add(centroid.toNd());
			way.setNdList(ndList);
		}
	}

	public List<RoadLink> extractRoadLinks(List<Way> ways, List<Node> nodes) {
		List<RoadLink> roadLinks = new ArrayList<>();
		Set<String> missingNodes = new HashSet<>();
		List<String> problemLinks = new LinkedList<>();
		for (Way w : ways) {
			if (isUsed(w)) {
				LOGGER.info("Processing Way:{}", w.toString());
				String name = w.getName();
				List<Nd> nds = w.getNdList();
				for (int i = 0; i < nds.size() - 1; i++) {
					String currentId = nds.get(i).getRef();
					String nextId = nds.get(i + 1).getRef();
					Node current = findNode(currentId, nodes);
					Node next = findNode(nextId, nodes);
					if (!current.isEmpty() && !next.isEmpty()) {
						BigDecimal distance = getHalversineDistance(current,
								next);
						int lanes = w.getLanes();
						Highway highwayType = getHighwayType(w.getHighwayType());
						RoadLink roadLink = new RoadLink(current, next, name,
								distance, lanes, highwayType);
						LOGGER.info("Extracted RoadLink:{}",
								roadLink.toString());
						roadLinks.add(roadLink);
						if (!isOneway(w)) {
							RoadLink reverseRoadLink = roadLink.reverse();
							LOGGER.info("Extracted RoadLink:{}",
									reverseRoadLink.toString());
							roadLinks.add(reverseRoadLink);
						}
					} else {
						if (current.isEmpty()) {
							logMissingNode(currentId);
							missingNodes.add(currentId);
						}
						if (next.isEmpty()) {
							logMissingNode(nextId);
							missingNodes.add(nextId);
						}
						problemLinks.add(name);
					}
				}
			}
		}
		writeMissingNodesToFile(missingNodes);
		writeProblemLinksToFile(problemLinks);
		return roadLinks;
	}

	private Highway getHighwayType(String highwayType) {
		switch (highwayType.toLowerCase()) {
		case "tertiary":
			return Highway.TERTIARY;
		case "tertiary_link":
			return Highway.TERTIARY_LINK;
		case "secondary":
			return Highway.SECONDARY;
		case "secondary_link":
			return Highway.SECONDARY_LINK;
		case "primary":
			return Highway.PRIMARY;
		case "primary_link":
			return Highway.PRIMARY_LINK;
		case "trunk":
			return Highway.TRUNK;
		case "trunk_link":
			return Highway.TRUNK_LINK;
		case "motorway":
			return Highway.MOTORWAY;
		case "motorway_link":
			return Highway.MOTORWAY_LINK;
		case "service":
			return Highway.SERVICE;
		case "unclassified":
			return Highway.UNCLASSIFIED;
		case "building_link":
			return Highway.BUILDING_LINK;
		}
		return null;
	}

	private void writeProblemLinksToFile(List<String> problemLinks) {
		String filename = "target/output/problem-roads.txt";
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (String s : problemLinks) {
				bw.write(s);
				bw.write("\n");
			}
		} catch (IOException e) {
			throw new DataCleaningException("Error Writing to File", e);
		}
	}

	private void writeMissingNodesToFile(Set<String> missingNodes) {
		writeNodesToFile(missingNodes, TARGET_OUTPUT_MISSING_NODES_TXT);
	}

	private void writeMissingBuildingNodesToFile(Set<String> missingNodes) {
		writeNodesToFile(missingNodes, TARGET_OUTPUT_MISSING_BUILDING_NODES_TXT);
	}

	private void writeNodesToFile(Set<String> missingNodes, String filename) {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (String n : missingNodes) {
				bw.write(n);
				bw.write("\n");
			}
		} catch (IOException e) {
			throw new DataCleaningException("Error Writing to File", e);
		}
	}

	private static void forceMkdirParent(File file) {
		File parent = new File(file.getParent());
		parent.mkdirs();
	}

	private BigDecimal getHalversineDistance(Node current, Node next) {
		return calc.getDistance(current, next);
	}

	private RoadLink createRoadLink(Way way, Node startNode, Node endNode) {
		String name = way.getName();
		BigDecimal distance = getHalversineDistance(startNode, endNode);
		int lanes = way.getLanes();
		Highway highwayType = getHighwayType(way.getHighwayType());
		RoadLink roadLink = new RoadLink(startNode, endNode, name, distance,
				lanes, highwayType);
		return roadLink;
	}

	private void logMissingNode(String missingNodeId) {
		LOGGER.warn("Failed to extract RoadLink due to missing from node:{}",
				missingNodeId);
	}

	private void placeNodeInGrid(Node node) {
		if (!node.isInGrid()) {
			setGridNos(node);
			boxes[node.getLatGridNo()][node.getLonGridNo()].add(node);
		}
	}

	private void initializeBoxes() {
		int maxLatGrid = getGridNo(Double.parseDouble(bounds.getMaxlat()),
				Double.parseDouble(bounds.getMinlat()),
				Double.parseDouble(bounds.getMaxlat()), side);
		int maxLonGrid = getGridNo(Double.parseDouble(bounds.getMaxlon()),
				Double.parseDouble(bounds.getMinlon()),
				Double.parseDouble(bounds.getMaxlon()), side);
		boxes = new Box[maxLatGrid + 1][maxLonGrid + 1];
		for (int i = 0; i < maxLatGrid + 1; i++) {
			for (int j = 0; j < maxLonGrid + 1; j++) {
				boxes[i][j] = new Box();
			}
		}
	}

	protected void setGridNos(Node node) {
		node.setLatGridNo(getLatGridNo(node, bounds, side));
		node.setLonGridNo(getLonGridNo(node, bounds, side));
	}

	private boolean isOneway(Way w) {
		return w.getTagList().contains(ONEWAY);
	}

	private double getSquaredDistanceDouble(Node current, Node next) {
		double x1 = Double.parseDouble(current.getLon());
		double y1 = Double.parseDouble(current.getLat());
		double x2 = Double.parseDouble(next.getLon());
		double y2 = Double.parseDouble(next.getLat());
		double lon = (x2 - x1) * (x2 - x1);
		double lat = (y2 - y1) * (y2 - y1);
		return lon + lat;
	}

	protected static Node findNode(String nodeId, List<Node> nodes) {
		int nodeIndex = Collections.binarySearch(nodes, new Node(nodeId));
		if (nodeIndex < 0) {
			return Node.EMPTY;
		}
		return nodes.get(nodeIndex);
	}

	public static List<Node> purgeIsolatedNodes(List<Node> nodes,
			List<RoadLink> roadLinks) {
		LOGGER.info("Sorting Road Links");
		Collections.sort(roadLinks);

		SortedSet<Node> nodeSet = new TreeSet<>();
		Node previous = findNode(roadLinks.get(0).getStartNode().getId(), nodes);
		nodeSet.add(previous);
		for (RoadLink rl : roadLinks) {
			if (previous != rl.getStartNode()) {
				Node node = findNode(rl.getStartNode().getId(), nodes);
				nodeSet.add(node);
				previous = node;
			}
			nodeSet.add(findNode(rl.getEndNode().getId(), nodes));
		}
		return new ArrayList<>(nodeSet);
	}

	private void writeAdjListToFile(String filename, List<RoadLink> roadLinks,
			List<Node> nodes) {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(AdjacencyListFormatter.formatAdjString(nodes, roadLinks));
		} catch (IOException e) {
			throw new DataCleaningException("Error Writing to File", e);
		}
	}

	private void writeRoadLinksToFile(String filename, List<RoadLink> roadLinks) {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			String d = roadLinks.get(0).getDelimiter();
			String header = formatHeader(d);
			bw.write(header);
			for (RoadLink rl : roadLinks) {
				bw.write(rl.toString());
				bw.write("\n");
			}
		} catch (IOException e) {
			throw new DataCleaningException("Error Writing to File", e);
		}
	}

	private String formatHeader(String d) {
		StringBuilder builder = new StringBuilder();
		builder.append("start").append(d);
		builder.append("end").append(d);
		builder.append("name").append(d);
		builder.append("distance").append(d);
		builder.append("lanes").append(d);
		builder.append("type").append("\n");
		return builder.toString();
	}

	public static void writeNodesToFile(String filename, List<Node> nodeList) {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write("Node\tlat\tlon\tname\n");
			for (Node n : nodeList) {
				bw.write(n.getId());
				bw.write("\t");
				bw.write(String.format("%.7f", Double.parseDouble(n.getLat())));
				bw.write("\t");
				bw.write(String.format("%.7f", Double.parseDouble(n.getLon())));
				bw.write("\t");
				bw.write(n.getName());
				bw.write("\n");
			}
		} catch (IOException e) {
			throw new DataCleaningException("Error Writing to File", e);
		}
	}

	public List<RoadLink> connectCentroidsToNearestLink(
			List<RoadLink> roadLinks, List<Node> centroids) {
		List<RoadLink> buildingLinks = new ArrayList<>();
		for (Node c : centroids) {
			setGridNos(c);
			double min = Double.MAX_VALUE;
			Node closestNode = null;
			List<Node> searchArea = getNodesAround(c.getLatGridNo(),
					c.getLonGridNo());
			LOGGER.info("Calculating nearest centroid from: {}", c);
			for (Node node : searchArea) {
				double distance = getSquaredDistanceDouble(node, c);
				if (distance < min) {
					min = distance;
					closestNode = node;
				}
			}
			RoadLink buildingLink = connect(c, closestNode);
			buildingLinks.add(buildingLink);
			buildingLinks.add(buildingLink.reverse());
		}
		return buildingLinks;
	}

	private List<Node> getNodesAround(int latGridNo, int lonGridNo) {
		List<Node> nodes = new LinkedList<>();
		for (int i = latGridNo - 1; i <= latGridNo + 1; i++) {
			for (int j = lonGridNo - 1; j <= lonGridNo + 1; j++) {
				if (isInBounds(i, j)) {
					nodes.addAll(boxes[i][j].getNodes());
				}
			}
		}
		return nodes;
	}

	private boolean isInBounds(int i, int j) {
		return i >= 0 && i < boxes.length && j >= 0 && j < boxes[i].length;
	}

	private RoadLink connect(Node buildingNode, Node roadNode) {
		RoadLink buildingLink = new RoadLink(buildingNode, roadNode,
				buildingNode.getName(), getHalversineDistance(buildingNode,
						roadNode), 2, Highway.BUILDING_LINK);
		return buildingLink;
	}

	public int getLatGridNo(Node node) {
		return getLatGridNo(node, bounds, side);
	}

	public int getLatGridNo(Node node, Bounds bounds, double side) {
		String minlat = bounds.getMinlat();
		String maxlat = bounds.getMaxlat();
		return getGridNo(Double.parseDouble(node.getLat()),
				Double.parseDouble(minlat), Double.parseDouble(maxlat), side);
	}

	public int getLonGridNo(Node node) {
		return getLonGridNo(node, bounds, side);
	}

	public int getLonGridNo(Node node, Bounds bounds, double side) {
		String minlon = bounds.getMinlon();
		String maxlon = bounds.getMaxlon();
		return getGridNo(Double.parseDouble(node.getLon()),
				Double.parseDouble(minlon), Double.parseDouble(maxlon), side);
	}

	public int getGridNo(double val, double minVal, double maxVal, double side) {
		int gridNo = (int) ((val - minVal) / side);
		int maxGridNo = (int) ((maxVal - minVal) / side);
		if (gridNo < 0) {
			gridNo = 0;
		} else if (gridNo > maxGridNo) {
			gridNo = maxGridNo;
		}
		return gridNo;
	}

	protected void setSide(double side) {
		this.side = side;
	}

	protected boolean isCompress(Way w) {
		return waysClassifier.isCompress(w);
	}

	protected boolean isUsed(Way w) {
		return waysClassifier.isUsed(w);
	}

	public List<Tag> getUsedTags() {
		return waysClassifier.getUsedTags();
	}

	public List<Tag> getCompressTags() {
		return waysClassifier.getCompressTags();
	}

}
