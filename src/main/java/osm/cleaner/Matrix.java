package osm.cleaner;

public interface Matrix {

	int get(int row, int column);

	void set(int row, int column, int value);

	int getRowsSize();

}
