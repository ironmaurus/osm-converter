package osm.cleaner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import osm.Node;
import osm.NodeDegreeData.Type;

public class GraphReducer {
	private List<Node> nodes;
	private List<RoadLink> roadLinks;
	
	public void reduce(Graph graph) {
		reduce(graph.getNodes(), graph.getRoadLinks());
	}

	private void reduce(List<Node> nodes, List<RoadLink> roadLinks) {
		Collections.sort(roadLinks);
		this.roadLinks = roadLinks;
		Collections.sort(nodes);
		this.nodes = nodes;
		Node cachedFrom = nodes.get(0);
		for (RoadLink road : roadLinks) {
			Node startNode = road.getStartNode();
			if (!cachedFrom.equals(startNode)) {
				cachedFrom = findNode(startNode);
			}
			Node endNode = road.getEndNode();
			endNode = findNode(endNode);
			cachedFrom.addOutNode(endNode);
			cachedFrom.addOutBaton(new Baton(endNode, road));
			endNode.addInNode(cachedFrom);
			endNode.addInBaton(new Baton(cachedFrom, road));
		}
		classifyNodes(nodes);
		Deque<Node> primaryNodes = filterPrimaryNodes(nodes);
		for (Node origin : primaryNodes) {
			List<Node> originOutNodes = origin.getOutNodes();
			Deque<Node> normalNodes = filterNormalNodes(originOutNodes);
			for (Node normalNode : normalNodes) {
				Node targetNode = getOtherOutNode(origin, normalNode);
				Type type = targetNode.getType();
				if (isPrimary(type) && !hasLink(origin, targetNode)) {
					addLink(origin, normalNode, targetNode);
					purgeLink(origin, normalNode);
					purgeLink(normalNode, targetNode);
				} else if (isNormal(type)) {
					List<Node> normals = new LinkedList<>();
					normals.add(normalNode);
					Node current = targetNode;
					Node next = getOtherOutNode(normalNode, current);
					Type nextType = next.getType();
					while (nextType == Type.NORMAL) {
						normals.add(current);
						Node temp = next;
						next = getOtherOutNode(current, next);
						current = temp;
						nextType = next.getType();
					}
					if (isPrimary(nextType)) {
						if (!hasLink(origin, next)) {
							normals.add(current);
							BigDecimal distance = tallyDistance(origin,
									normals, next);
							Node firstNormal = normals.get(0);
							String name = findName(origin, firstNormal);
							addLink(origin, next, distance, name);
							purgeLinks(origin, normals, next);
						} else if (hasLink(origin, next)) {
							//TODO: Pending Test Case
						}
					}
				}
			}
		}
		Collections.sort(roadLinks);
		// TODO: Now we traverse the map somehow. Basically, all the normals
		// between two ENDPOINTS, INTERSECTIONS, or an ENDPOINT and an
		// INTERSECTION are to be deleted and replaced with a direct link
		// between the two nodes if there doesn't exist a link.
	}

	private void purgeLinks(Node origin, List<Node> normals, Node next) {
		purgeLink(origin, normals.get(0));
		for (int i = 0; i < normals.size() - 1; i++) {
			Node from = normals.get(i);
			Node to = normals.get(i + 1);
			purgeLink(from, to);
		}
		purgeLink(normals.get(normals.size() - 1), next);
	}

	private BigDecimal tallyDistance(Node origin, List<Node> normals, Node next) {
		Node firstNormal = normals.get(0);
		Node lastNormal = normals.get(normals.size() - 1);
		BigDecimal distance = findDistance(origin, firstNormal);
		for (int i = 0; i < normals.size() - 1; i++) {
			Node from = normals.get(i);
			Node to = normals.get(i + 1);
			distance = distance.add(findDistance(from, to));
		}
		distance = distance.add(findDistance(lastNormal, next));
		return distance;
	}

	private void addLink(Node origin, Node next, BigDecimal distance,
			String name) {
		RoadLink link = new RoadLink(origin, next, name, distance);
		roadLinks.add(link);
		origin.addOutNode(next);
		origin.addOutBaton(new Baton(next, link));
		next.addInNode(origin);
		next.addInBaton(new Baton(origin, link));
	}

	private boolean hasLink(Node origin, Node targetNode) {
		return origin.getOutNodes().contains(targetNode);
	}

	private Node getOtherOutNode(Node from, Node current) {
		List<Node> outNodes = current.getOutNodes();
		int numEdges = current.getInBatons().size()
				+ current.getOutBatons().size();
		if (numEdges % 2 == 0 && outNodes.size() > 0) {
			Node value = Node.EMPTY;
			for (Node node : outNodes) {
				if (!node.equals(from)) {
					value = node;
					break;
				}
			}
			return value;
		} else {
			return Node.EMPTY;
		}
	}

	private void addLink(Node origin, Node normalNode, Node targetNode) {
		String name = findName(origin, normalNode);
		BigDecimal distance = findDistance(origin, normalNode).add(
				findDistance(normalNode, targetNode));
		RoadLink link = new RoadLink(origin, targetNode, name, distance);
		roadLinks.add(link);
		origin.addOutNode(targetNode);
		origin.addOutBaton(new Baton(targetNode, link));
		targetNode.addInNode(origin);
		targetNode.addInBaton(new Baton(origin, link));
	}

	private void purgeLink(Node source, Node target) {
		RoadLink link = findRoadLink(source, target);
		roadLinks.remove(link);
		source.removeOutNode(target);
		source.removeOutBaton(new Baton(target));
		target.removeInNode(source);
		target.removeInBaton(new Baton(source));
	}

	private RoadLink findRoadLink(Node node, Node targetNode) {
		for (Baton baton : node.getOutBatons()) {
			if (baton.getNode().equals(targetNode)) {
				return baton.getLink();
			}
		}
		// Not found. Find a better way to represent
		return null;
	}

	private BigDecimal findDistance(Node node, Node targetNode) {
		BigDecimal distance = BigDecimal.ZERO;
		for (Baton baton : node.getOutBatons()) {
			if (baton.getNode().equals(targetNode)) {
				distance = baton.getLink().getDistance();
			}
		}
		return distance;
	}

	private String findName(Node node, Node targetNode) {
		String name = "";
		for (Baton baton : node.getOutBatons()) {
			if (baton.getNode().equals(targetNode)) {
				name = baton.getLink().getName();
			}
		}
		return name;
	}

	private Deque<Node> filterPrimaryNodes(List<Node> nodes) {
		Deque<Node> primaryNodes = new LinkedList<>();
		for (Node node : nodes) {
			Type type = node.getType();
			if (isPrimary(type)) {
				primaryNodes.add(node);
			}
		}
		return primaryNodes;
	}

	private boolean isPrimary(Type type) {
		return type == Type.ENDPOINT || type == Type.INTERSECTION;
	}

	private boolean isNormal(Type type) {
		return type == Type.NORMAL;
	}

	private Deque<Node> filterNormalNodes(List<Node> nodes) {
		Deque<Node> normalNodes = new LinkedList<>();
		for (Node node : nodes) {
			Type type = node.getType();
			if (type == Type.NORMAL) {
				normalNodes.add(node);
			}
		}
		return normalNodes;
	}

	private void classifyNodes(List<Node> nodes) {
		for (Node node : nodes) {
			classifyNode(node);
		}
	}

	private void classifyNode(Node node) {
		int degree = node.getDegree();
		// TODO: Add an alert. There should not be any islands in the data
		// by this stage.
		if (degree == 0) {
			node.setType(Type.ISLAND);
		} else if (degree == 1) {
			node.setType(Type.ENDPOINT);
		} else if (degree == 2) {
			node.setType(Type.NORMAL);
		} else {
			node.setType(Type.INTERSECTION);
		}
	}

	private Node findNode(Node node) {
		return nodes.get(Collections.binarySearch(nodes, node));
	}
}
