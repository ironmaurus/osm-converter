package osm.cleaner;

@SuppressWarnings("serial")
public class DataCleaningException extends RuntimeException {

	public DataCleaningException() {
	}

	public DataCleaningException(String message) {
		super(message);
	}

	public DataCleaningException(Throwable cause) {
		super(cause);
	}

	public DataCleaningException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataCleaningException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
