package osm.cleaner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import osm.Node;

public class TrajanAlgorithm {
	private List<TrajanNode> vertices;
	private int index;
	private Deque<TrajanNode> stack;
	private List<List<TrajanNode>> components;

	public int getNumComponents(List<Node> nodes, List<RoadLink> roadLinks) {
		return execute(nodes, roadLinks).size();
	}

	public List<Component> findComponents(List<Node> nodes,
			List<RoadLink> roadLinks) {
		List<List<TrajanNode>> trajanNodes = execute(nodes, roadLinks);
		List<Component> componentNodes = new LinkedList<>();
		for (List<TrajanNode> component : trajanNodes) {
			componentNodes.add(new Component(new LinkedList<Node>()));
			Component current = componentNodes.get(componentNodes.size() - 1);
			for (TrajanNode n : component) {
				current.add(n.node);
			}
		}
		return componentNodes;
	}

	private List<List<TrajanNode>> execute(List<Node> nodes,
			List<RoadLink> roadLinks) {
		Collections.sort(nodes);
		Collections.sort(roadLinks);
		components = new ArrayList<>();
		index = 0;
		stack = new LinkedList<>();
		this.vertices = toTrajanNode(nodes);
		setSucessorEdges(roadLinks);
		for (TrajanNode v : vertices) {
			if (isUnvisited(v)) {
				strongConnect(v);
			}
		}
		return components;
	}

	private void setSucessorEdges(List<RoadLink> roadLinks) {
		int counter = 0;
		for (TrajanNode vertex : vertices) {
			Node currentNode = vertex.node;
			vertex.successorEdges = new LinkedList<>();
			while (counter < roadLinks.size()
					&& currentNode
							.equals(roadLinks.get(counter).getStartNode())) {
				RoadLink currentRoad = roadLinks.get(counter);
				vertex.successorEdges.add(new TrajanEdge(currentRoad));
				counter++;
			}
		}
	}

	private void strongConnect(TrajanNode vertex) {
		indexAndStack(vertex);
		for (TrajanEdge edge : vertex.successorEdges) {
			TrajanNode to = edge.to;
			if (isUnvisited(to)) {
				strongConnect(to);
				vertex.lowlink = Math.min(vertex.lowlink, to.lowlink);
			} else if (to.inStack) {
				vertex.lowlink = Math.min(vertex.lowlink, to.index);
			}
		}

		if (isRoot(vertex)) {
			generateComponent(vertex);
		}
	}

	private boolean isUnvisited(TrajanNode to) {
		return to.index == -1;
	}

	private void generateComponent(TrajanNode vertex) {
		List<TrajanNode> component = new LinkedList<>();
		TrajanNode to = null;
		while (!vertex.equals(to)) {
			to = stack.pop();
			to.inStack = false;
			component.add(to);
		}
		components.add(component);
	}

	private boolean isRoot(TrajanNode vertex) {
		return vertex.lowlink == vertex.index;
	}

	private void indexAndStack(TrajanNode vertex) {
		vertex.index = index;
		vertex.lowlink = index;
		index++;
		stack.push(vertex);
		vertex.inStack = true;
	}

	private List<TrajanNode> toTrajanNode(List<Node> nodes) {
		List<TrajanNode> vertices = new ArrayList<>();
		for (Node n : nodes) {
			vertices.add(new TrajanNode(n));
		}
		return vertices;
	}

	class TrajanEdge {
		TrajanNode from;
		TrajanNode to;

		public TrajanEdge(RoadLink link) {
			from = findVertex(link.getStartNode());
			to = findVertex(link.getEndNode());
		}

		private TrajanNode findVertex(Node vertex) {
			return vertices.get(Collections.binarySearch(vertices,
					new TrajanNode(vertex)));
		}
	}

	class TrajanNode implements Comparable<TrajanNode> {
		Node node;
		int index = -1;
		int lowlink = -1;
		List<TrajanEdge> successorEdges;
		boolean inStack = false;

		public TrajanNode(Node node) {
			this.node = node;
		}

		@Override
		public int compareTo(TrajanNode o) {
			return node.compareTo(o.node);
		}
	}

}
