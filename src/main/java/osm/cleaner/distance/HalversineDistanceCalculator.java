package osm.cleaner.distance;

import java.math.BigDecimal;

import osm.Node;

public class HalversineDistanceCalculator implements DistanceCalculator {
	private static final BigDecimal DEFAULT_RADIUS = new BigDecimal(6371);
	private BigDecimal radius;

	public HalversineDistanceCalculator() {
		this(DEFAULT_RADIUS);
	}

	public HalversineDistanceCalculator(BigDecimal radius) {
		this.radius = radius;
	}

	@Override
	public BigDecimal getDistance(Node from, Node to) {
		double lat1 = Double.parseDouble(from.getLat());
		double lon1 = Double.parseDouble(from.getLon());
		double lat2 = Double.parseDouble(to.getLat());
		double lon2 = Double.parseDouble(to.getLon());
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);

		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2)
				* Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return radius.multiply(BigDecimal.valueOf(c)).setScale(4,
				BigDecimal.ROUND_HALF_UP);
	}
}
