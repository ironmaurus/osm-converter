package osm.cleaner.distance;

import java.math.BigDecimal;

import osm.Node;

public interface DistanceCalculator {
	BigDecimal getDistance(Node from, Node to);
}
