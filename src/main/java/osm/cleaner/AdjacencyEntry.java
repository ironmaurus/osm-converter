package osm.cleaner;

import java.util.ArrayList;
import java.util.List;

import osm.Node;

public class AdjacencyEntry {
	private String id;
	private String location;
	private String name;
	private List<AdjacentNode> adjacentNodes;

	public AdjacencyEntry(Node node) {
		id = node.getId();
		StringBuilder builder = new StringBuilder();
		builder.append("[").append(node.getLon()).append(",")
				.append(node.getLat()).append("]");
		location = builder.toString();
		name = node.getName();
		adjacentNodes = new ArrayList<>();
	}

	public void addNode(String id, String distance, String roadName) {
		AdjacentNode adjNode = new AdjacentNode(id, distance, roadName);
		adjacentNodes.add(adjNode);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append("\"id\":\"").append(id).append("\",");
		builder.append("\"loc\":\"").append(location).append("\",");
		builder.append("\"name\":\"").append(name).append("\",");
		builder.append("\"adjacent_nodes\":[").append(adjNodesToString())
				.append("]");
		builder.append("}");
		return builder.toString();
	}

	private String adjNodesToString() {
		StringBuilder builder = new StringBuilder();
		for (AdjacentNode n : adjacentNodes) {
			if (builder.length() > 0) {
				builder.append(",");
			}
			builder.append(n.toString());
		}
		return builder.toString();
	}
}