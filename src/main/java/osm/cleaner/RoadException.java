package osm.cleaner;

public class RoadException extends RuntimeException {

	private static final long serialVersionUID = -2579141890715197071L;

	public RoadException() {
		// TODO Auto-generated constructor stub
	}

	public RoadException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RoadException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public RoadException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RoadException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
