package osm.cleaner;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import osm.Node;

//TODO: Update to include lanes when asked
public class JsonFormatter {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(JsonFormatter.class);

	public static String format(List<Node> nodes, List<RoadLink> roadLinks,
			String adjListContents) {
		sortCollections(nodes, roadLinks);
		String[] lines = adjListContents.split("\n");
		StringBuilder builder = new StringBuilder();
		int roadLinkCounter = 0;
		for (int i = 0; i < lines.length; i++) {
			if (i > 0) {
				builder.append("\n");
			}
			String[] adjEntry = lines[i].split(";");
			String nodeId = adjEntry[0];
			Node node = findNode(nodes, nodeId);
			AdjacencyEntry entry = new AdjacencyEntry(node);
			if (adjEntry.length > 1) {
				String[] adjNodes = adjEntry[1].split(",");
				for (int j = 0; j < adjNodes.length; j++) {
					RoadLink currentRoad = roadLinks.get(roadLinkCounter);
					String name = escapeSpecialCharacters(currentRoad.getName());
					entry.addNode(adjNodes[j], currentRoad.getDistance().toString(),
							name);
					roadLinkCounter++;
				}
			}
			String entryString = entry.toString();
			builder.append(entryString);
			LOGGER.info(entryString);
		}
		return builder.toString();
	}

	private static void sortCollections(List<Node> nodes,
			List<RoadLink> roadLinks) {
		Collections.sort(nodes);
		Collections.sort(roadLinks);
	}

	private static Node findNode(List<Node> nodes, String nodeId) {
		int index = Collections.binarySearch(nodes, new Node(nodeId));
		Node node = nodes.get(index);
		return node;
	}

	private static String escapeSpecialCharacters(String name) {
		name = name.replace("\"", "\\\"").replace("\'", "\\\'");
		return name;
	}

}
