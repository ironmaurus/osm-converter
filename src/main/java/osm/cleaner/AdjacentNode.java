package osm.cleaner;

public class AdjacentNode {
	private String id;
	private String distance;
	private String roadName;

	public AdjacentNode(String id, String distance, String roadName) {
		this.id = id;
		this.distance = distance;
		this.roadName = roadName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append("\"id\":\"").append(id).append("\",");
		builder.append("\"distance\":\"").append(distance).append("\",");
		builder.append("\"roadlink\":\"").append(roadName).append("\"");
		builder.append("}");
		return builder.toString();
	}
}
