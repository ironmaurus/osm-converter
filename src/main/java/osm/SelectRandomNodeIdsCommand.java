package osm;

import static osm.FileUtils.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class SelectRandomNodeIdsCommand implements Command {
	private String inputNodesFilename;
	private int numToSelect;
	private String outputNodeIdsFilename;

	public SelectRandomNodeIdsCommand(String inputNodesFilename, int numToSelect,
			String outputNodeIdsFilename) {
		this.inputNodesFilename = inputNodesFilename;
		this.numToSelect = numToSelect;
		this.outputNodeIdsFilename = outputNodeIdsFilename;
	}

	@Override
	public void execute() {
		try {
			List<Node> nodes = readNodes(inputNodesFilename);
			RandomGenerator randomGenerator = new RandomGenerator();
			List<Node> selectedNodes = randomGenerator.selectUniqueRandomValues(nodes, numToSelect);
			writeNodeIdsToFile(selectedNodes);
		} catch (Exception e) {
			throw new RuntimeException("Exception in SelectRandomNodesCommand", e);
		}

	}

	private void writeNodeIdsToFile(List<Node> selectedNodes) {
		try(PrintWriter writer = createPrintWriter(new File(outputNodeIdsFilename))) {
			for(Node node: selectedNodes) {
				writer.println(node.getId());
			}
		} catch	(IOException e) {
			throw new RuntimeException("Error writing to File: " + outputNodeIdsFilename, e);
		}
	}

}
