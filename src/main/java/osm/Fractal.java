package osm;

import java.util.List;

public class Fractal {

	public int[][] fractal(List<Node> nodes, int zoom) {
		int size = (int) Math.pow(2.0, zoom);
		int[][] grid = new int[size][size];
		double[] ranges = getRanges(nodes);
		ranges = makeRangesSquare(ranges);
		double maxLat = ranges[1];
		double minLat = ranges[0];
		double cutLat = (maxLat - minLat) / size;
		double maxLon = ranges[3];
		double minLon = ranges[2];
		double cutLon = (maxLon - minLon) / size;
		for (Node node : nodes) {
			double lat = Double.parseDouble(node.getLat());
			double lon = Double.parseDouble(node.getLon());
			int latIndex = (int) ((maxLat - lat) / cutLat);
			if (latIndex >= size) {
				latIndex = size - 1;
			}
			int lonIndex = (int) ((lon - minLon) / cutLon);
			if (lonIndex >= size) {
				lonIndex = size - 1;
			}
			grid[latIndex][lonIndex] = grid[latIndex][lonIndex] + 1;
		}
		return grid;
	}
	
	public int fractalDimension(List<Node> nodes, int zoom) {
		int size = (int) Math.pow(2.0, zoom);
		int[][] grid = new int[size][size];
		double[] ranges = getRanges(nodes);
		ranges = makeRangesSquare(ranges);
		double maxLat = ranges[1];
		double minLat = ranges[0];
		double cutLat = (maxLat - minLat) / size;
		double maxLon = ranges[3];
		double minLon = ranges[2];
		double cutLon = (maxLon - minLon) / size;
		for (Node node : nodes) {
			double lat = Double.parseDouble(node.getLat());
			double lon = Double.parseDouble(node.getLon());
			int latIndex = (int) ((maxLat - lat) / cutLat);
			if (latIndex >= size) {
				latIndex = size - 1;
			}
			int lonIndex = (int) ((lon - minLon) / cutLon);
			if (lonIndex >= size) {
				lonIndex = size - 1;
			}
			grid[latIndex][lonIndex] = 1;
		}
		int sum = 0;
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				sum += grid[i][j];
			}
		}
		return sum;
	}

	private double[] makeRangesSquare(double[] ranges) {
		double latSide = ranges[1] - ranges[0];
		double lonSide = ranges[3] - ranges[2];
		if (latSide > lonSide) {
			double center = (ranges[3] + ranges[2]) / 2.0;
			ranges[2] = center - (latSide / 2.0);
			ranges[3] = center + (latSide / 2.0);
		} else {
			double center = (ranges[0] + ranges[1]) / 2.0;
			ranges[0] = center - (lonSide / 2.0);
			ranges[1] = center + (lonSide / 2.0);
		}
		return ranges;
	}

	private double[] getRanges(List<Node> nodes) {
		double minLat = Double.parseDouble(nodes.get(0).getLat());
		double maxLat = minLat;
		double minLon = Double.parseDouble(nodes.get(0).getLon());
		double maxLon = minLon;
		for (Node node : nodes) {
			double lat = Double.parseDouble(node.getLat());
			double lon = Double.parseDouble(node.getLon());
			if (lat < minLat) {
				minLat = lat;
			} else if (lat > maxLat) {
				maxLat = lat;
			}
			if (lon < minLon) {
				minLon = lon;
			} else if (lon > maxLon) {
				maxLon = lon;
			}
		}
		double[] ranges = { minLat, maxLat, minLon, maxLon };
		return ranges;
	}

	protected double cut(double max, double min, int side) {
		return (max - min) / side;
	}

	public double[] getCuts(double max, double min, int side) {
		double cut = cut(max, min, side);
		double cuts[] = new double[side + 1];
		cuts[0] = min;
		cuts[side] = max;
		for (int i = 1; i < side; i++) {
			cuts[i] = cuts[i - 1] + cut;
		}
		return cuts;
	}

}
