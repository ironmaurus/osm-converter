package osm.debug;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import osm.Nd;
import osm.Osm;
import osm.Way;

public class RoadLinkDebugTool {
	private String fromString;
	private List<String> toString;
	private List<Way> ways;

	public RoadLinkDebugTool() {
		toString = new ArrayList<>();
	}

	public void readInput(String filename) throws FileNotFoundException {
		try (Scanner scanner = new Scanner(new FileReader(filename))) {
			String line = scanner.nextLine();
			String[] split = line.split(" ");
			fromString = split[1];
			toString.add(split[3]);
			while (scanner.hasNext()) {
				line = scanner.nextLine();
				split = line.split(" ");
				toString.add(split[3]);
			}
		}
	}

	public void unmarshallWays(String filename) throws FileNotFoundException,
			JAXBException {
		JAXBContext context = JAXBContext.newInstance(Osm.class);
		Unmarshaller um = context.createUnmarshaller();

		FileReader reader = new FileReader(filename);
		Osm osm = (Osm) um.unmarshal(reader);

		context = JAXBContext.newInstance(Osm.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		ways = osm.getWayList();
	}

	public String getFromString() {
		return fromString;
	}

	public List<String> getToString() {
		return toString;
	}

	public List<String> findWayInfo(String n) {
		List<String> info = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Nd node = new Nd(n);
		for(Way w: ways) {
			if(w.getNdList().contains(node)) {
				builder.append(w.getId()).append("\t").append(w.getName());
				info.add(builder.toString());
				builder = new StringBuilder();
			}
		}
		return info;
	}

}
