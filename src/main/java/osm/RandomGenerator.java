package osm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class RandomGenerator {

	public <E> List<E> selectUniqueRandomValues(List<E> values, int numToSelect) {
		List<Integer> indices = generateUniqueRandomIndices(values.size(),
				numToSelect);
		List<E> selectedValues = new LinkedList<>();
		for (Integer i : indices) {
			selectedValues.add(values.get(i));
		}
		return selectedValues;
	}
	
	protected List<Integer> generateUniqueRandomIndices(int listSize,
			int numToGenerate) {
		Set<Integer> indices = new HashSet<>(numToGenerate);
		while (indices.size() < numToGenerate) {
			indices.add((int) (Math.random() * listSize));
		}
		return new ArrayList<Integer>(indices);
	}

}
