package osm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "way")
public class Way {
	private String id;
	private String visible;
	private ArrayList<Nd> ndList;
	private ArrayList<Tag> tagList;

	public Way() {
		tagList = new ArrayList<>();
	}

	// TODO: Clone?
	public Way(String id, String... nodes) {
		this.id = id;
		this.ndList = new ArrayList<>();
		for (String s : nodes) {
			ndList.add(new Nd(s));
		}

		visible = "true";
		tagList = new ArrayList<>();

	}

	// TODO:Return 0 for non-highway
	public int getLanes() {
		int lanes = 2;
		for (Tag t : tagList) {
			if ("lanes".equals(t.getK())) {
				String value = t.getV();
				try {
					lanes = Integer.parseInt(value);
				} catch (NumberFormatException e) {
					value = value.split(";")[0];
					lanes = Integer.parseInt(value);
				}
			}
		}
		return lanes;
	}

	public List<Node> getNodeList() {
		List<Node> nodeList = new ArrayList<>();
		for (Nd n : ndList) {
			nodeList.add(new Node(n.getRef()));
		}
		return nodeList;
	}

	public String getName() {
		String name = "";
		for (Tag t : tagList) {
			if ("name".equals(t.getK())) {
				name = t.getV();
			}
		}
		return name;
	}

	public void clearNdList() {
		ndList.clear();
	}

	@XmlAttribute
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlAttribute
	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	@XmlElement(name = "nd")
	public ArrayList<Nd> getNdList() {
		return ndList;
	}

	public void setNdList(ArrayList<Nd> ndList) {
		this.ndList = ndList;
	}

	@XmlElement(name = "tag")
	public ArrayList<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(ArrayList<Tag> tagList) {
		this.tagList = tagList;
	}

	@Override
	public String toString() {
		return "Way [id=" + id + ", ndList=" + ndList + ", tagList=" + tagList
				+ "]";
	}

	public String getHighwayType() {
		for (Tag t : tagList) {
			if ("highway".equals(t.getK())) {
				return t.getV();
			}
		}
		return null;
	}

}
