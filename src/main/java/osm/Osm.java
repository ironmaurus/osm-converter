package osm;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "osm")
public class Osm {
	
	private Bounds bounds;
	private ArrayList<Node> nodeList;
	private ArrayList<Way> wayList;

	@XmlElement(name = "bounds")
	public Bounds getBounds() {
		return bounds;
	}

	public void setBounds(Bounds bounds) {
		this.bounds = bounds;
	}

	@XmlElement(name = "node")
	public ArrayList<Node> getNodeList() {
		return nodeList;
	}

	public void setNodeList(ArrayList<Node> nodeList) {
		this.nodeList = nodeList;
	}

	@XmlElement(name = "way")
	public ArrayList<Way> getWayList() {
		return wayList;
	}

	public void setWayList(ArrayList<Way> wayList) {
		this.wayList = wayList;
	}
	
}
