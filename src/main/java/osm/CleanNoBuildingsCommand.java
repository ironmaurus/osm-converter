package osm;

import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import osm.cleaner.DataCleaner;

public class CleanNoBuildingsCommand implements Command {

	private static final String DEFAULT_CONFIG_FOLDER = "";
	private String osmFilename;
	private String nodesFilename;
	private String roadLinksFilename;
	private String adjListFilename;
	private String configFolder;

	public CleanNoBuildingsCommand(String osmFilename, String nodesFilename, String roadLinksFilename, String adjListFilename) {
		this(osmFilename, nodesFilename, roadLinksFilename, adjListFilename, DEFAULT_CONFIG_FOLDER);
	}
	
	public CleanNoBuildingsCommand(String osmFilename, String nodesFilename,
			String roadLinksFilename, String adjListFilename, String configFolder) {
		this.osmFilename = osmFilename;
		this.nodesFilename = nodesFilename;
		this.roadLinksFilename = roadLinksFilename;
		this.adjListFilename = adjListFilename;
		this.configFolder = configFolder;
	}

	@Override
	public void execute() {		
		try {
			Osm osm = unmarshallOsm();
			DataCleaner cleaner = new DataCleaner(osm.getBounds());
			cleaner.extractRoadNetwork(osm.getWayList(), osm.getNodeList(),
					nodesFilename, roadLinksFilename, adjListFilename, configFolder);
		} catch (FileNotFoundException | JAXBException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private Osm unmarshallOsm() throws JAXBException,
			FileNotFoundException {
		JAXBContext context = JAXBContext.newInstance(Osm.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		Osm osm = (Osm) unmarshaller.unmarshal(new FileReader(osmFilename));
		return osm;
	}

	protected String getConfigFolder() {
		return configFolder;
	}


}
