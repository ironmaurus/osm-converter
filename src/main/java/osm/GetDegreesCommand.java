package osm;
import static osm.FileUtils.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import osm.cleaner.Graph;
import osm.cleaner.RoadLink;

public class GetDegreesCommand implements Command {
	private String inputNodesFilename;
	private String inputRoadlinksFilename;
	private String outputDegreesFilename;
	
	public GetDegreesCommand(String inputNodesFilename, String inputRoadlinksFilename, String outputDegreesFilename) {
		this.inputNodesFilename = inputNodesFilename;
		this.inputRoadlinksFilename = inputRoadlinksFilename;
		this.outputDegreesFilename = outputDegreesFilename;
	}

	@Override
	public void execute() {
		try {
			List<Node> nodes = readNodes(inputNodesFilename);
			List<RoadLink> roadLinks = readRoadlinks(inputRoadlinksFilename);
			Graph graph = new Graph(nodes, roadLinks);
			try(PrintWriter writer = createPrintWriter(new File(outputDegreesFilename))) {
				writer.println("id,indegree,outdegree,sumdegree");
				writer.append(graph.getDegrees());
			} catch	(IOException e) {
				throw new RuntimeException("Error writing to File: " + outputDegreesFilename, e);
			}
		} catch(FileNotFoundException e) {
			throw new GetDegreesCommandException("File not Found", e);
		}
		
	}

}
