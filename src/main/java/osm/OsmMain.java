package osm;

import static osm.FileUtils.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import osm.cleaner.AdjacencyListFormatter;
import osm.cleaner.BrokenRoadData;
import osm.cleaner.Component;
import osm.cleaner.CondensedGraph;
import osm.cleaner.DataCleaner;
import osm.cleaner.ExperimentException;
import osm.cleaner.Graph;
import osm.cleaner.JsonFormatter;
import osm.cleaner.Road;
import osm.cleaner.RoadLink;
import osm.cleaner.RoadLink.Highway;
import osm.cleaner.TrajanAlgorithm;
import osm.cleaner.distance.DistanceCalculator;
import osm.cleaner.distance.HalversineDistanceCalculator;

public class OsmMain {
	private static final String DEFAULT_LOG4J_PROPERTIES = "log4j.properties";
	private static final String DEFAULT_RESULT_OUTPUT = "result.out";
	private static final String DEFAULT_REPAIR_OUTPUT = "output/REPAIR.txt";
	private static final String DEFAULT_ROADLINKS = "roadlinks.txt";
	private static final String DEFAULT_NODES = "nodes.txt";
	private static final Logger LOGGER = LoggerFactory.getLogger(OsmMain.class);

	public static void main(String[] args) throws JAXBException, IOException {
		PropertyConfigurator.configure(DEFAULT_LOG4J_PROPERTIES);
		LOGGER.info("Start");
		String flag = args[0];
		if ("-a".equals(flag)) {
			createAdjacencyList(args);
		} else if ("-an".equals(flag)) {
			getANDegree(args);
		} else if ("-c".equals(flag)) {
			listComponentsToFile(args);
		} else if ("-con".equals(flag)) {
			printConnectivity(args);
		} else if ("-d".equals(flag)) {
			toKilometers(args);
		} else if ("-er".equals(flag)) {
			String nodes = args[1];
			String roadLinks = args[2];
			double radius = Double.parseDouble(args[3]);
			double threshold = Double.parseDouble(args[4]);
			int id = Integer.parseInt(args[5]);
			String repairOutput = args[6];
			String resultOutput = args[7];
			random(nodes, roadLinks, radius, threshold, id, repairOutput,
					resultOutput);
		} else if ("-erf".equals(flag)) {
			String nodes = args[1];
			String roadLinks = args[2];
			String inputParams = args[3];
			randomFromFile(nodes, roadLinks, inputParams);
		} else if ("-erx".equals(flag)) {
			randomMultiple(args);
		} else if ("-erxf".equals(flag)) {
			randomMultipleFromFile(args);
		} else if ("-erxp".equals(flag)) {
			randomAllRadii(args);
		} else if ("-erxpa".equals(flag)) {
			randomAll(args);
		} else if ("-es".equals(flag)) {
			String nodes = args[1];
			String roadlinks = args[2];
			String repairOutput = args[3];
			String resultOutput = args[4];
			String experimentId = args[5];
			String radius = args[6];
			String threshold = args[7];
			String centerId = args[8];
			selectExperiment(nodes, roadlinks, repairOutput, resultOutput,
					experimentId, radius, threshold, centerId);
		} else if ("-esf".equals(flag)) {
			String experimentParams = args[1];
			selectExperimentFromFile(experimentParams);
		} else if ("-esfnr".equals(flag)) {
			String experimentParams = args[1];
			selectExperimentNoRepairFromFile(experimentParams);
		} else if ("-espfnr".equals(flag)) {
			String experimentParams = args[1];
			selectExperimentNewPointNoRepairFromFile(experimentParams);
		} else if ("-f".equals(flag)) {
			String nodes = args[1];
			String output = args[2];
			int zoomLevel = Integer.parseInt(args[3]);
			fractal(nodes, output, zoomLevel);
		} else if ("-fd".equals(flag)) {
			String nodes = args[1];
			String output = args[2];
			int zoomLevel = Integer.parseInt(args[3]);
			fractalDimension(nodes, output, zoomLevel);
		} else if ("-i".equals(flag)) {
			interactiveMode(System.in, args);
		} else if ("-j".equals(flag)) {
			formatToJson(args);
		} else if ("-kp".equals(flag)) {
			// 1.in/node, 2.in/roadlink, 3.out/node, 4.out/roadlink
			purgeSmallerComponents(args);
		} else if ("-r".equals(flag)) {
			repairPriority(args);
		} else if ("-skp".equals(flag)) {
			// 1.osm, 2.node, 3.roadlink, 4.adj-list, 5.node-c, 6.roadlink-c
			// TODO: Consider fixing adj-list as well
			cleanNoBuildingsPurgeSmallerComponents(args);
		} else if ("-str".equals(flag)) {
			straighten(args);
		} else if ("-tr".equals(flag)) {
			// TODO: Improve pairing speed.
			List<RoadLink> roadLinks = readRoadLinks(args[1]);
			String outFile = args[2];
			toRoad(roadLinks, outFile);
		} else {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.createCommand(flag, args).execute();
		}
	}

	private static void selectExperimentNewPointNoRepairFromFile(
			String experimentParams) throws IOException {
		ExperimentParams params = readExperimentParamsNewPoint(experimentParams);
		List<CaseParams> caseParams = params.getCaseParams();
		List<Node> nodes = readNodes(params.getNodesFilename());
		List<RoadLink> roadLinks = readRoadLinks(params.getRoadlinksFilename());
		try (FileWriter writer = new FileWriter(new File(
				params.getResultOutputFilename()))) {
			for (CaseParams c : caseParams) {
				writer.append(selectedNewPointNoRepair(nodes, roadLinks,
						c.getExperimentId(), c.getRadius(),
						c.getTargetThreshold(), c.getLat(), c.getLon()));
			}
		}

	}

	public static String selectedNewPointNoRepair(List<Node> nodes,
			List<RoadLink> roadLinks, String experimentId, String radiusString,
			String thresholdString, String latString, String lonString) {
		Graph damagedGraph = new Graph(new ArrayList<>(nodes), new ArrayList<>(
				roadLinks));
		Node center = new Node("c" + experimentId, latString, lonString);
		double radius = Double.parseDouble(radiusString);
		double targetThreshold = Double.parseDouble(thresholdString);
		LOGGER.info("Parameters - Radius: {} Threshold: {} Id: {}", radius,
				targetThreshold, experimentId);
		LOGGER.info("Center: " + center.toString());

		List<RoadLink> inRangeLinks = damagedGraph.inRangeLinks(center, radius);

		LOGGER.info("RL in range: " + inRangeLinks.size());
		LOGGER.info("Roads in range: " + Road.toRoads(inRangeLinks).size());
		// Must never print ERROR
		StringBuilder text = new StringBuilder("" + targetThreshold)
				.append(",").append(radius).append(",");

		BrokenRoadData brokenRoadData = damagedGraph.destroyLinksAround(center,
				radius, targetThreshold);
		List<Road> brokenRoads = brokenRoadData.getBrokenRoads();
		double actualThreshold = brokenRoadData.getActualThreshold();
		double distanceBroken = sumLength(brokenRoads);

		LOGGER.info("Destroyed: " + brokenRoads.size());
		int totalBroken = brokenRoads.size();
		LOGGER.info("Connectivity: " + damagedGraph.pathMatrixConnectivity());

		StringBuilder builder = new StringBuilder();
		builder.append(experimentId).append(",").append("NR").append(",")
				.append(totalBroken).append(",").append("NR").append(",")
				.append(distanceBroken).append(",").append(actualThreshold)
				.append("\n");
		text.append(builder.toString());

		return text.toString();
	}

	private static void selectExperimentNoRepairFromFile(String experimentParams)
			throws IOException {
		ExperimentParams params = readExperimentParams(experimentParams);
		List<CaseParams> caseParams = params.getCaseParams();
		for (CaseParams c : caseParams) {
			selectExperimentNoRepair(params.getNodesFilename(),
					params.getRoadlinksFilename(),
					params.getResultOutputFilename(), c.getExperimentId(),
					c.getRadius(), c.getTargetThreshold(), c.getCenterId());
		}
	}

	private static void selectExperimentFromFile(String experimentParams)
			throws IOException {
		ExperimentParams params = readExperimentParams(experimentParams);
		List<CaseParams> caseParams = params.getCaseParams();
		for (CaseParams c : caseParams) {
			selectExperiment(params.getNodesFilename(),
					params.getRoadlinksFilename(),
					params.getRepairOutputFilename(),
					params.getResultOutputFilename(), c.getExperimentId(),
					c.getRadius(), c.getTargetThreshold(), c.getCenterId());
		}
	}

	private static void selectExperimentNoRepair(String nodesFilename,
			String roadLinksFilename, String resultOutput, String experimentId,
			String radiusString, String thresholdString, String centerId)
			throws IOException {
		List<Node> nodes = readNodes(nodesFilename);
		List<RoadLink> roadLinks = readRoadLinks(roadLinksFilename);
		Graph damagedGraph = new Graph(nodes, roadLinks);
		Node center = findNode(centerId, nodes);
		double radius = Double.parseDouble(radiusString);
		double targetThreshold = Double.parseDouble(thresholdString);
		LOGGER.info("Parameters - Radius: {} Threshold: {} Id: {}", radius,
				targetThreshold, experimentId);
		LOGGER.info("Center: " + center.toString());

		List<RoadLink> inRangeLinks = damagedGraph.inRangeLinks(center, radius);

		LOGGER.info("RL in range: " + inRangeLinks.size());

		// Must never print ERROR
		StringBuilder text = new StringBuilder("" + targetThreshold)
				.append(",").append(radius).append(",");

		BrokenRoadData brokenRoadData = damagedGraph.destroyLinksAround(center,
				radius, targetThreshold);
		List<Road> brokenRoads = brokenRoadData.getBrokenRoads();
		double actualThreshold = brokenRoadData.getActualThreshold();
		double distanceBroken = sumLength(brokenRoads);

		LOGGER.info("Destroyed: " + brokenRoads.size());
		int totalBroken = brokenRoads.size();
		LOGGER.info("Connectivity: " + damagedGraph.pathMatrixConnectivity());

		StringBuilder builder = new StringBuilder();
		builder.append(experimentId).append(",").append("NR").append(",")
				.append(totalBroken).append(",").append("NR").append(",")
				.append(distanceBroken).append(",").append(actualThreshold)
				.append("\n");
		text.append(builder.toString());

		appendToFile(resultOutput, text.toString());
	}

	private static void selectExperiment(String nodesFilename,
			String roadLinksFilename, String repairOutput, String resultOutput,
			String experimentId, String radiusString, String thresholdString,
			String centerId) throws IOException {
		List<Node> nodes = readNodes(nodesFilename);
		List<RoadLink> roadLinks = readRoadLinks(roadLinksFilename);
		Graph damagedGraph = new Graph(nodes, roadLinks);
		Node center = findNode(centerId, nodes);
		double radius = Double.parseDouble(radiusString);
		double targetThreshold = Double.parseDouble(thresholdString);
		LOGGER.info("Parameters - Radius: {} Threshold: {} Id: {}", radius,
				targetThreshold, experimentId);
		LOGGER.info("Center: " + center.toString());

		List<RoadLink> inRangeLinks = damagedGraph.inRangeLinks(center, radius);

		LOGGER.info("RL in range: " + inRangeLinks.size());

		// Must never print ERROR
		StringBuilder text = new StringBuilder("" + targetThreshold)
				.append(",").append(radius).append(",");

		BrokenRoadData brokenRoadData = damagedGraph.destroyLinksAround(center,
				radius, targetThreshold);
		List<Road> brokenRoads = brokenRoadData.getBrokenRoads();
		double actualThreshold = brokenRoadData.getActualThreshold();
		double distanceBroken = sumLength(brokenRoads);

		LOGGER.info("Destroyed: " + brokenRoads.size());
		int totalBroken = brokenRoads.size();
		LOGGER.info("Connectivity: " + damagedGraph.pathMatrixConnectivity());

		CondensedGraph condensed = damagedGraph.condense();
		LOGGER.info("Starting Repair");
		List<Road> repairPriority = condensed.repairPriority(brokenRoads);
		LOGGER.info("Num Repaired: " + repairPriority.size());
		LOGGER.info("Connectivity: " + damagedGraph.pathMatrixConnectivity());

		writeRoadsWithHeaderToFile(repairOutput, repairPriority, brokenRoads,
				"" + actualThreshold, "" + targetThreshold, "" + radius, ""
						+ experimentId);

		double distanceRepair = sumLength(repairPriority);

		StringBuilder builder = new StringBuilder();
		builder.append(experimentId).append(",").append(repairPriority.size())
				.append(",").append(totalBroken).append(",")
				.append(distanceRepair).append(",").append(distanceBroken)
				.append(",").append(actualThreshold).append("\n");
		text.append(builder.toString());

		appendToFile(resultOutput, text.toString());
	}

	private static Node findNode(String centerId, List<Node> nodes) {
		int centerIndex = Collections.binarySearch(nodes, new Node(centerId));
		Node center = nodes.get(centerIndex);
		return center;
	}

	private static void fractalDimension(String nodesFilename,
			String outputFilename, int maxZoom) throws IOException {
		List<Node> nodes = readNodes(nodesFilename);
		Fractal fractal = new Fractal();
		StringBuilder builder = new StringBuilder();
		int[] fractalDimensions = new int[maxZoom + 1];
		for (int i = 0; i <= maxZoom; i++) {
			// Reversing Fractal Dimensions
			fractalDimensions[maxZoom - i] = fractal.fractalDimension(nodes, i);
		}
		for (int i = 0; i <= maxZoom; i++) {
			builder.append(i).append(",").append(fractalDimensions[i])
					.append("\n");
		}
		writeStringToFile(builder.toString(), outputFilename);
	}

	private static void fractal(String nodesFilename, String outputFilename,
			int zoomLevel) throws IOException {
		List<Node> nodes = readNodes(nodesFilename);
		Fractal fractal = new Fractal();
		int[][] grid = fractal.fractal(nodes, zoomLevel);
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				if (j != 0) {
					builder.append(",");
				}
				builder.append(grid[i][j]);
			}
			builder.append("\n");
		}
		writeStringToFile(builder.toString(), outputFilename);
	}

	private static void randomFromFile(String nodes, String roadLinks,
			String inputParams) throws IOException {
		File file = new File(inputParams);
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] values = line.split(",");
				double radius = Double.parseDouble(values[0]);
				double threshold = Double.parseDouble(values[1]);
				int id = Integer.parseInt(values[2]);
				random(nodes, roadLinks, radius, threshold, id);
			}
		}
	}

	private static void cleanNoBuildingsPurgeSmallerComponents(String[] args)
			throws JAXBException, IOException {
		String[] cleanArgs = new String[5];
		for (int i = 1; i < 5; i++) {
			cleanArgs[i] = args[i];
		}
		// if configFolder was specified
		if (args.length > 7) {
			List<String> test = new ArrayList<>(Arrays.asList(cleanArgs));
			test.add(args[7]);
			cleanArgs = test.toArray(cleanArgs);
		}
		cleanNoBuildings(cleanArgs);
		String[] purgeArgs = new String[5];
		purgeArgs[1] = args[2];
		purgeArgs[2] = args[3];
		purgeArgs[3] = args[5];
		purgeArgs[4] = args[6];
		purgeSmallerComponents(purgeArgs);
	}

	private static void toRoad(List<RoadLink> roadLinks, String outFile)
			throws IOException {
		List<Road> roads = Road.toRoads(roadLinks);
		writeRoadsToFile(outFile, roads);
	}

	private static void writeRoadsToFile(String filename, List<Road> roads)
			throws IOException {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			String d = ";";
			bw.write("start" + d + "end" + d + "isTwoWay" + d + "name" + d
					+ "distance" + d + "lanes" + d + "type\n");
			for (Road road : roads) {
				bw.write(road.toString());
				bw.write("\n");
			}
		} catch (IOException e) {
			throw new IOException("Error Writing to File: " + filename, e);
		}
	}

	private static void printConnectivity(String[] args)
			throws FileNotFoundException {
		List<Node> nodes = readNodes(args[1]);
		List<RoadLink> roadLinks = readRoadLinks(args[2]);

		Graph graph = new Graph(nodes, roadLinks);
		double connectivity = graph.pathMatrixConnectivity();
		LOGGER.info("" + connectivity);
		System.out.println("" + connectivity);
	}

	private static void randomAll(String[] args) throws IOException {
		int initial = 5;
		int inc = 5;
		String[] thresholds = new String[10];
		thresholds[0] = "0.005";
		for (int i = 1; i < thresholds.length; i++) {
			thresholds[i] = "0.0" + (initial + (inc * i));
		}

		String[] currentArgs = new String[5];
		currentArgs[0] = args[0];
		currentArgs[1] = args[1];
		currentArgs[2] = args[2];
		currentArgs[3] = "";
		currentArgs[4] = args[3];
		// currentArgs[5] = args[4];
		for (int i = 0; i < thresholds.length; i++) {
			currentArgs[3] = thresholds[i];
			randomAllRadii(currentArgs);
		}
		LOGGER.info("EXECUTION COMPLETE");
		System.out.println("EXECUTION COMPLETE");
	}

	private static void randomAllRadii(String[] args) throws IOException {
		String[] radii = { "1", "2", "5", "10" };
		String[] currentArgs = new String[6];
		currentArgs[0] = args[0];
		currentArgs[1] = args[1];
		currentArgs[2] = args[2];
		currentArgs[3] = "";
		currentArgs[4] = args[3];
		currentArgs[5] = args[4];
		for (int i = 0; i < 4; i++) {
			currentArgs[3] = radii[i];
			randomMultiple(currentArgs);
		}
		LOGGER.info("THRESHOLD COMPLETE :" + args[3]);
		System.out.println("THRESHOLD COMPLETE :" + args[3]);
	}

	private static void randomMultipleFromFile(String[] args)
			throws IOException {
		String[] currentArgs = new String[6];
		currentArgs[0] = args[0];
		currentArgs[1] = args[1];
		currentArgs[2] = args[2];

		File file = new File(args[3]);
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] values = line.split(",");
				currentArgs[3] = values[0];
				currentArgs[4] = values[1];
				currentArgs[5] = values[2];
				randomMultiple(currentArgs);
			}
		}
	}

	private static void randomMultiple(String[] args) throws IOException {
		String nodes = args[1];
		String roadLinks = args[2];
		double radius = Double.parseDouble(args[3]);
		double threshold = Double.parseDouble(args[4]);
		int runs = Integer.parseInt(args[5]);
		for (int i = 1; i <= runs; i++) {
			int id = i;
			try {
				random(nodes, roadLinks, radius, threshold, id);
			} catch (ExperimentException e) {
				// Do nothing
			}
		}
	}

	private static void random(String nodesFilename, String roadLinksFilename,
			double radius, double threshold, int id) throws IOException {
		random(nodesFilename, roadLinksFilename, radius, threshold, id,
				DEFAULT_REPAIR_OUTPUT, DEFAULT_RESULT_OUTPUT);
	}

	private static void random(String nodesFilename, String roadLinksFilename,
			double radius, double threshold, int id, String repairOutput,
			String resultOutput) throws IOException {
		List<Node> nodes = readNodes(nodesFilename);
		List<RoadLink> roadLinks = readRoadLinks(roadLinksFilename);
		Graph damagedGraph = new Graph(nodes, roadLinks);
		int centerIndex = (int) (Math.random() * nodes.size());
		Node center = nodes.get(centerIndex);
		LOGGER.info("Parameters - Radius: {} Threshold: {} Id: {}", radius,
				threshold, id);
		LOGGER.info("Center: " + center.toString());

		List<RoadLink> inRangeLinks = damagedGraph.inRangeLinks(center, radius);

		LOGGER.info("RL in range: " + inRangeLinks.size());

		// Must never print ERROR
		StringBuilder text = new StringBuilder("" + threshold).append(",")
				.append(radius).append(",");

		try {
			BrokenRoadData brokenRoadData = damagedGraph.destroyLinksAround(
					center, radius, threshold);
			List<Road> brokenRoads = brokenRoadData.getBrokenRoads();
			double actualThreshold = brokenRoadData.getActualThreshold();
			double distanceBroken = sumLength(brokenRoads);

			LOGGER.info("Destroyed: " + brokenRoads.size());
			int totalBroken = brokenRoads.size();
			// for (RoadLink link : brokenRoads) {
			// assert (!link.getStartNode().equals(Node.EMPTY));
			// }
			LOGGER.info("Connectivity: "
					+ damagedGraph.pathMatrixConnectivity());

			CondensedGraph condensed = damagedGraph.condense();
			LOGGER.info("Starting Repair");
			List<Road> repairPriority = condensed.repairPriority(brokenRoads);
			LOGGER.info("Num Repaired: " + repairPriority.size());
			LOGGER.info("Connectivity: "
					+ damagedGraph.pathMatrixConnectivity());

			writeRoadsWithHeaderToFile(repairOutput, repairPriority,
					brokenRoads, "" + actualThreshold, "" + threshold, ""
							+ radius, "" + id);

			double distanceRepair = sumLength(repairPriority);

			StringBuilder builder = new StringBuilder();
			builder.append(id).append(",").append(repairPriority.size())
					.append(",").append(totalBroken).append(",")
					.append(distanceRepair).append(",").append(distanceBroken)
					.append("\n");
			text.append(builder.toString());

		} catch (ExperimentException e) {
			double distanceBroken = sumLength(Road.toRoads(inRangeLinks));

			StringBuilder builder = new StringBuilder();
			builder.append(id).append(",").append("0").append(",")
					.append(inRangeLinks.size()).append(",0,")
					.append(distanceBroken).append(",EXCEPTION").append("\n");
			text.append(builder.toString());
		} finally {
			appendToFile(resultOutput, text.toString());
		}
	}

	private static double sumLength(List<Road> repairPriority) {
		double distance = 0;
		for (Road road : repairPriority) {
			distance += road.getDistance().doubleValue();
		}
		return distance;
	}

	public static void appendToFile(String filename, String text)
			throws IOException {
		File file = new File(filename);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {
			bw.append(text);
		} catch (IOException e) {
			throw new IOException("Error Writing to File: " + filename, e);
		}
	}

	private static void straighten(String[] args) throws IOException {
		List<Node> nodes = readNodes(args[1]);
		List<RoadLink> roadLinks = readRoadLinks(args[2]);
		Graph graph = new Graph(nodes, roadLinks);
		graph.reduce();
		nodes = DataCleaner.purgeIsolatedNodes(nodes, roadLinks);
		writeNodesToFile(args[3], nodes);
		writeRoadLinksToFile(args[4], roadLinks);
	}

	private static void repairPriority(String[] args) throws IOException {
		List<Node> nodes = readNodes(args[1]);
		List<RoadLink> roadLinks = readRoadLinks(args[2]);
		List<RoadLink> brokenRoads = readRoadLinks(args[3]);
		Graph graph = new Graph(nodes, roadLinks);
		CondensedGraph condensed = graph.condense();
		List<Road> repairPriority = condensed.repairPriority(Road
				.toRoads(brokenRoads));
		writeRoadsToFileForRepair(args[4], repairPriority);
	}

	protected static void interactiveMode(InputStream inputStream, String[] args)
			throws IOException {
		List<Node> nodes = readNodes(args[1]);
		List<RoadLink> roadLinks = readRoadLinks(args[2]);
		Graph graph = new Graph(nodes, roadLinks);
		String command = "";
		System.out.println("Entered interactive mode.");
		System.out.println("Graph Loaded.");
		System.out.println(graph.status());
		System.out.println(showCommandList());
		try (Scanner scanner = new Scanner(inputStream)) {
			while (!"exit".equals(command)) {
				String nextLine = scanner.nextLine();
				String[] splitLine = nextLine.split(" ");
				command = splitLine[0];
				switch (command) {
				case "del":
					System.out.println("Removing edge...");
					graph.removeRoadLink(splitLine[1], splitLine[2]);
				case "status":
					System.out.println(graph.status());
					break;
				case "deleteRoadLinksByName":
					splitLine = nextLine.split("\"");
					List<RoadLink> brokenRoads = graph
							.findRoadLinksByName(splitLine[1]);
					graph.removeRoadLinks(brokenRoads);
					String[] outFiles = splitLine[2].trim().split(" ");
					writeRoadLinksToFile(outFiles[0], graph.getRoadLinks());
					writeRoadLinksToFile(outFiles[1].trim(), brokenRoads);
					break;
				case "findRoadLinksByName":
					splitLine = nextLine.split("\"");
					List<RoadLink> foundLinks = graph
							.findRoadLinksByName(splitLine[1]);
					writeRoadLinksToFile(splitLine[2].trim(), foundLinks);
					break;
				case "print":
					System.out.println("Writing to File.");
					writeRoadLinksToFile(splitLine[1], graph.getRoadLinks());
					break;
				default:
					break;
				}
			}
		}
	}

	private static String showCommandList() {
		StringBuilder builder = new StringBuilder();
		builder.append("Commands:\n");
		builder.append("del <nodeFrom> <nodeTo>\n");
		builder.append("deleteRoadLinksByName <\"roadName\"> <roadLinks> <brokenRoads>\n");
		builder.append("findRoadLinksByName <\"roadName\"> <outFile>\n");
		builder.append("print <roadlinksFilename>\n");
		builder.append("status\n");
		builder.append("exit\n");
		return builder.toString();
	}

	private static void getANDegree(String[] args) throws IOException {
		List<Node> nodes;
		List<RoadLink> roadLinks;
		if (args.length == 1) {
			nodes = readNodes(DEFAULT_NODES);
			roadLinks = readRoadLinks(DEFAULT_ROADLINKS);
		} else {
			nodes = readNodes(args[1]);
			roadLinks = readRoadLinks(args[2]);
		}

		int[] anDegrees = new int[nodes.size()];
		for (int i = 0; i < roadLinks.size(); i++) {
			RoadLink link = roadLinks.get(i);
			Node from = link.getStartNode();
			Node to = link.getEndNode();
			if (from.compareTo(to) < 0) {
				int fromIndex = Collections.binarySearch(nodes, from);
				anDegrees[fromIndex] = anDegrees[fromIndex] + 1;
				int toIndex = Collections.binarySearch(nodes, to);
				anDegrees[toIndex] = anDegrees[toIndex] + 1;
			}
		}

		StringBuilder builder = new StringBuilder();
		int endpoints = 0;
		int secondary = 0;
		int intersection = 0;
		for (int i : anDegrees) {
			builder.append(i).append("\n");
			if (i == 1) {
				endpoints++;
			} else if (i == 2) {
				secondary++;
			} else if (i >= 3) {
				intersection++;
			} else {
				// TODO: replace with more meaningful for backtrace
				LOGGER.error("There is an isolated node");
			}
		}
		StringBuilder b2 = new StringBuilder();
		b2.append("Primary: ").append(endpoints + intersection).append("\n");
		b2.append("\tEndpoints: ").append(endpoints).append("\n");
		b2.append("\tIntersections: ").append(intersection).append("\n");
		b2.append("Secondary: ").append(secondary).append("\n");
		writeStringToFile(builder.toString(), "target/output/an-degrees.txt");
		LOGGER.info(b2.toString());
	}

	private static void purgeSmallerComponents(String[] args)
			throws IOException {
		List<Node> nodes;
		List<RoadLink> roadLinks;
		if (args.length == 1) {
			nodes = readNodes(DEFAULT_NODES);
			roadLinks = readRoadLinks(DEFAULT_ROADLINKS);
		} else {
			nodes = readNodes(args[1]);
			roadLinks = readRoadLinks(args[2]);
		}
		List<Component> components = getComponents(nodes, roadLinks);
		List<Node> purgeList = createPurgeList(components);

		LOGGER.info("Started Purging nodes");
		for (Node n : purgeList) {
			int index = Collections.binarySearch(nodes, n);
			nodes.remove(index);
		}
		LOGGER.info("Finished Purging nodes");

		LOGGER.info("Started Purging roadLinks");
		int[] marker = new int[roadLinks.size()];
		LOGGER.info("Marking roadLinks");
		for (int i = 0; i < roadLinks.size(); i++) {
			RoadLink link = roadLinks.get(i);
			if (purgeList.contains(link.getStartNode())
					|| purgeList.contains(link.getEndNode())) {
				marker[i] = 1;
			}
		}
		LOGGER.info("Deleting roadLinks");
		for (int i = marker.length - 1; i >= 0; i--) {
			if (marker[i] == 1) {
				roadLinks.remove(i);
			}
		}
		LOGGER.info("Finished Purging roadLinks");
		writeNodesToFile(args[3], nodes);
		writeRoadLinksToFile(args[4], roadLinks);
	}

	private static List<Node> createPurgeList(List<Component> components) {
		List<Node> purgeList = new LinkedList<>();
		// Get all except the largest component
		for (int i = 0; i < components.size() - 1; i++) {
			Component component = components.get(i);
			for (Node n : component) {
				purgeList.add(n);
			}
		}
		Collections.sort(purgeList);
		return purgeList;
	}

	private static void listComponentsToFile(String[] args) throws IOException {
		List<Component> components = getComponents(args);
		writeStringToFile(formatComponents(components), args[3]);
		LOGGER.info("FINISHED");
	}

	private static List<Component> getComponents(String[] args)
			throws FileNotFoundException {
		List<Node> nodes;
		List<RoadLink> roadLinks;
		if (args.length == 1) {
			nodes = readNodes(DEFAULT_NODES);
			roadLinks = readRoadLinks(DEFAULT_ROADLINKS);
		} else {
			nodes = readNodes(args[1]);
			roadLinks = readRoadLinks(args[2]);
		}
		return getComponents(nodes, roadLinks);
	}

	private static List<Component> getComponents(List<Node> nodes,
			List<RoadLink> roadLinks) throws FileNotFoundException {
		TrajanAlgorithm trajan = new TrajanAlgorithm();
		List<Component> components = trajan.findComponents(nodes, roadLinks);
		LOGGER.info("Number of components:{}", components.size());

		Collections.sort(components);
		return components;
	}

	private static String formatComponents(List<Component> components) {
		int counter = 1;
		StringBuilder builder = new StringBuilder();
		for (Component component : components) {
			LOGGER.info("Component {}: {} nodes", counter, component.size());
			counter++;
			for (int i = 0; i < component.size(); i++) {
				Node n = component.get(i);
				if (i > 0) {
					builder.append(";");
				}
				builder.append(n);
			}
			builder.append("\n");
		}
		return builder.toString();
	}

	private static void cleanNoBuildings(String[] args)
			throws FileNotFoundException, JAXBException {
		// TODO create better validator
		validateArgs(args);

		String osmFilename = args[1];
		Osm osm = unmarshallOsm(osmFilename);
		DataCleaner cleaner = new DataCleaner(osm.getBounds());
		if (args.length < 3) {
			cleaner.extractRoadNetwork(osm.getWayList(), osm.getNodeList());
		} else if (args.length < 6) {
			String nodesFilename = args[2];
			String roadLinksFilename = args[3];
			cleaner.extractRoadNetwork(osm.getWayList(), osm.getNodeList(),
					nodesFilename, roadLinksFilename, args[4]);
		} else {
			String nodesFilename = args[2];
			String roadLinksFilename = args[3];
			cleaner.extractRoadNetwork(osm.getWayList(), osm.getNodeList(),
					nodesFilename, roadLinksFilename, args[4], args[5]);
		}
	}

	// TODO: Validate args
	// TODO: Overload by allowing user to specify distanceCalculator
	private static void toKilometers(String[] args) throws IOException {
		List<Node> nodes = readNodes(args[1]);
		Collections.sort(nodes);
		List<RoadLink> roadLinks = readRoadLinks(args[2]);
		Collections.sort(roadLinks);
		DistanceCalculator calc = new HalversineDistanceCalculator();
		for (RoadLink rl : roadLinks) {
			Node from = getData(rl.getStartNode(), nodes);
			Node to = getData(rl.getEndNode(), nodes);
			BigDecimal distance = calc.getDistance(from, to);
			rl.setDistance(distance);
		}
		// TODO: Consider a RoadLinkFormatter
		writeRoadLinksToFile(args[2], roadLinks);
	}

	public static void writeNodesToFile(String filename, List<Node> nodes)
			throws IOException {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write("Node\tlat\tlon\tname\n");
			for (Node n : nodes) {
				bw.write(n.getId());
				bw.write("\t");
				bw.write(String.format("%.7f", Double.parseDouble(n.getLat())));
				bw.write("\t");
				bw.write(String.format("%.7f", Double.parseDouble(n.getLon())));
				bw.write("\t");
				bw.write(n.getName());
				bw.write("\n");
			}
		} catch (IOException e) {
			throw new IOException("Error Writing to File: " + filename, e);
		}
	}

	private static void writeRoadsWithHeaderToFile(String filename,
			List<Road> roads, List<Road> brokenRoads, String actualThreshold,
			String targetThreshold, String radius, String id)
			throws IOException {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {
			String d = roads.get(0).getRoadLinks().get(0).getDelimiter();
			bw.write("Actual Threshold: " + actualThreshold + "\n");
			bw.write("Target Threshold: " + targetThreshold + "\n");
			bw.write("Radius: " + radius + "\n");
			bw.write(id + "\n");
			bw.write("Repaired Roads\n");
			bw.write("start" + d + "end" + d + "name" + d + "distance" + d
					+ "lanes\n");
			for (Road road : roads) {
				for (RoadLink rl : road.getRoadLinks()) {
					bw.write(rl.toString());
					bw.write("\n");
				}
			}
			bw.write("\n----------\n");
			bw.write("Destroyed Roads\n");
			bw.write("start" + d + "end" + d + "name" + d + "distance" + d
					+ "lanes\n");
			for (Road road : brokenRoads) {
				for (RoadLink rl : road.getRoadLinks()) {
					bw.write(rl.toString());
					bw.write("\n");
				}
			}
			bw.write("\n==========\n");
		} catch (IOException e) {
			throw new IOException("Error Writing to File: " + filename, e);
		}
	}

	public static void writeRoadLinksToFile(String filename,
			List<RoadLink> roadLinks) throws IOException {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			String d = roadLinks.get(0).getDelimiter();
			String header = "";
			if (roadLinks.get(0).getHighwayType() == null) {
				header = oldFormatHeader(d);
			} else {
				header = formatHeader(d);
			}
			bw.write(header);
			for (RoadLink rl : roadLinks) {
				bw.write(rl.toString());
				bw.write("\n");
			}
		} catch (IOException e) {
			throw new IOException("Error Writing to File: " + filename, e);
		}
	}

	private static String oldFormatHeader(String d) {
		StringBuilder builder = new StringBuilder();
		builder.append("start").append(d);
		builder.append("end").append(d);
		builder.append("name").append(d);
		builder.append("distance").append(d);
		builder.append("lanes").append("\n");
		return builder.toString();
	}

	private static String formatHeader(String d) {
		StringBuilder builder = new StringBuilder();
		builder.append("start").append(d);
		builder.append("end").append(d);
		builder.append("name").append(d);
		builder.append("distance").append(d);
		builder.append("lanes").append(d);
		builder.append("type").append("\n");
		return builder.toString();
	}

	public static void writeRoadsToFileForRepair(String filename,
			List<Road> roads) throws IOException {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			String d = roads.get(0).getRoadLinks().get(0).getDelimiter();
			bw.write("start" + d + "end" + d + "name" + d + "distance" + d
					+ "lanes\n");
			for (Road road : roads) {
				for (RoadLink rl : road.getRoadLinks()) {
					bw.write(rl.toString());
					bw.write("\n");
				}
			}
		} catch (IOException e) {
			throw new IOException("Error Writing to File: " + filename, e);
		}
	}

	private static Node getData(Node from, List<Node> nodes) {
		from = nodes.get(Collections.binarySearch(nodes, from));
		return from;
	}

	// TODO: Validate Args
	// TODO: Add logs
	private static void createAdjacencyList(String[] args) throws IOException {
		List<Node> nodes = readNodes(args[1]);
		List<RoadLink> roadLinks = readRoadLinks(args[2]);
		String output = AdjacencyListFormatter
				.formatAdjString(nodes, roadLinks);
		writeStringToFile(output, args[3]);
	}

	// TODO: This only creates a dummy node.
	// To create real nodes. Need to be in Graph
	protected static List<RoadLink> readRoadLinks(String roadLinksFilename)
			throws FileNotFoundException {
		File roadLinksFile = new File(roadLinksFilename);
		List<RoadLink> roadLinks = new ArrayList<>();
		try (Scanner scanner = new Scanner(roadLinksFile)) {
			scanner.nextLine();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				Node start = new Node(values[0]);
				Node end = new Node(values[1]);
				if (values.length == 6) {
					BigDecimal distance = new BigDecimal(
							values[values.length - 3]);
					int lanes = Integer.parseInt(values[values.length - 2]);
					Highway highwayType = getHighwayType(values[values.length - 1]);
					RoadLink roadLink = new RoadLink(start, end, values[2],
							distance, lanes, highwayType);
					roadLinks.add(roadLink);
				} else if (values.length == 5) {
					BigDecimal distance = new BigDecimal(
							values[values.length - 2]);
					int lanes = Integer.parseInt(values[values.length - 1]);
					RoadLink roadLink = new RoadLink(start, end, values[2],
							distance, lanes);
					roadLinks.add(roadLink);
				}
			}
		}
		return roadLinks;
	}

	private static Highway getHighwayType(String highwayType) {
		switch (highwayType.toLowerCase()) {
		case "tertiary":
			return Highway.TERTIARY;
		case "tertiary_link":
			return Highway.TERTIARY_LINK;
		case "secondary":
			return Highway.SECONDARY;
		case "secondary_link":
			return Highway.SECONDARY_LINK;
		case "primary":
			return Highway.PRIMARY;
		case "primary_link":
			return Highway.PRIMARY_LINK;
		case "trunk":
			return Highway.TRUNK;
		case "trunk_link":
			return Highway.TRUNK_LINK;
		case "motorway":
			return Highway.MOTORWAY;
		case "motorway_link":
			return Highway.MOTORWAY_LINK;
		case "service":
			return Highway.SERVICE;
		case "unclassified":
			return Highway.UNCLASSIFIED;
		}
		return null;
	}

	private static void writeStringToFile(String output, String filename)
			throws IOException {
		File file = new File(filename);
		forceMkdirParent(file);
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
			writer.write(output);
		}
	}

	private static void forceMkdirParent(File file) {
		if (file.getParent() != null) {
			File parent = new File(file.getParent());
			parent.mkdirs();
		}
	}

	private static void formatToJson(String[] args) throws IOException {
		List<Node> nodes = readNodes(args[1]);
		List<RoadLink> roadLinks = readRoadLinks(args[2]);
		String adjListContents = readFile(args[3]);
		String output = JsonFormatter.format(nodes, roadLinks, adjListContents);
		writeStringToFile(output, args[4]);
	}

	private static String readFile(String fileName)
			throws FileNotFoundException {
		StringBuilder builder = new StringBuilder();
		try (Scanner scanner = new Scanner(new File(fileName))) {
			while (scanner.hasNext()) {
				if (builder.length() > 0) {
					builder.append("\n");
				}
				builder.append(scanner.nextLine());
			}
		}
		return builder.toString();
	}

	private static void validateArgs(String[] args) {
		if (args.length < 1) {
			System.err.println("Must input OSM filename");
			System.exit(-1);
		}
	}

	private static Osm unmarshallOsm(String name) throws JAXBException,
			FileNotFoundException {
		JAXBContext context = JAXBContext.newInstance(Osm.class);
		Unmarshaller um = context.createUnmarshaller();
		FileReader reader = new FileReader(name);
		Osm osm = (Osm) um.unmarshal(reader);
		return osm;
	}

	public static ExperimentParams readExperimentParams(String filename)
			throws FileNotFoundException {
		ExperimentParams params = new ExperimentParams();
		try (Scanner scanner = new Scanner(new File(filename))) {
			params.setNodesFilename(scanner.nextLine());
			params.setRoadlinksFilename(scanner.nextLine());
			params.setRepairOutputFilename(scanner.nextLine());
			params.setResultOutputFilename(scanner.nextLine());
			while (scanner.hasNext()) {
				String[] line = scanner.nextLine().split(",");
				CaseParams singleCaseParams = new CaseParams();
				singleCaseParams.setExperimentId(line[0]);
				singleCaseParams.setRadius(line[1]);
				singleCaseParams.setTargetThreshold(line[2]);
				singleCaseParams.setCenterId(line[3]);
				params.addCaseParams(singleCaseParams);
			}
		}
		return params;
	}

	public static ExperimentParams readExperimentParamsNewPoint(String filename)
			throws FileNotFoundException {
		ExperimentParams params = new ExperimentParams();
		try (Scanner scanner = new Scanner(new File(filename))) {
			params.setNodesFilename(scanner.nextLine());
			params.setRoadlinksFilename(scanner.nextLine());
			params.setRepairOutputFilename(scanner.nextLine());
			params.setResultOutputFilename(scanner.nextLine());
			while (scanner.hasNext()) {
				String[] line = scanner.nextLine().split(",");
				CaseParams singleCaseParams = new CaseParams();
				singleCaseParams.setExperimentId(line[0]);
				singleCaseParams.setRadius(line[1]);
				singleCaseParams.setTargetThreshold(line[2]);
				singleCaseParams.setLat(line[3]);
				singleCaseParams.setLon(line[4]);
				params.addCaseParams(singleCaseParams);
			}
		}
		return params;
	}
}
