package osm;

public class GetDegreesCommandException extends RuntimeException {

	public GetDegreesCommandException() {
	}

	public GetDegreesCommandException(String arg0) {
		super(arg0);
	}

	public GetDegreesCommandException(Throwable arg0) {
		super(arg0);
	}

	public GetDegreesCommandException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public GetDegreesCommandException(String arg0, Throwable arg1,
			boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
