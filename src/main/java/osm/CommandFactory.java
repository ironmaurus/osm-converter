package osm;

import static osm.TestUtils.target;

public class CommandFactory {
	public static final String CLEAN_WB = "-cwb";
	public static final String CLEAN_NB = "-cnb";
	public static final String GET_DEGREES = "-gd";
	public static final String REPAIRED_ROAD_LEVEL_THREE_ANALYSIS = "-ra3";
	public static final String REPAIRED_ROAD_GENERAL_ANALYSIS = "-rag";
	public static final String SELECT_RANDOM_NODE_IDS = "-srni";
	private String inputOsm;
	private String outputNodes;
	private String outputRoadlinks;
	private String outputAdjList;

	public Command createCommand(String commandFlag, String[] args) {
		Command command = null;
		switch (commandFlag) {
		case CLEAN_WB:
			command = createCleanWithBuildingsCommand(args);
			break;
		case CLEAN_NB:
			command = createCleanNoBuildingsCommand(args);
			break;
		case GET_DEGREES:
			command = createGetDegreesCommand(args);
			break;
		case REPAIRED_ROAD_LEVEL_THREE_ANALYSIS:
			command = createRepairedRoadLevelThreeAnalysisCommand(args);
			break;
		case REPAIRED_ROAD_GENERAL_ANALYSIS:
			command = createRepairedRoadGeneralAnalysisCommand(args);
			break;
		case SELECT_RANDOM_NODE_IDS:
			command = createSelectRandomNodeIdsCommand(args);
			break;
		}
		return command;
	}

	private Command createCleanWithBuildingsCommand(String[] args) {
		Command command;
		setCleanCommandBasicParams(args);
		if (args.length < 6) {
			command = new CleanWithBuildingsCommand(inputOsm, outputNodes,
					outputRoadlinks, outputAdjList);
		} else {
			String configFolder = args[5];
			command = new CleanWithBuildingsCommand(inputOsm, outputNodes,
					outputRoadlinks, outputAdjList, configFolder);
		}
		return command;
	}

	private Command createCleanNoBuildingsCommand(String[] args) {
		Command command;
		setCleanCommandBasicParams(args);
		if (args.length < 6) {
			command = new CleanNoBuildingsCommand(inputOsm, outputNodes,
					outputRoadlinks, outputAdjList);
		} else {
			String configFolder = args[5];
			command = new CleanNoBuildingsCommand(inputOsm, outputNodes,
					outputRoadlinks, outputAdjList, configFolder);
		}
		return command;
	}

	private void setCleanCommandBasicParams(String... args) {
		inputOsm = args[1];
		outputNodes = args[2];
		outputRoadlinks = args[3];
		outputAdjList = args[4];
	}
	
	private Command createGetDegreesCommand(String[] args) {
		String inputNodesFilename = args[1];
		String inputRoadlinksFilename = args[2];
		String outputDegreesFilename = args[3];
		return new GetDegreesCommand(inputNodesFilename, inputRoadlinksFilename, outputDegreesFilename);
	}

	private Command createRepairedRoadLevelThreeAnalysisCommand(String[] args) {
		String repairedRoadsFilename = args[1];
		String outputFilename = args[2];
		return new RepairedRoadLevelThreeAnalysisCommand(repairedRoadsFilename,
				outputFilename);
	}

	private Command createRepairedRoadGeneralAnalysisCommand(String[] args) {
		String repairedRoadsFilename = args[1];
		String outputFilename = args[2];
		return new RepairedRoadGeneralAnalysisCommand(repairedRoadsFilename,
				outputFilename);
	}

	private Command createSelectRandomNodeIdsCommand(String[] args) {
		String inputNodesFilename = args[1];
		int numToSelect = Integer.parseInt(args[2]);
		String outputNodeIdsFilename = args[3];
		return new SelectRandomNodeIdsCommand(inputNodesFilename, numToSelect,
				outputNodeIdsFilename);
	}

}
