package osm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import osm.cleaner.RoadLink;
import osm.cleaner.RoadLink.Highway;

public class RepairedRoadsAnalyzer {

	public RoadLink readRoadLink(String test) {
		String[] params = test.split(";");

		Node start = new Node(params[0]);
		Node end = new Node(params[1]);
		String name = params[2];
		BigDecimal distance = new BigDecimal(params[3]);
		int lanes = Integer.parseInt(params[4]);
		Highway type = Highway.valueOf(params[5]);
		return new RoadLink(start, end, name, distance, lanes, type);
	}

	public boolean isRoadLink(String line) {
		String[] params = line.split(";");
		return params.length == 6;
	}

	public List<Analysis> analyze(String test) {
		List<Analysis> analyses = new ArrayList<>();
		try (Scanner scanner = new Scanner(test);) {
			while (scanner.hasNext()) {
				String threshold = "";
				String radius = "";
				String id = "";
				List<String> stringEdges = new ArrayList<>();

				threshold = scanner.nextLine();
				radius = scanner.nextLine();
				id = scanner.nextLine();
				scanner.nextLine();
				String strEdge = scanner.nextLine();
				while (isRoadLink(strEdge)) {
					stringEdges.add(strEdge);
					strEdge = scanner.nextLine();
				}
				scanner.nextLine();
				List<RoadLink> edges = new ArrayList<>();
				for (String e : stringEdges) {
					edges.add(readRoadLink(e));
				}

				analyses.add(new Analysis(threshold, radius, id, edges));
			}
		}
		return analyses;
	}

}
