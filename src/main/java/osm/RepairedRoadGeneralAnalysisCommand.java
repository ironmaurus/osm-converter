package osm;

import static osm.FileUtils.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class RepairedRoadGeneralAnalysisCommand implements Command {
	private String repairedRoadsFilename;
	private String outputFilename;
	private RepairedRoadsAnalyzer analyzer;

	public RepairedRoadGeneralAnalysisCommand(String repairedRoadsFilename,
			String outputFilename) {
		this.repairedRoadsFilename = repairedRoadsFilename;
		this.outputFilename = outputFilename;
		analyzer = new RepairedRoadsAnalyzer();
	}

	@Override
	public void execute() {
		try {
			String content = readFileToString(repairedRoadsFilename);
			List<Analysis> analyses = analyzer.analyze(content);
			writeGeneralAnalysisToFile(outputFilename, analyses);
		} catch (IOException e) {
			throw new RepairAnalysisCommandException(e);
		}
	}

	private void writeGeneralAnalysisToFile(String filename,
			List<Analysis> analyses) {
		try (PrintWriter writer = createPrintWriter(forceMkdirParent(filename))) {
			for (Analysis analysis : analyses) {
				writer.println(analysis.printGeneralInfo());
			}
		} catch (IOException e) {
			throw new RepairAnalysisCommandException("Error Writing to File: "
					+ filename, e);
		}
	}

}
