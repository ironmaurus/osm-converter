package osm;

public interface Command {
	void execute();
}
