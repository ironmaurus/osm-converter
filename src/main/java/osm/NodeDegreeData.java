package osm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import osm.cleaner.Baton;

public class NodeDegreeData {
	public List<Node> outNodes;
	public List<Baton> outBatons;
	public List<Node> inNodes;
	public List<Baton> inBatons;
	private Type type;
	
	public enum Type {
		ENDPOINT, INTERSECTION, ISLAND, NORMAL
	}

	public NodeDegreeData() {
		outNodes = new ArrayList<>();
		outBatons = new ArrayList<>();
		inNodes = new ArrayList<>();
		inBatons = new ArrayList<>();
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	// TODO: Add guard against duplicate entries
	public void addOutNode(Node outNode) {
		outNodes.add(outNode);
	}

	// TODO: Add guard against duplicate entries
	public void addInNode(Node inNode) {
		inNodes.add(inNode);
	}

	public List<Node> getOutNodes() {
		return outNodes;
	}

	public List<Node> getInNodes() {
		return inNodes;
	}

	public int getDegree() {
		Set<Node> nodes = new HashSet<>();
		nodes.addAll(inNodes);
		nodes.addAll(outNodes);
		return nodes.size();
	}

	public List<Baton> getOutBatons() {
		return outBatons;
	}

	public List<Baton> getInBatons() {

		return inBatons;
	}

	public void addOutBaton(Baton baton) {
		outBatons.add(baton);
	}

	public void addInBaton(Baton baton) {
		inBatons.add(baton);
	}

	public void removeOutNode(Node node) {
		outNodes.remove(node);
	}

	public void removeOutBaton(Baton baton) {
		outBatons.remove(baton);
	}

	public void removeInNode(Node node) {
		inNodes.remove(node);
	}

	public void removeInBaton(Baton baton) {
		inBatons.remove(baton);
	}
}