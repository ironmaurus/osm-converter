package osm;

public class RepairAnalysisCommandException extends RuntimeException {

	public RepairAnalysisCommandException() {
	}

	public RepairAnalysisCommandException(String msg) {
		super(msg);
	}

	public RepairAnalysisCommandException(Throwable e) {
		super(e);
	}

	public RepairAnalysisCommandException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public RepairAnalysisCommandException(String arg0, Throwable arg1,
			boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
