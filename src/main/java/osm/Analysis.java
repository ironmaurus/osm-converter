package osm;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import osm.cleaner.Road;
import osm.cleaner.RoadLink;
import osm.cleaner.RoadLink.Highway;

public class Analysis {
	private String threshold;
	private String radius;
	private String id;
	private List<RoadLink> edges;
	private List<Road> roads;
	private Map<Highway, Integer> types;
	private Map<Highway, BigDecimal> lengths;

	public Analysis(String threshold, String radius, String id,
			List<RoadLink> edges) {
		this.threshold = threshold;
		this.radius = radius;
		this.id = id;
		this.edges = edges;
		this.roads = Road.toRoads(edges);
		initMaps();
		processTypes();
	}

	private void initMaps() {
		types = new HashMap<>();
		for (Highway h : Highway.values()) {
			types.put(h, 0);
		}
		lengths = new HashMap<>();
		for (Highway h : Highway.values()) {
			lengths.put(h, BigDecimal.ZERO);
		}
	}

	private void processTypes() {
		for (Road r : roads) {
			Highway type = r.getHighwayType();
			increment(type);
			addToSumLength(type, r.getDistance());
		}
	}

	private void increment(Highway type) {
		int num = types.get(type);
		types.put(type, ++num);
	}

	private void addToSumLength(Highway type, BigDecimal distance) {
		BigDecimal length = lengths.get(type);
		length = length.add(distance);
		lengths.put(type, length);
	}

	public String getThreshold() {
		return threshold;
	}

	public String getRadius() {
		return radius;
	}

	public String getId() {
		return id;
	}

	public List<RoadLink> getEdges() {
		return edges;
	}

	public List<Road> getRoads() {
		return roads;
	}

	public int numOfMotorway() {
		return types.get(Highway.MOTORWAY);
	}

	public int numOfMotorwayLink() {
		return types.get(Highway.MOTORWAY_LINK);
	}

	public int numOfTrunk() {
		return types.get(Highway.TRUNK);
	}

	public int numOfTrunkLink() {
		return types.get(Highway.TRUNK_LINK);
	}

	public int numOfPrimary() {
		return types.get(Highway.PRIMARY);
	}

	public int numOfPrimaryLink() {
		return types.get(Highway.PRIMARY_LINK);
	}

	public String lengthOfMotorway() {
		return lengths.get(Highway.MOTORWAY).toString();
	}

	public String lengthOfMotorwayLink() {
		return lengths.get(Highway.MOTORWAY_LINK).toString();
	}

	public String lengthOfTrunk() {
		return lengths.get(Highway.TRUNK).toString();
	}

	public String lengthOfTrunkLink() {
		return lengths.get(Highway.TRUNK_LINK).toString();
	}

	public String lengthOfPrimary() {
		return lengths.get(Highway.PRIMARY).toString();
	}

	public String lengthOfPrimaryLink() {
		return lengths.get(Highway.PRIMARY_LINK).toString();
	}

	public String printLevelThreeTypeInfo() {
		StringBuilder builder = new StringBuilder();
		builder.append(threshold).append(";");
		builder.append(radius).append(";");
		builder.append(id).append(";");
		builder.append("" + types.get(Highway.MOTORWAY)).append(";");
		builder.append("" + types.get(Highway.MOTORWAY_LINK)).append(";");
		builder.append("" + types.get(Highway.TRUNK)).append(";");
		builder.append("" + types.get(Highway.TRUNK_LINK)).append(";");
		builder.append("" + types.get(Highway.PRIMARY)).append(";");
		builder.append("" + types.get(Highway.PRIMARY_LINK)).append(";");
		builder.append(lengths.get(Highway.MOTORWAY).toString()).append(";");
		builder.append(lengths.get(Highway.MOTORWAY_LINK).toString()).append(
				";");
		builder.append(lengths.get(Highway.TRUNK).toString()).append(";");
		builder.append(lengths.get(Highway.TRUNK_LINK).toString()).append(";");
		builder.append(lengths.get(Highway.PRIMARY).toString()).append(";");
		builder.append(lengths.get(Highway.PRIMARY_LINK).toString());
		return builder.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(threshold).append(";");
		builder.append(radius).append(";");
		builder.append(id).append(";");
		builder.append("" + types.get(Highway.MOTORWAY)).append(";");
		builder.append("" + types.get(Highway.MOTORWAY_LINK)).append(";");
		builder.append("" + types.get(Highway.TRUNK)).append(";");
		builder.append("" + types.get(Highway.TRUNK_LINK)).append(";");
		builder.append("" + types.get(Highway.PRIMARY)).append(";");
		builder.append("" + types.get(Highway.PRIMARY_LINK)).append(";");
		builder.append(lengths.get(Highway.MOTORWAY).toString()).append(";");
		builder.append(lengths.get(Highway.MOTORWAY_LINK).toString()).append(
				";");
		builder.append(lengths.get(Highway.TRUNK).toString()).append(";");
		builder.append(lengths.get(Highway.TRUNK_LINK).toString()).append(";");
		builder.append(lengths.get(Highway.PRIMARY).toString()).append(";");
		builder.append(lengths.get(Highway.PRIMARY_LINK).toString());
		return builder.toString();
	}

	public String printGeneralInfo() {
		StringBuilder builder = new StringBuilder();
		builder.append(threshold).append(";");
		builder.append(radius).append(";");
		builder.append(id).append(";");
		builder.append(edges.size()).append(";");
		builder.append(roads.size()).append(";");
		builder.append(sumRoadLengths());
		return builder.toString();
	}

	private String sumRoadLengths() {
		BigDecimal totalLength = BigDecimal.ZERO;
		for(BigDecimal length : lengths.values()) {
			totalLength = totalLength.add(length);
		}
		return totalLength.toPlainString();
	}

}
