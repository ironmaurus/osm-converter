package osm;

import java.util.ArrayList;
import java.util.List;

public class ExperimentParams {
	private String nodesFilename;
	private String roadlinksFilename;
	private String repairOutputFilename;
	private String resultOutputFilename;
	private List<CaseParams> caseParams;

	public ExperimentParams() {
		caseParams = new ArrayList<>();
	}

	public String getNodesFilename() {
		return nodesFilename;
	}

	public void addCaseParams(CaseParams caseParams) {
		this.caseParams.add(caseParams);
	}

	public void setNodesFilename(String nodesFilename) {
		this.nodesFilename = nodesFilename;
	}

	public String getRoadlinksFilename() {
		return roadlinksFilename;
	}

	public void setRoadlinksFilename(String roadlinksFilename) {
		this.roadlinksFilename = roadlinksFilename;
	}

	public String getRepairOutputFilename() {
		return repairOutputFilename;
	}

	public void setRepairOutputFilename(String repairOutputFilename) {
		this.repairOutputFilename = repairOutputFilename;
	}

	public String getResultOutputFilename() {
		return resultOutputFilename;
	}

	public void setResultOutputFilename(String resultOutputFilename) {
		this.resultOutputFilename = resultOutputFilename;
	}

	public List<CaseParams> getCaseParams() {
		return caseParams;
	}

	public void setCaseParams(List<CaseParams> caseParams) {
		this.caseParams = caseParams;
	}

}
