package osm;

import static org.junit.Assert.*;
import static osm.TestUtils.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class CleanNoBuildingsCommandTest {
	@Before
	public void setUp() {
		deleteFileInTarget("nodes-nb.txt");
		deleteFileInTarget("roadlinks-nb.txt");
		deleteFileInTarget("adj-list-nb.txt");
	}

	@Test
	public void simpleTest() throws FileNotFoundException {
		String inputOsm = resource("test-osm.osm");
		String expectedNodes = resource("nodes-nb-expected.csv");
		String outputNodes = target("nodes-nb.txt");
		String expectedRoadlinks = resource("roadlinks-nb-expected.csv");
		String outputRoadlinks = target("roadlinks-nb.txt");
		String expectedAdjList = resource("adj-list-nb-expected.csv");
		String outputAdjList = target("adj-list-nb.txt");

		CleanNoBuildingsCommand command = new CleanNoBuildingsCommand(inputOsm, outputNodes,
				outputRoadlinks, outputAdjList, CLEANER_CONFIG);
		command.execute();

		assertFileEquals(expectedNodes, outputNodes);
		assertFileEquals(expectedRoadlinks, outputRoadlinks);
		assertFileEquals(expectedAdjList, outputAdjList);
	}

	@Test
	public void defaultConfigTest() {
		String inputOsm = resource("test-osm.osm");
		String outputNodes = target("nodes-nb.txt");
		String outputRoadlinks = target("roadlinks-nb.txt");
		String outputAdjList = target("adj-list-nb.txt");
		CleanNoBuildingsCommand command = new CleanNoBuildingsCommand(inputOsm, outputNodes,
				outputRoadlinks, outputAdjList);
		assertEquals("", command.getConfigFolder());
	}

}
