package osm;

import static osm.TestUtils.assertFileEquals;
import static osm.TestUtils.deleteFileInTarget;
import static osm.TestUtils.resource;
import static osm.TestUtils.target;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class RepairedRoadLevelThreeAnalysisCommandTest {
	private static final String REPAIR_OUTPUT_LVL3 = "repair-output-LVL3.txt";

	@Before
	public void setUp() {
		deleteFileInTarget(REPAIR_OUTPUT_LVL3);
	}

	@Test
	public void simpleTest() throws FileNotFoundException {
		String repairedRoadsFilename = resource("repair.txt");
		String outputFilename = target(REPAIR_OUTPUT_LVL3);
		Command command = new RepairedRoadLevelThreeAnalysisCommand(repairedRoadsFilename, outputFilename);
		command.execute();
		String expectedOutput = resource(REPAIR_OUTPUT_LVL3);	
		assertFileEquals(expectedOutput, outputFilename);
	}

}
