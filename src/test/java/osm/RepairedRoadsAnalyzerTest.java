package osm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.cleaner.RoadLink;

public class RepairedRoadsAnalyzerTest {
	private RepairedRoadsAnalyzer analyzer;
	private List<Analysis> analyses;

	@Before
	public void setUp() {
		analyzer = new RepairedRoadsAnalyzer();
		analyses = new ArrayList<>();
	}

	@Test
	public void toStringTest() {
		String test = case1() + case2();
		List<Analysis> analyses = new ArrayList<>();
		analyses = analyzer.analyze(test);
		Analysis first = analyses.get(0);
		assertEquals("0.005;10.0;1;0;0;0;0;1;0;0;0;0;0;0.0105;0", first.toString());
		Analysis second = analyses.get(1);
		assertEquals("0.005;10.0;2;0;0;5;2;0;1;0;0;0.3597;0.0196;0;0.0141", second.toString());
	}

	@Test
	public void tripleIterationTest() {
		String test = case1() + case2() + case3();
		analyses = analyzer.analyze(test);
		assertEquals(3, analyses.size());
		Analysis first = analyses.get(0);
		assertHeader("0.005", "10.0", "1", 2, 1, first);
		assertNumTypes(first, 0, 0, 0, 0, 1, 0);
		assertLengthTypes(first, "0", "0", "0", "0", "0.0105", "0");
		Analysis second = analyses.get(1);
		assertHeader("0.005", "10.0", "2", 8, 8, second);
		assertNumTypes(second, 0, 0, 5, 2, 0, 1);
		assertLengthTypes(second, "0", "0", "0.3597", "0.0196", "0", "0.0141");
		Analysis third = analyses.get(2);
		assertHeader("0.005", "10.0", "3", 0, 0, third);
		assertNumTypes(third, 0, 0, 0, 0, 0, 0);
		assertLengthTypes(third, "0", "0", "0", "0", "0", "0");
	}

	@Test
	public void noRoadsTest() {
		String test = case3();
		analyses = analyzer.analyze(test);
		assertEquals(1, analyses.size());
		Analysis third = analyses.get(0);
		assertHeader("0.005", "10.0", "3", 0, 0, third);
		assertNumTypes(third, 0, 0, 0, 0, 0, 0);
		assertLengthTypes(third, "0", "0", "0", "0", "0", "0");
	}

	@Test
	public void doubleIterationTest() {
		String test = case1() + case2();
		analyses = analyzer.analyze(test);
		assertEquals(2, analyses.size());
		Analysis first = analyses.get(0);
		assertHeader("0.005", "10.0", "1", 2, 1, first);
		assertNumTypes(first, 0, 0, 0, 0, 1, 0);
		assertLengthTypes(first, "0", "0", "0", "0", "0.0105", "0");
		Analysis second = analyses.get(1);
		assertHeader("0.005", "10.0", "2", 8, 8, second);
		assertNumTypes(second, 0, 0, 5, 2, 0, 1);
		assertLengthTypes(second, "0", "0", "0.3597", "0.0196", "0", "0.0141");
	}

	@Test
	public void singleIterationTest() {
		String test = case1();
		analyses = analyzer.analyze(test);
		assertEquals(1, analyses.size());
		Analysis first = analyses.get(0);
		assertHeader("0.005", "10.0", "1", 2, 1, first);
		assertNumTypes(first, 0, 0, 0, 0, 1, 0);
		assertLengthTypes(first, "0", "0", "0", "0", "0.0105", "0");
	}

	private void assertHeader(String threshold, String radius, String id, int numEdges, int numRoads, Analysis analysis) {
		assertEquals(threshold, analysis.getThreshold());
		assertEquals(radius, analysis.getRadius());
		assertEquals(id, analysis.getId());
		assertEquals(numEdges, analysis.getEdges().size());
		assertEquals(numRoads, analysis.getRoads().size());
	}

	private void assertLengthTypes(Analysis analysis, String... expected) {
		assertEquals(expected[0], analysis.lengthOfMotorway());
		assertEquals(expected[1], analysis.lengthOfMotorwayLink());
		assertEquals(expected[2], analysis.lengthOfTrunk());
		assertEquals(expected[3], analysis.lengthOfTrunkLink());
		assertEquals(expected[4], analysis.lengthOfPrimary());
		assertEquals(expected[5], analysis.lengthOfPrimaryLink());
	}

	private void assertNumTypes(Analysis analysis, int... expected) {
		assertEquals(expected[0], analysis.numOfMotorway());
		assertEquals(expected[1], analysis.numOfMotorwayLink());
		assertEquals(expected[2], analysis.numOfTrunk());
		assertEquals(expected[3], analysis.numOfTrunkLink());
		assertEquals(expected[4], analysis.numOfPrimary());
		assertEquals(expected[5], analysis.numOfPrimaryLink());
	}

	@Test
	public void hasNextLinkTest() {
		String line1 = "32428546;596957912;M.H. Del Pilar Ave.;0.0105;2;PRIMARY";
		String line2 = "\n";
		assertTrue(analyzer.isRoadLink(line1));
		assertFalse(analyzer.isRoadLink(line2));
	}

	@Test
	public void namedTest() {
		String test = "32428546;596957912;M.H. Del Pilar Ave.;0.0105;2;PRIMARY";
		RoadLink roadLink = analyzer.readRoadLink(test);
		assertEquals("32428546", roadLink.getStartNode().getId());
		assertEquals("596957912", roadLink.getEndNode().getId());
		assertEquals("M.H. Del Pilar Ave.", roadLink.getName());
		assertEquals("0.0105", roadLink.getDistance().toString());
		assertEquals(2, roadLink.getLanes());
		assertEquals("PRIMARY", roadLink.getHighwayType().toString());
	}

	@Test
	public void namelessTest() {
		String test = "32428546;596957912;;0.0105;2;PRIMARY";
		RoadLink roadLink = analyzer.readRoadLink(test);
		assertEquals("32428546", roadLink.getStartNode().getId());
		assertEquals("596957912", roadLink.getEndNode().getId());
		assertEquals("", roadLink.getName());
		assertEquals("0.0105", roadLink.getDistance().toString());
		assertEquals(2, roadLink.getLanes());
		assertEquals("PRIMARY", roadLink.getHighwayType().toString());
	}

	private String case1() {
		StringBuilder builder = new StringBuilder();
		builder.append(header("0.005", "10.0", "1"));
		builder.append("32428546;596957912;M.H. Del Pilar Ave.;0.0105;2;PRIMARY").append("\n");
		builder.append("596957912;32428546;M.H. Del Pilar Ave.;0.0105;2;PRIMARY").append("\n");
		builder.append(footer());
		return builder.toString();
	}

	private String case2() {
		StringBuilder builder = new StringBuilder();
		builder.append(header("0.005", "10.0", "2"));
		builder.append("312264226;1074549762;Katipunan Avenue;0.2163;2;TRUNK").append("\n");
		builder.append("2307304052;313463079;Guadalupe Cloverleaf to JP Rizal;0.0103;2;TRUNK_LINK").append("\n");
		builder.append("2466216118;33466357;Commonwealth Avenue;0.0129;6;TRUNK").append("\n");
		builder.append("31462129;31462130;;0.0614;2;TRUNK").append("\n");
		builder.append("2331992410;2331992413;Shaw Tunnel;0.0473;3;TRUNK").append("\n");
		builder.append("503317062;503317146;;0.0141;2;PRIMARY_LINK").append("\n");
		builder.append("1141832074;1141832062;;0.0093;2;TRUNK_LINK").append("\n");
		builder.append("2066765303;224865455;Epifanio de los Santos Avenue;0.0218;3;TRUNK").append("\n");
		builder.append(footer());
		return builder.toString();
	}

	private String case3() {
		StringBuilder builder = new StringBuilder();
		builder.append(header("0.005", "10.0", "3"));
		builder.append(footer());
		return builder.toString();
	}

	private String header(String threshold, String radius, String id) {
		StringBuilder builder = new StringBuilder();
		builder.append(threshold).append("\n");
		builder.append(radius).append("\n");
		builder.append(id).append("\n");
		builder.append("start;end;name;distance;lanes").append("\n");
		return builder.toString();
	}

	private String footer() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n");
		builder.append("==========").append("\n");
		return builder.toString();
	}
}
