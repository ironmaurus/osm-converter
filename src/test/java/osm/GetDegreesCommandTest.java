package osm;

import static org.junit.Assert.*;
import static osm.TestUtils.*;
import static osm.CommandFactory.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class GetDegreesCommandTest {
	private CommandFactory commandFactory;
	
	@Before
	public void setUp() {
		commandFactory = new CommandFactory();
	}

	@Test
	public void test() {
		
	}
	
	@Test
	public void getDegreesTest() throws FileNotFoundException {
		String command = GET_DEGREES;
		String inputNodesFilename = resource("nodes-02.txt");
		String inputRoadlinksFilename = resource("roadlinks-02.txt");
		String outputDegreesFilename = target("degrees.csv");
		String expectedOutput = resource("degrees-01.csv");
		Command actualCommand = createCommand(command, inputNodesFilename,
				inputRoadlinksFilename, outputDegreesFilename);
		assertEquals(GetDegreesCommand.class, actualCommand.getClass());
		actualCommand.execute();
		assertFileEquals(expectedOutput, outputDegreesFilename);
	}
	
	private Command createCommand(String command, String... args) {
		String[] argsWithCommand = new String[args.length + 1];
		// Prepend args with command
		argsWithCommand[0] = command;
		for (int i = 0; i < args.length; i++) {
			argsWithCommand[i + 1] = args[i];
		}
		return commandFactory.createCommand(command, argsWithCommand);
	}

}
