package osm;

import static org.junit.Assert.*;
import static osm.TestUtils.*;
import static osm.CommandFactory.*;

import org.junit.Before;
import org.junit.Test;

public class CommandFactoryTest {
	private static final String TEST_OSM = resource("test-osm.osm");
	private CommandFactory commandFactory;

	@Before
	public void setUp() {
		commandFactory = new CommandFactory();
	}

	@Test
	public void getDegreesTest() {
		String command = GET_DEGREES;
		String inputNodesFilename = resource("nodes-02.txt");
		String inputRoadlinksFilename = resource("roadlinks-02.txt");
		String outputDegreesFilename = target("degrees.csv");
		Command actualCommand = createCommand(command, inputNodesFilename,
				inputRoadlinksFilename, outputDegreesFilename);
		assertEquals(GetDegreesCommand.class, actualCommand.getClass());
	}

	@Test
	public void selectRandomNodeIdsTest() {
		String command = SELECT_RANDOM_NODE_IDS;
		String inputNodesFilename = resource("nodes.txt");
		String numToSelect = "5";
		String outputNodeIdsFilename = target("nodeIds.csv");
		Command actualCommand = createCommand(command, inputNodesFilename,
				numToSelect, outputNodeIdsFilename);
		assertEquals(SelectRandomNodeIdsCommand.class, actualCommand.getClass());
	}

	@Test
	public void repairedRoadGeneralAnalysisTest() {
		String command = REPAIRED_ROAD_GENERAL_ANALYSIS;
		String repairedRoadsFilename = resource("repair.txt");
		String outputFilename = target("repair-output-G.txt");
		Command actualCommand = createCommand(command, repairedRoadsFilename,
				outputFilename);
		assertEquals(RepairedRoadGeneralAnalysisCommand.class,
				actualCommand.getClass());
	}

	@Test
	public void repairedRoadAnalysisLevelThreeTest() {
		String command = REPAIRED_ROAD_LEVEL_THREE_ANALYSIS;
		String repairedRoadsFilename = resource("repair.txt");
		String outputFilename = target("repair-output-LVL3.txt");
		Command actualCommand = createCommand(command, repairedRoadsFilename,
				outputFilename);
		assertEquals(RepairedRoadLevelThreeAnalysisCommand.class,
				actualCommand.getClass());
	}

	@Test
	public void cleanNoBuildingsSetConfigFolderTest() {
		String command = CLEAN_NB;
		String inputOsm = TEST_OSM;
		String outputNodes = target("nodes-nb.txt");
		String outputRoadlinks = target("roadlinks-nb.txt");
		String outputAdjList = target("adj-list-nb.txt");
		Command actualCommand = createCommand(command, inputOsm, outputNodes,
				outputRoadlinks, outputAdjList, CLEANER_CONFIG);
		assertEquals(CleanNoBuildingsCommand.class, actualCommand.getClass());
		assertEquals(CLEANER_CONFIG,
				((CleanNoBuildingsCommand) actualCommand).getConfigFolder());
	}

	@Test
	public void cleanNoBuildingsDefaultConfigFolderTest() {
		String command = CLEAN_NB;
		String inputOsm = TEST_OSM;
		String outputNodes = target("nodes.txt");
		String outputRoadlinks = target("roadlinks.txt");
		String outputAdjList = target("adj-list.txt");
		Command actualCommand = createCommand(command, inputOsm, outputNodes,
				outputRoadlinks, outputAdjList);
		assertEquals(CleanNoBuildingsCommand.class, actualCommand.getClass());
		assertEquals("",
				((CleanNoBuildingsCommand) actualCommand).getConfigFolder());
	}

	@Test
	public void cleanWithBuildingsSetConfigFolderTest() {
		String command = CLEAN_WB;
		String inputOsm = TEST_OSM;
		String outputNodes = target("nodes.txt");
		String outputRoadlinks = target("roadlinks.txt");
		String outputAdjList = target("adj-list.txt");
		Command actualCommand = createCommand(command, inputOsm, outputNodes,
				outputRoadlinks, outputAdjList, CLEANER_CONFIG);
		assertEquals(CleanWithBuildingsCommand.class, actualCommand.getClass());
		assertEquals(CLEANER_CONFIG,
				((CleanWithBuildingsCommand) actualCommand).getConfigFolder());
	}

	@Test
	public void cleanWithBuildingsDefaultConfigFolderTest() {
		String command = CLEAN_WB;
		String inputOsm = TEST_OSM;
		String outputNodes = target("nodes.txt");
		String outputRoadlinks = target("roadlinks.txt");
		String outputAdjList = target("adj-list.txt");
		Command actualCommand = createCommand(command, inputOsm, outputNodes,
				outputRoadlinks, outputAdjList);
		assertEquals(CleanWithBuildingsCommand.class, actualCommand.getClass());
		assertEquals("",
				((CleanWithBuildingsCommand) actualCommand).getConfigFolder());
	}

	private Command createCommand(String command, String... args) {
		String[] argsWithCommand = new String[args.length + 1];
		// Prepend args with command
		argsWithCommand[0] = command;
		for (int i = 0; i < args.length; i++) {
			argsWithCommand[i + 1] = args[i];
		}
		return commandFactory.createCommand(command, argsWithCommand);
	}
}
