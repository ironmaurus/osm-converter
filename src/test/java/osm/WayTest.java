package osm;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class WayTest {
	private Way way;
	private ArrayList<Tag> tags; 
	
	@Before
	public void setUp() {
		way = new Way();
		tags = new ArrayList<>();
		way.setTagList(tags);
	}
	
	@Test
	public void getLanesJunkTest() {
		tags.add(new Tag("lanes", "4; 3; 4"));
		assertEquals(4, way.getLanes());
	}

	@Test
	public void getNameTest() {
		tags.add(new Tag("name", "North Highway"));	
		assertEquals("North Highway", way.getName());
	}
}
