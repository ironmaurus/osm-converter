package osm;

import static osm.CommandFactory.*;
import static osm.TestUtils.assertFileEquals;
import static osm.TestUtils.resource;
import static osm.TestUtils.target;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Ignore;
import org.junit.Test;

/*
 * These tests double as preloaded program parameters.
 */
public class OsmMainIntegrationTest {
	// TODO: Replace with Test Resources LOG4J Properties
	private static final String LOG4J_PROPERTIES = "log4j.properties";
	private static final String INPUT = "input/";
	private static final String OUTPUT = "output/";

	@Test
	@Ignore
	public void getDegreesTest() throws JAXBException, IOException {
		String command = GET_DEGREES;
		String inputNodesFilename = input("nodes-nb-mtpst-c.csv");
		String inputRoadlinksFilename = input("roadlinks-nb-mtpst-c.txt");
		String outputDegreesFilename = output("degrees.csv");
		String[] args = { command, inputNodesFilename, inputRoadlinksFilename,
				outputDegreesFilename };
		OsmMain.main(args);
	}

	@Test
	@Ignore
	public void selectRandomNodeIdsTest() throws JAXBException, IOException {
		String command = SELECT_RANDOM_NODE_IDS;
		String inputNodesFilename = input("nodes-nb-mtpst-c.csv");
		String numToSelect = "1000";
		String outputNodeIdsFilename = output("nodeIds-200.csv");
		String[] args = { command, inputNodesFilename, numToSelect,
				outputNodeIdsFilename };
		OsmMain.main(args);
	}

	@Test
	@Ignore
	// Ensure that a valid osm-file is in input/ before activating this test.
	public void cleanNoBuildingsTest() throws IOException, JAXBException {
		String command = CLEAN_NB;
		String osm = input("manila-2.osm");
		String nodes = output("nodes-nb-mtpst.csv");
		String roadlinks = output("roadlinks-nb-mtpst.csv");
		String adjList = output("adjList-nb-mtpst.csv");
		String[] args = { command, osm, nodes, roadlinks, adjList };
		OsmMain.main(args);
	}

	@Test
	@Ignore
	// Ensure that a valid osm-file is in input/ before activating this test.
	// Set the arguments in used.txt
	public void cleanNoBuildingsPurgeSmallerComponentsTest()
			throws IOException, JAXBException {
		String roadDetail = "m";
		String command = "-skp";
		String osm = input("manila-2.osm");
		String nodes = output("nodes-nb-" + roadDetail + ".csv");
		String roadlinks = output("roadlinks-nb-" + roadDetail + ".csv");
		String adjList = output("adjList-nb-" + roadDetail + ".csv");
		String purgedNodes = output("nodes-nb-" + roadDetail + "-c.csv");
		String purgedRoadLinks = output("roadlinks-nb-" + roadDetail + "-c.csv");
		String[] args = { command, osm, nodes, roadlinks, adjList, purgedNodes,
				purgedRoadLinks };
		OsmMain.main(args);
	}

	@Test
	@Ignore
	public void fractalTest() throws IOException, JAXBException {
		String roadDetail = "mtpst";
		for (int i = 0; i < 13; i++) {
			int zoom = i;
			String command = "-f";
			String nodes = input("nodes-nb-" + roadDetail + "-c.csv");
			String output = output("fractal-" + roadDetail + "-" + zoom
					+ ".csv");
			String zoomLevel = "" + zoom;
			String[] args = { command, nodes, output, zoomLevel };
			OsmMain.main(args);
		}
	}

	@Test
	@Ignore
	public void fractalDimensionTest() throws IOException, JAXBException {
		String roadDetail = "m";
		String command = "-fd";
		String nodes = input("nodes-nb-" + roadDetail + "-c.csv");
		String maxZoom = "12";
		String output = output("fractal-dimension-" + maxZoom + "-"
				+ roadDetail + ".csv");
		String[] args = { command, nodes, output, maxZoom };
		OsmMain.main(args);

	}

	@Test
	@Ignore
	public void repairAnalysisTest() throws IOException, JAXBException {
		String command = "-ra";
		String repair = input("REPAIR.txt");
		String output = output("repair-output.txt");
		String[] args = { command, repair, output };
		OsmMain.main(args);
	}

	@Test
	@Ignore
	public void bigMapTest() throws JAXBException, IOException {
		PropertyConfigurator.configure(LOG4J_PROPERTIES);
		String command = "-er";
		String nodes = input("nodes-nb-mtp-c.txt");
		String roadlinks = input("roadlinks-nb-mtp-c.txt");
		String radius = "10";
		String threshold = "0.05";
		String id = "5";
		String[] args = { command, nodes, roadlinks, radius, threshold, id };
		OsmMain.main(args);
	}

	@Test
	@Ignore
	public void toRoadStronglyConnectedTest() throws JAXBException, IOException {
		PropertyConfigurator.configure(LOG4J_PROPERTIES);
		String command = "-tr";
		String roadlinks = input("roadlinks-nb-mtpst-c.txt");
		String output = output("roads-lvl5.csv");
		String[] args = { command, roadlinks, output };
		OsmMain.main(args);
	}

	@Test
	@Ignore
	public void toRoadTest() throws JAXBException, IOException {
		PropertyConfigurator.configure(LOG4J_PROPERTIES);
		String command = "-tr";
		String roadlinks = input("roadlinks-nb-mtpst.txt");
		String output = output("roads-mtpst.csv");
		String[] args = { command, roadlinks, output };
		OsmMain.main(args);
	}

	private String input(String filename) {
		return INPUT + filename;
	}

	private String output(String filename) {
		return OUTPUT + filename;
	}
}
