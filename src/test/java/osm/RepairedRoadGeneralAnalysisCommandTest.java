package osm;

import static osm.TestUtils.assertFileEquals;
import static osm.TestUtils.deleteFileInTarget;
import static osm.TestUtils.resource;
import static osm.TestUtils.target;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class RepairedRoadGeneralAnalysisCommandTest {
	@Before
	public void setUp() {
		deleteFileInTarget("repair-output.txt");
	}

	@Test
	public void simpleTest() throws FileNotFoundException {
		String repairedRoadsFilename = resource("repair.txt");
		String outputFilename = target("repair-output-G.txt");
		Command command = new RepairedRoadGeneralAnalysisCommand(repairedRoadsFilename, outputFilename);
		command.execute();
		String expectedOutput = resource("repair-output-G.txt");	
		assertFileEquals(expectedOutput, outputFilename);
	}

}
