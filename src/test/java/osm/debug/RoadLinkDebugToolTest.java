package osm.debug;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class RoadLinkDebugToolTest {
	private static final String DEBUG_FILE = "debug";
	private static final String OSM_FILE = "qc-circle.osm";
	private RoadLinkDebugTool rldt;
	
	@Before
	public void setUp() {
		rldt = new RoadLinkDebugTool();
	}

	@Test
	@Ignore
	public void readInputTest() throws FileNotFoundException {
		rldt.readInput(DEBUG_FILE);
		assertEquals("12067440", rldt.getFromString());
		assertEquals(266, rldt.getToString().size());
	}

	@Test
	@Ignore
	public void findWayInfoTest() throws FileNotFoundException, JAXBException {
		rldt.readInput(DEBUG_FILE);
		rldt.unmarshallWays(OSM_FILE);
		
		List<String> nodes = new LinkedList<>();
		nodes.add(rldt.getFromString());
		nodes.addAll(rldt.getToString());
		
		List<NodeInfo> nodeInfos = new LinkedList<>();
		for(String n: nodes) {
			List<String> info = rldt.findWayInfo(n);
			nodeInfos.add(new NodeInfo(n, info));
		}
		
		for(NodeInfo n : nodeInfos) {
			System.out.println(n.toStringTable());
		}
	}
}
