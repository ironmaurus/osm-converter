package osm.cleaner;

import static org.junit.Assert.*;

import java.math.*;

import org.junit.Before;
import org.junit.Test;

import osm.Node;
import osm.cleaner.RoadLink.Highway;

public class RoadLinkTest {
	private RoadLink roadLink;

	@Before
	public void setUp() {

	}
	
	@Test
	public void autoDistanceTest() {
		Node node1 = new Node("1", "14.6525517", "121.0327874");
		Node node5 = new Node("5", "14.634", "121.0327874");
		RoadLink link = new RoadLink(node1, node5);
		assertEquals(new BigDecimal("2.0629"), link.getDistance());
	}
	
	@Test
	public void defaultNamingTest() {
		Node nodeA = new Node("A", "14.6525517", "121.0327874");
		Node nodeB = new Node("B", "14.634", "121.0327874");
		roadLink = new RoadLink(nodeA, nodeB);
		assertEquals(nodeA, roadLink.getStartNode());
		assertEquals(nodeB, roadLink.getEndNode());
		assertEquals("AB", roadLink.getName());
		assertEquals(new BigDecimal("2.0629"), roadLink.getDistance());
	}
	
	@Test
	public void toStringHighwayTest() {
		roadLink = new RoadLink(new Node("11"), new Node("12"),
				"North Highway", BigDecimal.ONE.setScale(7), 4, Highway.PRIMARY);
		assertEquals("11;12;North Highway;1.0000000;4;PRIMARY", roadLink.toString());
	}
	
	@Test
	public void toStringLanesTest() {
		roadLink = new RoadLink(new Node("11"), new Node("12"),
				"North Highway", BigDecimal.ONE.setScale(7), 4);
		assertEquals("11;12;North Highway;1.0000000;4", roadLink.toString());
	}

	@Test
	public void toStringTest() {
		roadLink = new RoadLink(new Node("11"), new Node("12"),
				"North Highway", BigDecimal.ONE.setScale(7));
		assertEquals("11;12;North Highway;1.0000000;2", roadLink.toString());
	}

	@Test
	public void delimiterTest() {
		roadLink = new RoadLink(new Node("11"), new Node("12"),
				"North Highway", BigDecimal.ONE.setScale(7), ",");
		assertEquals("11,12,North Highway,1.0000000,2", roadLink.toString());
	}

	@Test
	public void getSlopeTest() {
		createPositiveSlopeLine();
		assertEquals(new BigDecimal("1.0000000"), roadLink.getSlope());
	}

	private void createPositiveSlopeLine() {
		roadLink = new RoadLink(new Node("7", "5", "1"),
				new Node("6", "7", "3"), "Positive Slope", BigDecimal.valueOf(
						Math.sqrt(8)).setScale(7, BigDecimal.ROUND_HALF_DOWN));
	}

	@Test
	public void isVerticalTest() {
		createVerticalLine();
		assertTrue(roadLink.isVertical());
	}

	private void createVerticalLine() {
		roadLink = new RoadLink(new Node("3", "1", "1"),
				new Node("1", "3", "1"), "Vertical Slope", BigDecimal
						.valueOf(2).setScale(7, BigDecimal.ROUND_HALF_DOWN));
	}

	@Test
	public void isHorizontalTest() {
		createHorizontalLine();
		assertTrue(roadLink.isHorizontal());
	}

	private void createHorizontalLine() {
		roadLink = new RoadLink(new Node("2", "3", "3"),
				new Node("1", "3", "1"), "Horizontal Slope", BigDecimal
						.valueOf(2).setScale(7, BigDecimal.ROUND_HALF_DOWN));
	}

	@Test
	public void verticalDistanceTest() {
		createVerticalLine();
		double distance = roadLink.distanceFrom(new Node("A", "2", "2"));
		assertEquals(1.0000000, distance, 0.1);
	}

	@Test
	public void commonDistanceTest() {
		createPositiveSlopeLine();
		double distance = roadLink.distanceFrom(new Node("A", "2", "2"));
		assertEquals(Math.sqrt(10), distance, 0.1);
	}

	@Test
	public void horizontalDistanceTest() {
		createHorizontalLine();
		double distance = roadLink.distanceFrom(new Node("A", "2", "2"));
		assertEquals(1.0000000, distance, 0.1);
	}
}
