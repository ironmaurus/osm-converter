package osm.cleaner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.Node;

public class CondensedGraphTest {
	private List<Node> nodes;
	private List<RoadLink> links;
	private Graph graph;

	@Before
	public void setUp() {
		nodes = new ArrayList<>();
		links = new ArrayList<>();
		graph = new Graph(nodes, links);
	}
	
	@Test
	public void repairImpactTrajanMapTest() {
		trajanMap();
		Road road = new Road(graph.createRoadLink("G", "D"));
		CondensedGraph condensed = graph.condense();
		assertEquals(49.0/64.0, condensed.repairImpactOf(road), 0.000000000005);
		graph.addRoadLink("G", "D");
		road = new Road(graph.createRoadLink("G", "B"));
		assertEquals(1.0, condensed.repairImpactOf(road), 0.000000000005);
	}
	
	@Test
	public void repairImpactLongMapTest() {
		longMap();
		Road road = new Road(graph.createRoadLink("E", "A"));
		CondensedGraph condensed = graph.condense();
		assertEquals(1, condensed.repairImpactOf(road), 0.000000000005);
	}
	
	@Test
	public void sumPathMatrixOverflowTest() {
		addNodes(2);
		CondensedGraph condensed = graph.condense();
		short[][] matrix = {{1}};
		List<Component> components = new ArrayList<>();
		List<Node> localNodes = new ArrayList<>();
		for(int i = 0; i < 55390; i++) {
			localNodes.add(new Node());
		}
		components.add(new Component(localNodes));
		condensed.setComponents(components);
		assertEquals(3068052100L, condensed.sumPathMatrix(matrix));
	}

	@Test
	public void simpleMajorHubTest() {
		addNodes(7);
		addCluster("A", "B", "C");
		graph.addRoadLink("A", "D");
		CondensedGraph condensed = graph.condense();
		List<RoadLink> brokenRoads = new ArrayList<>();
		brokenRoads.add(graph.createRoadLink("D", "C"));
		brokenRoads.add(graph.createRoadLink("D", "E"));
		brokenRoads.add(graph.createRoadLink("E", "D"));
		brokenRoads.add(graph.createRoadLink("E", "F"));
		brokenRoads.add(graph.createRoadLink("F", "E"));
		brokenRoads.add(graph.createRoadLink("F", "G"));
		brokenRoads.add(graph.createRoadLink("G", "F"));

		List<Road> repairPriority = condensed.repairPriority(Road
				.toRoads(brokenRoads));

		assertEquals(4, repairPriority.size());
	}

	// TODO: NAILED IT
	@Test
	public void majorHubTest() {
		addNodes(26);
		addCluster("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
				"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V");
		graph.addRoadLink("A", "W");
		CondensedGraph condensed = graph.condense();
		List<RoadLink> brokenRoads = new ArrayList<>();
		brokenRoads.add(graph.createRoadLink("W", "V"));
		brokenRoads.add(graph.createRoadLink("W", "X"));
		brokenRoads.add(graph.createRoadLink("X", "W"));
		brokenRoads.add(graph.createRoadLink("X", "Y"));
		brokenRoads.add(graph.createRoadLink("Y", "X"));
		brokenRoads.add(graph.createRoadLink("Y", "Z"));
		brokenRoads.add(graph.createRoadLink("Z", "Y"));

		List<Road> repairPriority = condensed.repairPriority(Road
				.toRoads(brokenRoads));

		assertEquals(4, repairPriority.size());
	}

	private void addCluster(String... ids) {
		for (int i = 0; i < ids.length - 1; i++) {
			graph.addRoadLink(ids[i], ids[i + 1]);
			graph.addRoadLink(ids[i + 1], ids[i]);
		}
	}

	@Test(expected = ExperimentException.class)
	public void addRoadLinkNullTest() {
		addNodes(2);
		CondensedGraph condensed = graph.condense();
		condensed.addRoadLink(null);
	}

	@Test
	public void sumPathMatrixTest() {
		trajanMap();
		double expectedSum = graph.pathMatrixConnectivity();
		assertEquals(43.0 / 64.0, expectedSum, 0.01);
		CondensedGraph condensed = graph.condense();
		double actualSum = condensed.connectivity();
		assertEquals(expectedSum, actualSum, 0.01);
	}

	@Test
	public void sumPathMatrixRepairTest() {
		trajanMap();
		double expectedSum = graph.pathMatrixConnectivity();
		assertEquals(43.0 / 64.0, expectedSum, 0.01);
		CondensedGraph condensed = graph.condense();
		double actualSum = condensed.connectivity();
		assertEquals(expectedSum, actualSum, 0.01);

		graph.addRoadLink("H", "E");
		expectedSum = graph.pathMatrixConnectivity();
		assertEquals(52.0 / 64.0, expectedSum, 0.01);
		RoadLink temp = graph.createRoadLink("H", "E");
		condensed.addRoadLink(temp);
		actualSum = condensed.connectivity();
		assertEquals(expectedSum, actualSum, 0.01);

		graph.addRoadLink("G", "D");
		expectedSum = graph.pathMatrixConnectivity();
		assertEquals(64.0 / 64.0, expectedSum, 0.01);
		temp = graph.createRoadLink("G", "D");
		condensed.addRoadLink(temp);
		actualSum = condensed.connectivity();
		assertEquals(expectedSum, actualSum, 0.01);
	}

	@Test
	public void repairPriorityTest() {
		trajanMap();
		CondensedGraph condensed = graph.condense();
		List<RoadLink> brokenRoads = new ArrayList<>();
		brokenRoads.add(graph.createRoadLink("A", "C"));
		brokenRoads.add(graph.createRoadLink("D", "F"));
		brokenRoads.add(graph.createRoadLink("G", "D"));
		brokenRoads.add(graph.createRoadLink("H", "E"));

		List<Road> repairPriority = condensed.repairPriority(Road
				.toRoads(brokenRoads));

		assertEquals(2, repairPriority.size());
		assertRoad("H", "E", 1, repairPriority.get(0));
		assertRoad("G", "D", 1, repairPriority.get(1));
	}
	
	private void assertRoad(String from, String to, int ways, Road actual) {
		assertEquals(ways, actual.getRoadLinks().size());
		assertTrue(actual.getRoadLinks().contains(
				graph.createRoadLink(from, to)));
		if (ways == 2) {
			assertTrue(actual.getRoadLinks().contains(
					graph.createRoadLink(to, from)));
		}
	}

	private void trajanMap() {
		addNodes(8);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("B", "E");
		graph.addRoadLink("B", "F");
		graph.addRoadLink("C", "D");
		graph.addRoadLink("C", "G");
		graph.addRoadLink("D", "C");
		graph.addRoadLink("D", "H");
		graph.addRoadLink("E", "A");
		graph.addRoadLink("E", "F");
		graph.addRoadLink("F", "G");
		graph.addRoadLink("G", "F");
		graph.addRoadLink("H", "D");
		graph.addRoadLink("H", "G");
	}
	
	private void longMap() {
		addNodes(5);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("C", "D");
		graph.addRoadLink("D", "E");
	}

	private void addNodes(int num) {
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for (int i = 0; i < num; i++) {
			addNode("" + alphabet.charAt(i));
		}
	}

	private Node addNode(String id) {
		Node node = new Node(id, "0", "0");
		graph.addNode(node);
		return node;
	}

}
