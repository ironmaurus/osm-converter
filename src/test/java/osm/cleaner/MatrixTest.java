package osm.cleaner;

import static org.junit.Assert.*;

import org.junit.Test;

public class MatrixTest {

	@Test
	public void test() {
		int side = 8;
		Matrix matrix = new MySquareMatrix(side);
		int row = 4;
		int column = 3;
		assertEquals(0, matrix.get(row, column));
		int value = 1;
		matrix.set(row, column, value);
		assertEquals(1, matrix.get(row, column));
		assertEquals(8, matrix.getRowsSize());
	}

}
