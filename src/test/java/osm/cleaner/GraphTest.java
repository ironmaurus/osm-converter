package osm.cleaner;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import osm.Node;

public class GraphTest {
	private List<Node> nodes;
	private List<RoadLink> links;
	private Graph graph;
	private Matrix matrix;

	@Before
	public void setUp() {
		nodes = new ArrayList<>();
		links = new ArrayList<>();
		graph = new Graph(nodes, links);
	}
	
	@Test
	public void getDegreesTrivialTest() {
		addNodes(4);
		String degrees = graph.getDegrees();
		assertEquals("A,0,0,0\nB,0,0,0\nC,0,0,0\nD,0,0,0\n", degrees);
	}
	
	@Test
	public void getDegreesSunburst() {
		addNodes(4);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("A", "C");
		graph.addRoadLink("A", "D");
		String degrees = graph.getDegrees();
		assertEquals("A,0,3,3\nB,1,0,1\nC,1,0,1\nD,1,0,1\n", degrees);
	}
	
	@Test
	public void getDegreesCompleteTest() {
		addNodes(4);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "A");
		graph.addRoadLink("A", "C");
		graph.addRoadLink("C", "A");
		graph.addRoadLink("A", "D");
		graph.addRoadLink("D", "A");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("C", "B");
		graph.addRoadLink("B", "D");
		graph.addRoadLink("D", "B");
		graph.addRoadLink("C", "D");
		graph.addRoadLink("D", "C");
		String degrees = graph.getDegrees();
		assertEquals("A,3,3,6\nB,3,3,6\nC,3,3,6\nD,3,3,6\n", degrees);
	}

	@Test
	public void getANDegreeTrivialTest() {
		addNodes(4);
		Map<Node, Integer> degrees = graph.getANDegree();
		assertEquals(4, degrees.keySet().size());
		for (Node n : degrees.keySet()) {
			assertEquals(0, (int) degrees.get(n));
		}
	}

	@Test
	public void getANDegreeCompleteTest() {
		addNodes(4);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "A");
		graph.addRoadLink("A", "C");
		graph.addRoadLink("C", "A");
		graph.addRoadLink("A", "D");
		graph.addRoadLink("D", "A");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("C", "B");
		graph.addRoadLink("B", "D");
		graph.addRoadLink("D", "B");
		graph.addRoadLink("C", "D");
		graph.addRoadLink("D", "C");
		Map<Node, Integer> degrees = graph.getANDegree();
		assertEquals(4, degrees.keySet().size());
		for (Node n : degrees.keySet()) {
			assertEquals(3, (int) degrees.get(n));
		}
	}

	@Test
	public void addRoadTest() {
		addNodes(2);
		assertEquals(0, graph.getRoadLinks().size());
		List<Road> roads = new ArrayList<>();
		Road road = new Road(graph.createRoadLink("A", "B"),
				graph.createRoadLink("B", "A"));
		roads.add(road);
		assertEquals(1, roads.size());
		graph.addRoad(road);
		assertEquals(2, graph.getRoadLinks().size());
		assertTrue(graph.getRoadLinks()
				.contains(graph.createRoadLink("A", "B")));
		assertTrue(graph.getRoadLinks()
				.contains(graph.createRoadLink("B", "A")));
	}

	@Test
	public void removeRoadTest() {
		addNodes(2);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "A");
		List<Road> roads = Road.toRoads(graph.getRoadLinks());
		assertEquals(1, roads.size());
		graph.removeRoad(roads.get(0));
		assertEquals(0, graph.getRoadLinks().size());
	}

	@Test
	public void destroyAllLinksAroundTest() {
		Node center = new Node("center", "14.25", "121");
		List<Node> inRange = new ArrayList<>();
		// 12.13 km away
		Node in1 = new Node("inside", "14.3", "121.1");
		inRange.add(in1);
		// 14.96 km away
		Node in2 = new Node("borderLat", "14.3845", "121");
		inRange.add(in2);
		// 14.98 km away
		Node in3 = new Node("borderLon", "14.25", "121.139");
		inRange.add(in3);

		nodes.add(center);
		nodes.addAll(inRange);
		// 15.09 km
		nodes.add(new Node("outside", "14.32", "121.12"));
		// 15.02 km
		nodes.add(new Node("borderOutLat", "14.3851", "121"));
		// 15.09 km
		nodes.add(new Node("borderOutLon", "14.25", "121.14"));

		Collections.sort(nodes);

		completeGraph(graph);
		BrokenRoadData data = graph.destroyLinksAround(center, 15.0, 1.00);
		assertEquals(6, graph.getRoadLinks().size());
		assertEquals(18, data.getBrokenRoads().size());
		assertEquals(1, data.getTargetThreshold(), 0.001);
		assertEquals(0.734, data.getActualThreshold(), 0.001);
	}

	@Test
	public void destroyLinksAroundTest() {
		Node center = new Node("center", "14.25", "121");
		List<Node> inRange = new ArrayList<>();
		// 12.13 km away
		Node in1 = new Node("inside", "14.3", "121.1");
		inRange.add(in1);
		// 14.96 km away
		Node in2 = new Node("borderLat", "14.3845", "121");
		inRange.add(in2);
		// 14.98 km away
		Node in3 = new Node("borderLon", "14.25", "121.139");
		inRange.add(in3);

		nodes.add(center);
		nodes.addAll(inRange);
		// 15.09 km
		nodes.add(new Node("outside", "14.32", "121.12"));
		// 15.02 km
		nodes.add(new Node("borderOutLat", "14.3851", "121"));
		// 15.09 km
		nodes.add(new Node("borderOutLon", "14.25", "121.14"));

		Collections.sort(nodes);

		completeGraph(graph);
		graph.destroyLinksAround(center, 15.0, 0.10);
		assertTrue(graph.pathMatrixConnectivity() <= 0.90);
	}

	@Test
	public void destroyLinksAroundFailedThresholdTest() {
		Node center = new Node("center", "14.25", "121");
		List<Node> inRange = new ArrayList<>();
		// 12.13 km away
		Node in1 = new Node("inside", "14.3", "121.1");
		inRange.add(in1);
		// 14.96 km away
		Node in2 = new Node("borderLat", "14.3845", "121");
		inRange.add(in2);
		// 14.98 km away
		Node in3 = new Node("borderLon", "14.25", "121.139");
		inRange.add(in3);

		nodes.add(center);
		nodes.addAll(inRange);
		// 15.09 km
		nodes.add(new Node("outside", "14.32", "121.12"));
		// 15.02 km
		nodes.add(new Node("borderOutLat", "14.3851", "121"));
		// 15.09 km
		nodes.add(new Node("borderOutLon", "14.25", "121.14"));

		Collections.sort(nodes);

		completeGraph(graph);
		graph.destroyLinksAround(center, 5.0, 0.10);
		assertTrue(graph.pathMatrixConnectivity() <= 0.90);
	}

	private void completeGraph(Graph graph) {
		List<Node> localNodes = graph.getNodes();
		for (Node n : localNodes) {
			for (Node m : localNodes) {
				if (!n.equals(m)) {
					graph.addRoadLink(new RoadLink(n, m));
				}
			}
		}
	}

	@Test
	public void inRangeLinksTest() {
		Node center = new Node("center", "14.25", "121");
		List<Node> inRange = new ArrayList<>();
		// 12.13 km away
		Node in1 = new Node("inside", "14.3", "121.1");
		inRange.add(in1);
		// 14.96 km away
		Node in2 = new Node("borderLat", "14.3845", "121");
		inRange.add(in2);
		// 14.98 km away
		Node in3 = new Node("borderLon", "14.25", "121.139");
		inRange.add(in3);

		nodes.add(center);
		nodes.addAll(inRange);
		// 15.09 km
		nodes.add(new Node("outside", "14.32", "121.12"));
		// 15.02 km
		nodes.add(new Node("borderOutLat", "14.3851", "121"));
		// 15.09 km
		nodes.add(new Node("borderOutLon", "14.25", "121.14"));

		Collections.sort(nodes);

		graph.addRoadLink("center", "inside");
		graph.addRoadLink("borderLat", "borderOutLat");
		graph.addRoadLink("borderOutLon", "borderLat");
		graph.addRoadLink("borderOutLon", "borderOutLat");

		List<RoadLink> rangeLinks = graph.inRangeLinks(center, 15);

		assertEquals(3, rangeLinks.size());
		assertTrue(rangeLinks
				.contains(graph.createRoadLink("center", "inside")));
		assertTrue(rangeLinks.contains(graph.createRoadLink("borderLat",
				"borderOutLat")));
		assertTrue(rangeLinks.contains(graph.createRoadLink("borderOutLon",
				"borderLat")));
	}

	@Test
	public void inRangeTest() {
		Node center = new Node("center", "14.25", "121");
		List<Node> inRange = new ArrayList<>();
		// 12.13 km away
		Node in1 = new Node("inside", "14.3", "121.1");
		inRange.add(in1);
		// 14.96 km away
		Node in2 = new Node("borderLat", "14.3845", "121");
		inRange.add(in2);
		// 14.98 km away
		Node in3 = new Node("borderLon", "14.25", "121.139");
		inRange.add(in3);

		nodes.add(center);
		nodes.addAll(inRange);
		// 15.09 km
		nodes.add(new Node("outside", "14.32", "121.12"));
		// 15.02 km
		nodes.add(new Node("borderOutLat", "14.3851", "121"));
		// 15.09 km
		nodes.add(new Node("borderOutLon", "14.25", "121.14"));

		List<Node> foundNodes = graph.inRange(center, 15);

		assertEquals(4, foundNodes.size());
		assertTrue(foundNodes.contains(center));
		assertTrue(foundNodes.containsAll(inRange));
	}

	@Test
	public void condenseTest() {
		addNodes(8);
		// Trajan Map
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("B", "E");
		graph.addRoadLink("B", "F");
		graph.addRoadLink("C", "D");
		graph.addRoadLink("C", "G");
		graph.addRoadLink("D", "C");
		graph.addRoadLink("D", "H");
		graph.addRoadLink("E", "A");
		graph.addRoadLink("E", "F");
		graph.addRoadLink("F", "G");
		graph.addRoadLink("G", "F");
		graph.addRoadLink("H", "D");
		graph.addRoadLink("H", "G");
		CondensedGraph condensed = graph.condense();
		assertEquals(3, condensed.getNumNodes());

		List<Node> expected = new ArrayList<>();
		expected.add(new Node("F"));
		expected.add(new Node("G"));
		assertTrue(condensed.getComponents().get(0).containsAll(expected));

		expected.clear();
		expected.add(new Node("C"));
		expected.add(new Node("D"));
		expected.add(new Node("H"));
		assertTrue(condensed.getComponents().get(1).containsAll(expected));

		expected.clear();
		expected.add(new Node("A"));
		expected.add(new Node("B"));
		expected.add(new Node("E"));
		assertTrue(condensed.getComponents().get(2).containsAll(expected));

		assertEquals(5, condensed.getNumEdges());

	}

	@Test
	public void pathMatrixConnectivityTest() {
		addNodes(13);
		graph.addRoadLink("B", "F");
		graph.addRoadLink("C", "I");
		graph.addRoadLink("C", "K");
		graph.addRoadLink("C", "M");
		graph.addRoadLink("D", "B");
		graph.addRoadLink("D", "J");
		graph.addRoadLink("F", "B");
		graph.addRoadLink("G", "C");
		graph.addRoadLink("G", "E");
		graph.addRoadLink("H", "D");
		graph.addRoadLink("I", "L");
		graph.addRoadLink("I", "M");
		graph.addRoadLink("J", "F");
		graph.addRoadLink("K", "G");
		graph.addRoadLink("L", "A");
		graph.addRoadLink("L", "E");
		graph.addRoadLink("M", "A");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },// 1
				{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },// 2
				{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1 },// 8
				{ 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0 },// 4
				{ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },// 1
				{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },// 2
				{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1 },// 8
				{ 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0 },// 5
				{ 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1 },// 5
				{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 },// 3
				{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1 },// 8
				{ 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0 },// 3
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } };// 2
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);
		assertEquals(52.0 / 169.0, graph.pathMatrixConnectivity(), 0.001);

		graph.addRoadLink("A", "H");
		graph.addRoadLink("E", "H");
		graph.addRoadLink("F", "G");

		assertEquals(1.0, graph.pathMatrixConnectivity(), 0.001);
	}

	@Test
	public void getPathMatrixTrivialTest() {
		addNode("A");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);

		addNode("B");
		matrix = graph.getPathMatrix();
		short[][] expected2 = { { 1, 0 }, { 0, 1 } };
		expectedMatrix = new MySquareMatrix(expected2);
		assertEquals(expectedMatrix, matrix);

		addNode("C");
		matrix = graph.getPathMatrix();
		short[][] expected3 = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		expectedMatrix = new MySquareMatrix(expected3);
		assertEquals(expectedMatrix, matrix);
	}

	@Test
	public void getPathMatrixTwoNodesTest() {
		addNodes(2);
		graph.addRoadLink("A", "B");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 1 }, { 0, 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);

		graph.addRoadLink("B", "A");
		matrix = graph.getPathMatrix();
		short[][] expected2 = { { 1, 1 }, { 1, 1 } };
		expectedMatrix = new MySquareMatrix(expected2);
		assertEquals(expectedMatrix, matrix);
	}

	@Test
	public void getPathMatrixTwoJumpTest() {
		addNodes(3);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "C");
		Matrix matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 1, 1 }, { 0, 1, 1 }, { 0, 0, 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);
	}

	@Test
	public void getPathMatrixCycleTest() {
		addNodes(3);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("C", "A");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);
	}

	@Test
	public void getPathMatrixBranchTest() {
		addNodes(4);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("C", "A");
		graph.addRoadLink("B", "D");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 },
				{ 0, 0, 0, 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);

	}

	@Test
	public void getPathMatrixBigTest() {
		addNodes(8);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("A", "E");
		graph.addRoadLink("A", "F");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("B", "E");
		graph.addRoadLink("C", "D");
		graph.addRoadLink("C", "G");
		graph.addRoadLink("E", "G");
		graph.addRoadLink("F", "H");
		graph.addRoadLink("H", "A");
		graph.addRoadLink("H", "D");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 1, 1, 1, 1, 1, 1, 1 },
				{ 0, 1, 1, 1, 1, 0, 1, 0 }, { 0, 0, 1, 1, 0, 0, 1, 0 },
				{ 0, 0, 0, 1, 0, 0, 0, 0 }, { 0, 0, 0, 0, 1, 0, 1, 0 },
				{ 1, 1, 1, 1, 1, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 1, 0 },
				{ 1, 1, 1, 1, 1, 1, 1, 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);
	}

	@Test
	public void getPathMatrixBigWithIslandTest() {
		addNodes(9);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("A", "E");
		graph.addRoadLink("A", "F");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("B", "E");
		graph.addRoadLink("C", "D");
		graph.addRoadLink("C", "G");
		graph.addRoadLink("E", "G");
		graph.addRoadLink("F", "H");
		graph.addRoadLink("H", "A");
		graph.addRoadLink("H", "D");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 1, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 1, 1, 1, 1, 0, 1, 0, 0 }, { 0, 0, 1, 1, 0, 0, 1, 0, 0 },
				{ 0, 0, 0, 1, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 1, 0, 1, 0, 0 },
				{ 1, 1, 1, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
				{ 1, 1, 1, 1, 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);
	}

	@Test
	public void getPathMatrixANotHubTest() {
		addNodes(5);
		graph.addRoadLink("A", "E");
		graph.addRoadLink("B", "D");
		graph.addRoadLink("B", "E");
		graph.addRoadLink("C", "B");
		graph.addRoadLink("D", "A");
		graph.addRoadLink("E", "A");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 0, 0, 0, 1 }, { 1, 1, 0, 1, 1 },
				{ 1, 1, 1, 1, 1 }, { 1, 0, 0, 1, 1 }, { 1, 0, 0, 0, 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);
	}

	@Test
	public void getPathMatrixTwoContinentTest() {
		addNodes(13);
		graph.addRoadLink("B", "F");
		graph.addRoadLink("C", "I");
		graph.addRoadLink("C", "K");
		graph.addRoadLink("C", "M");
		graph.addRoadLink("D", "B");
		graph.addRoadLink("D", "J");
		graph.addRoadLink("F", "B");
		graph.addRoadLink("G", "C");
		graph.addRoadLink("G", "E");
		graph.addRoadLink("H", "D");
		graph.addRoadLink("I", "L");
		graph.addRoadLink("I", "M");
		graph.addRoadLink("J", "F");
		graph.addRoadLink("K", "G");
		graph.addRoadLink("L", "A");
		graph.addRoadLink("L", "E");
		graph.addRoadLink("M", "A");
		matrix = graph.getPathMatrix();
		short[][] expected = { { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
				{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1 },
				{ 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0 },
				{ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
				{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1 },
				{ 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0 },
				{ 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1 },
				{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 },
				{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1 },
				{ 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0 },
				{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 } };
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);
	}

	@Test
	public void getPathMatrixTwoContinentFullConnectTest() {
		addNodes(13);
		graph.addRoadLink("B", "F");
		graph.addRoadLink("C", "I");
		graph.addRoadLink("C", "K");
		graph.addRoadLink("C", "M");
		graph.addRoadLink("D", "B");
		graph.addRoadLink("D", "J");
		graph.addRoadLink("F", "B");
		graph.addRoadLink("G", "C");
		graph.addRoadLink("G", "E");
		graph.addRoadLink("H", "D");
		graph.addRoadLink("I", "L");
		graph.addRoadLink("I", "M");
		graph.addRoadLink("J", "F");
		graph.addRoadLink("K", "G");
		graph.addRoadLink("L", "A");
		graph.addRoadLink("L", "E");
		graph.addRoadLink("M", "A");
		matrix = graph.getPathMatrix();
		// Test for seeing if it updates
		graph.addRoadLink("A", "H");
		graph.addRoadLink("E", "H");
		graph.addRoadLink("F", "G");
		matrix = graph.getPathMatrix();
		short[][] expected = createCompleteMatrix(13);
		Matrix expectedMatrix = new MySquareMatrix(expected);
		assertEquals(expectedMatrix, matrix);
	}

	private short[][] createCompleteMatrix(int size) {
		short[][] matrix = new short[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				matrix[i][j] = 1;
			}
		}
		return matrix;
	}

	// Debug Tool
	protected void printMatrix(short[][] matrix) {
		for (int i = 0; i < matrix[0].length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.print(matrix[i][j] + ",");
			}
			System.out.println();
		}
	}

	@Test
	public void addNodeTest() {
		Node node = addNode("A");
		assertEquals(node, graph.getNodes().get(0));
	}

	@Test
	public void addRoadLinkTest() {
		Node nodeA = addNode("A");
		Node nodeB = addNode("B");
		RoadLink link = new RoadLink(nodeA, nodeB);
		graph.addRoadLink(link);
		assertEquals(link, graph.getRoadLinks().get(0));
	}

	@Test
	public void addRoadLinkShorthandTest() {
		Node nodeA = addNode("A");
		Node nodeB = addNode("B");
		RoadLink link = new RoadLink(nodeA, nodeB);
		graph.addRoadLink("A", "B");
		assertEquals(link, graph.getRoadLinks().get(0));
	}

	@Test
	public void addRoadLinkShorthandFailTest() {
		addNode("A");
		assertFalse(graph.addRoadLink("A", "B"));
	}

	@Test
	public void repairPriorityConnectivityDefnTest() {
		addNodes(2);
		List<RoadLink> brokenRoads = new ArrayList<>();
		RoadLink expected1 = graph.createRoadLink("A", "B");
		brokenRoads.add(expected1);
		RoadLink expected2 = graph.createRoadLink("B", "A");
		brokenRoads.add(expected2);
		graph.setBrokenRoads(brokenRoads);
		List<RoadLink> repairPriority = graph.repairPriority();
		assertEquals(2, repairPriority.size());
		assertEquals(expected1, repairPriority.get(0));
		assertEquals(expected2, repairPriority.get(1));
	}

	@Test
	public void repairPriorityChainingTest() {
		addNodes(6);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("C", "A");
		List<RoadLink> brokenRoads = new ArrayList<>();
		RoadLink expected1 = graph.createRoadLink("A", "D");
		brokenRoads.add(expected1);
		RoadLink expected2 = graph.createRoadLink("D", "B");
		brokenRoads.add(expected2);
		RoadLink expected3 = graph.createRoadLink("D", "E");
		brokenRoads.add(expected3);
		RoadLink expected4 = graph.createRoadLink("E", "D");
		brokenRoads.add(expected4);
		RoadLink expected5 = graph.createRoadLink("E", "F");
		brokenRoads.add(expected5);
		RoadLink expected6 = graph.createRoadLink("F", "E");
		brokenRoads.add(expected6);
		brokenRoads.add(graph.createRoadLink("A", "C"));
		brokenRoads.add(graph.createRoadLink("B", "A"));
		brokenRoads.add(graph.createRoadLink("C", "B"));
		// TODO: Potential Bug
		// Analysis: There seems to be a weakness when it comes to order.
		// brokenRoads.add(graph.createRoadLink("A", "F"));
		graph.setBrokenRoads(brokenRoads);
		List<RoadLink> repairPriority = graph.repairPriority();
		assertEquals(6, repairPriority.size());
		assertEquals(expected1, repairPriority.get(0));
		assertEquals(expected2, repairPriority.get(1));
		assertEquals(expected3, repairPriority.get(2));
		assertEquals(expected4, repairPriority.get(3));
		assertEquals(expected5, repairPriority.get(4));
		assertEquals(expected6, repairPriority.get(5));
	}

	// Add Test for the BIG ONE
	@Test
	public void repairPriorityGeneralTest() {
		hexGraph();
		addNode("G");
		List<RoadLink> brokenRoads = new ArrayList<>();
		brokenRoads.add(graph.createRoadLink("A", "E"));
		brokenRoads.add(graph.createRoadLink("A", "F"));
		brokenRoads.add(graph.createRoadLink("B", "D"));
		brokenRoads.add(graph.createRoadLink("B", "E"));
		brokenRoads.add(graph.createRoadLink("B", "F"));
		brokenRoads.add(graph.createRoadLink("C", "D"));
		brokenRoads.add(graph.createRoadLink("C", "E"));
		RoadLink expected1 = graph.createRoadLink("D", "A");
		brokenRoads.add(expected1);
		brokenRoads.add(graph.createRoadLink("D", "B"));
		brokenRoads.add(graph.createRoadLink("D", "C"));
		brokenRoads.add(graph.createRoadLink("D", "F"));
		brokenRoads.add(graph.createRoadLink("E", "A"));
		brokenRoads.add(graph.createRoadLink("E", "B"));
		brokenRoads.add(graph.createRoadLink("E", "C"));
		RoadLink expected2 = graph.createRoadLink("F", "D");
		brokenRoads.add(expected2);
		brokenRoads.add(graph.createRoadLink("F", "E"));
		RoadLink expected3 = graph.createRoadLink("F", "G");
		brokenRoads.add(expected3);
		RoadLink expected4 = graph.createRoadLink("G", "F");
		brokenRoads.add(expected4);

		graph.setBrokenRoads(brokenRoads);
		List<RoadLink> repairPriority = graph.repairPriority();
		assertEquals(4, repairPriority.size());
		assertEquals(expected1, repairPriority.get(0));
		assertEquals(expected2, repairPriority.get(1));
		assertEquals(expected3, repairPriority.get(2));
		assertEquals(expected4, repairPriority.get(3));
	}

	@Test
	public void repairPrioritySingleRoadTest() {
		hexGraph();
		List<RoadLink> brokenRoads = new ArrayList<>();
		brokenRoads.add(graph.createRoadLink("A", "E"));
		brokenRoads.add(graph.createRoadLink("A", "F"));
		brokenRoads.add(graph.createRoadLink("B", "D"));
		brokenRoads.add(graph.createRoadLink("B", "E"));
		brokenRoads.add(graph.createRoadLink("B", "F"));
		brokenRoads.add(graph.createRoadLink("C", "D"));
		brokenRoads.add(graph.createRoadLink("C", "E"));
		brokenRoads.add(graph.createRoadLink("D", "A"));
		brokenRoads.add(graph.createRoadLink("D", "B"));
		brokenRoads.add(graph.createRoadLink("D", "C"));
		brokenRoads.add(graph.createRoadLink("D", "F"));
		brokenRoads.add(graph.createRoadLink("E", "A"));
		brokenRoads.add(graph.createRoadLink("E", "B"));
		brokenRoads.add(graph.createRoadLink("E", "C"));
		// brokenRoads.add(graph.createRoadLink("F", "A"));
		// brokenRoads.add(graph.createRoadLink("F", "B"));
		brokenRoads.add(graph.createRoadLink("F", "D"));
		brokenRoads.add(graph.createRoadLink("F", "E"));
		RoadLink expected = graph.createRoadLink("F", "C");
		brokenRoads.add(expected);
		graph.setBrokenRoads(brokenRoads);
		List<RoadLink> repairPriority = graph.repairPriority();
		assertEquals(1, repairPriority.size());
		assertTrue(repairPriority.contains(expected));
	}

	@Test
	public void selectBiggestImpactTest() {
		hexGraph();
		List<RoadLink> brokenRoads = new ArrayList<>();
		brokenRoads.add(graph.createRoadLink("A", "E"));
		brokenRoads.add(graph.createRoadLink("A", "F"));
		brokenRoads.add(graph.createRoadLink("B", "D"));
		brokenRoads.add(graph.createRoadLink("B", "E"));
		brokenRoads.add(graph.createRoadLink("B", "F"));
		brokenRoads.add(graph.createRoadLink("C", "D"));
		brokenRoads.add(graph.createRoadLink("C", "E"));
		brokenRoads.add(graph.createRoadLink("D", "A"));
		brokenRoads.add(graph.createRoadLink("D", "B"));
		brokenRoads.add(graph.createRoadLink("D", "C"));
		brokenRoads.add(graph.createRoadLink("D", "F"));
		brokenRoads.add(graph.createRoadLink("E", "A"));
		brokenRoads.add(graph.createRoadLink("E", "B"));
		brokenRoads.add(graph.createRoadLink("E", "C"));
		// brokenRoads.add(graph.createRoadLink("F", "A"));
		// brokenRoads.add(graph.createRoadLink("F", "B"));
		brokenRoads.add(graph.createRoadLink("F", "D"));
		brokenRoads.add(graph.createRoadLink("F", "E"));
		RoadLink expected = graph.createRoadLink("F", "C");
		brokenRoads.add(expected);
		graph.setBrokenRoads(brokenRoads);
		RoadLink biggestImpact = graph.getBiggestImpact();
		assertEquals(expected, biggestImpact);
	}

	@Test
	public void connectivityTest() {
		hexGraph();
		double connectivity = graph.componentConnectivity();
		assertEquals(0.5000, connectivity, 0.00005);
	}

	@Test
	public void fullyConnectedConnectivityTest() {
		hexGraph();
		graph.addRoadLink("F", "A");
		double connectivity = graph.componentConnectivity();
		assertEquals(1.0000, connectivity, 0.00005);
	}

	@Test
	public void findComponentsTest() {
		hexGraph();
		List<Component> components = graph.getComponents();
		assertEquals(3, components.size());
		Collections.sort(components);
		List<Node> clusterF = toList("F");
		assertTrue(components.get(0).containsAll(clusterF));
		List<Node> clusterDE = toList("D", "E");
		assertTrue(components.get(1).containsAll(clusterDE));
		List<Node> clusterABC = toList("A", "B", "C");
		assertTrue(components.get(2).containsAll(clusterABC));
	}

	@Test
	public void findComponentsCacheTest() {
		hexGraph();
		List<Component> components = graph.getComponents();
		assertEquals(3, components.size());
		Collections.sort(components);
		List<Node> clusterF = toList("F");
		assertTrue(components.get(0).containsAll(clusterF));
		List<Node> clusterDE = toList("D", "E");
		assertTrue(components.get(1).containsAll(clusterDE));
		List<Node> clusterABC = toList("A", "B", "C");
		assertTrue(components.get(2).containsAll(clusterABC));

		graph.addRoadLink("F", "A");
		components = graph.getComponents();
		assertEquals(1, components.size());
	}

	private void hexGraph() {
		addNodes(6);
		addCluster("A", "B", "C");
		addCluster("D", "E");
		graph.addRoadLink("A", "D");
		graph.addRoadLink("C", "F");
		graph.addRoadLink("E", "F");
	}

	private void addCluster(String... ids) {
		for (int i = 0; i < ids.length - 1; i++) {
			graph.addRoadLink(ids[i], ids[i + 1]);
			graph.addRoadLink(ids[i + 1], ids[i]);
		}
	}

	private void addNodes(int num) {
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for (int i = 0; i < num; i++) {
			addNode("" + alphabet.charAt(i));
		}
	}

	private Node addNode(String id) {
		Node node = new Node(id, "0", "0");
		graph.addNode(node);
		return node;
	}

	private List<Node> toList(String... nodeIds) {
		List<Node> cluster = new ArrayList<>();
		for (String n : nodeIds) {
			cluster.add(graph.findNode(n));
		}
		return cluster;
	}
}
