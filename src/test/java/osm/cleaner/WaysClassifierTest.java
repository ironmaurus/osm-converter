package osm.cleaner;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.Tag;
import osm.Way;

public class WaysClassifierTest {
	private WaysClassifier classifier;

	@Before
	public void setUp() {
		classifier = new WaysClassifier(new ArrayList<Tag>(),
				new ArrayList<Tag>(), new ArrayList<Tag>());
	}
	
	@Test
	public void isUsedIgnoreTest() {
		List<Tag> usedTags = new ArrayList<>();
		usedTags.add(createTag("highway=*"));
		classifier.setUsedTags(usedTags);

		List<Tag> usedIgnoreTags = new ArrayList<>();
		usedIgnoreTags.add(createTag("highway=footway"));
		classifier.setUsedIgnoreTags(usedIgnoreTags);

		Way way1 = new Way();
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(createTag("bridge=yes"));
		tagList.add(createTag("foot=yes"));
		tagList.add(createTag("highway=footway"));
		tagList.add(createTag("layer=2"));
		tagList.add(createTag("name=footbridge"));
		way1.setTagList(tagList);
		assertFalse(classifier.isUsed(way1));
	}

	@Test
	public void isUsedStarTest() {
		List<Tag> usedTags = new ArrayList<>();
		usedTags.add(createTag("highway=*"));
		classifier.setUsedTags(usedTags);

		Way way1 = new Way();
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("highway", "random"));
		way1.setTagList(tagList);
		assertTrue(classifier.isUsed(way1));
	}

	@Test
	public void createTagTest() {
		Tag tag = classifier.createTag("highway=primary");
		assertEquals("highway", tag.getK());
		assertEquals("primary", tag.getV());
	}

	@Test
	public void createTagStarTest() {
		Tag tag = createTag("highway=*");
		assertEquals("highway", tag.getK());
		assertEquals("*", tag.getV());
	}

	private Tag createTag(String line) {
		return classifier.createTag(line);
	}
}
