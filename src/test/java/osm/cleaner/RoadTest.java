package osm.cleaner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.Node;

public class RoadTest {
	private List<Node> nodes;
	private List<RoadLink> links;
	private Graph graph;

	@Before
	public void setUp() {
		nodes = new ArrayList<>();
		links = new ArrayList<>();
		graph = new Graph(nodes, links);
	}

	@Test
	public void createOneWayRoadTest() {
		addNodes(2);
		Road road = new Road(graph.createRoadLink("A", "B"));
		assertRoad("A", "B", 1, road);
	}

	@Test
	public void createTwoWayRoadTest() {
		addNodes(2);
		Road road = new Road(graph.createRoadLink("A", "B"),
				graph.createRoadLink("B", "A"));
		assertRoad("A", "B", 2, road);
	}

	@Test(expected = RoadException.class)
	public void addRoadLinkWithSameOriginAndDestinationTest() {
		addNodes(2);
		new Road(graph.createRoadLink("A", "B"), graph.createRoadLink("A", "A"));
	}

	@Test(expected = RoadException.class)
	public void addSameRoadLinkAgainTest() {
		addNodes(2);
		new Road(graph.createRoadLink("A", "B"), graph.createRoadLink("A", "B"));
	}

	@Test(expected = RoadException.class)
	public void addUnexpectedRoadLinkTest() {
		addNodes(3);
		new Road(graph.createRoadLink("A", "B"), graph.createRoadLink("C", "A"));
	}

	@Test
	public void getRoadsOneWaySimpleTest() {
		addNodes(2);
		graph.addRoadLink("A", "B");
		List<Road> roads = Road.toRoads(graph.getRoadLinks());
		assertEquals(1, roads.size());
		assertRoad("A", "B", 1, roads.get(0));
	}

	@Test
	public void getRoadsTwoWaySimpleTest() {
		addNodes(2);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "A");
		List<Road> roads = Road.toRoads(graph.getRoadLinks());
		assertEquals(1, roads.size());
		assertRoad("A", "B", 2, roads.get(0));
	}

	@Test
	public void getRoadsTwoWayStraightTest() {
		addNodes(4);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "A");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("C", "B");
		graph.addRoadLink("C", "D");
		graph.addRoadLink("D", "C");
		List<Road> roads = Road.toRoads(graph.getRoadLinks());
		assertEquals(3, roads.size());
		assertRoad("A", "B", 2, roads.get(0));
		assertRoad("B", "C", 2, roads.get(1));
		assertRoad("C", "D", 2, roads.get(2));
	}

	@Test
	public void getRoadsTwoWayStraightWithPartialOneWayTest() {
		addNodes(4);
		graph.addRoadLink("A", "B");
		graph.addRoadLink("B", "A");
		graph.addRoadLink("B", "C");
		graph.addRoadLink("C", "B");
		graph.addRoadLink("C", "D");
		List<Road> roads = Road.toRoads(graph.getRoadLinks());
		assertEquals(3, roads.size());
		assertRoad("A", "B", 2, roads.get(0));
		assertRoad("B", "C", 2, roads.get(1));
		assertRoad("C", "D", 1, roads.get(2));
	}

	@Test
	public void matchPairsTest() {
		addNodes(3);
		RoadLink pair = graph.createRoadLink("B", "A");
		RoadLink notPair = graph.createRoadLink("C", "B");
		List<Road> unpairedRoads = new ArrayList<>();
		unpairedRoads.add(new Road(graph.createRoadLink("A", "B")));
		unpairedRoads.add(new Road(graph.createRoadLink("B", "C")));
		assertEquals(1, Road.indexOfPartner(notPair, unpairedRoads));
		assertEquals(0, Road.indexOfPartner(pair, unpairedRoads));
	}

	@Test
	public void matchPairsSimpleTest() {
		addNodes(3);
		RoadLink pair = graph.createRoadLink("B", "A");
		RoadLink notPair = graph.createRoadLink("C", "B");
		List<Road> unpairedRoads = new ArrayList<>();
		unpairedRoads.add(new Road(graph.createRoadLink("A", "B")));
		assertEquals(-1, Road.indexOfPartner(notPair, unpairedRoads));
		assertEquals(0, Road.indexOfPartner(pair, unpairedRoads));
	}

	@Test
	public void pairTest() {
		addNodes(3);
		RoadLink initial = graph.createRoadLink("A", "B");
		RoadLink pair = graph.createRoadLink("B", "A");
		Road road = new Road(initial);
		assertTrue(road.isPartnerOf(pair));
		RoadLink notPair = graph.createRoadLink("C", "B");
		assertFalse(road.isPartnerOf(notPair));
	}

	private void assertRoad(String from, String to, int ways, Road actual) {
		assertEquals(ways, actual.getRoadLinks().size());
		assertTrue(actual.getRoadLinks().contains(
				graph.createRoadLink(from, to)));
		if (ways == 2) {
			assertTrue(actual.getRoadLinks().contains(
					graph.createRoadLink(to, from)));
		}
	}

	private void addNodes(int num) {
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for (int i = 0; i < num; i++) {
			addNode("" + alphabet.charAt(i));
		}
	}

	private Node addNode(String id) {
		Node node = new Node(id, "0", "0");
		graph.addNode(node);
		return node;
	}
}
