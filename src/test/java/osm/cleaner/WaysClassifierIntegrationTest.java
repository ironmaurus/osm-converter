package osm.cleaner;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.Tag;
import osm.Way;

public class WaysClassifierIntegrationTest {

	private WaysClassifier classifier;

	@Before
	public void setUp() {
		String usedTagsFile = "src/test/resources/osm/cleaner/used.txt";
		String usedIgnoreTagsFile = "src/test/resources/osm/cleaner/usedIgnore.txt";
		String compressTagsFile = "src/test/resources/osm/cleaner/compress.txt";

		classifier = new WaysClassifier(usedTagsFile, usedIgnoreTagsFile,
				compressTagsFile);
	}
	
	@Test
	public void isCompressTest() {
		Way way = new Way();
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("building", "yes"));
		way.setTagList(tagList);
		assertTrue(classifier.isCompress(way));
	}
	
	@Test
	public void isCompressFalseTest() {
		Way way = new Way();
		assertFalse(classifier.isCompress(way));
		
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("building", "no"));
		way.setTagList(tagList);
		assertFalse(classifier.isCompress(way));
	}
	
	@Test
	public void isUsedTest() {
		Way way = new Way();
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("highway", "primary"));
		way.setTagList(tagList);
		assertTrue(classifier.isUsed(way));
	}
	
	@Test
	public void isUsedIgnoreTest() {
		Way way = new Way();
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("highway", "footway"));
		way.setTagList(tagList);
		assertFalse(classifier.isUsed(way));
	}

	@Test
	public void readConfigTest() {

		List<Tag> usedTags = classifier.getUsedTags();
		List<Tag> usedIgnoreTags = classifier.getUsedIgnoreTags();
		List<Tag> compressTags = classifier.getCompressTags();

		assertTrue(usedTags.contains(new Tag("highway", "*")));
		assertEquals(3, usedIgnoreTags.size());
		assertTrue(usedIgnoreTags.contains(new Tag("highway", "footway")));
		assertTrue(usedIgnoreTags.contains(new Tag("highway", "steps")));
		assertTrue(usedIgnoreTags.contains(new Tag("highway", "track")));
		assertTrue(compressTags.contains(new Tag("building", "yes")));
		assertTrue(compressTags
				.contains(new Tag("amenity", "place_of_worship")));
	}

}
