package osm.cleaner;

import static org.junit.Assert.*;

import org.junit.Test;

import osm.Node;

public class AdjacencyEntryTest {

	@Test
	public void addNodeTest() {
		Node node = new Node("A", "1.0", "2.0");
		AdjacencyEntry entry = new AdjacencyEntry(node);
		entry.addNode("B", "0.005", "AB");
		assertEquals(
				"{\"id\":\"A\",\"loc\":\"[2.0,1.0]\",\"name\":\"\",\"adjacent_nodes\":[{\"id\":\"B\",\"distance\":\"0.005\",\"roadlink\":\"AB\"}]}",
				entry.toString());
		entry.addNode("C", "0.002", "AC");
		assertEquals(
				"{\"id\":\"A\",\"loc\":\"[2.0,1.0]\",\"name\":\"\",\"adjacent_nodes\":[{\"id\":\"B\",\"distance\":\"0.005\",\"roadlink\":\"AB\"},{\"id\":\"C\",\"distance\":\"0.002\",\"roadlink\":\"AC\"}]}",
				entry.toString());
	}
}
