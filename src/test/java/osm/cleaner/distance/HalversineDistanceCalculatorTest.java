package osm.cleaner.distance;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import osm.Node;

public class HalversineDistanceCalculatorTest {
	private static final BigDecimal RADIUS = new BigDecimal(6371);
	private DistanceCalculator calc;
	
	@Before
	public void setUp() {
		calc = new HalversineDistanceCalculator(RADIUS);
	}

	@Test
	public void getDistanceTest() {
		Node from = new Node("A", "14.6503180", "121.0520222");
		Node to = new Node("B", "14.6501908", "121.0534853");
		BigDecimal distance = calc.getDistance(from, to);
		BigDecimal expected = new BigDecimal("0.1580");
		assertTrue(expected.compareTo(distance) == 0);
	}

}
