package osm.cleaner;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.Node;

public class AdjacencyListFormatterTest {
	private List<Node> nodes;
	private List<RoadLink> roadLinks;

	@Before
	public void setUp() {
		nodes = new ArrayList<>();
		roadLinks = new ArrayList<>();
	}

	@Test
	public void basicRest() {
		initializeNodesAndRoadLinks();
		String line = AdjacencyListFormatter.formatAdjString(nodes, roadLinks);
		assertEquals("A;B,C\nB;C\nC;A", line);
	}

	@Test
	public void noOutgoingEdgesFirstNodeTest() {
		addNodes("A", "B", "C", "D");
		addRoadLink("BA");
		addRoadLink("BC");
		addRoadLink("CA");
		addRoadLink("CB");
		addRoadLink("DB");
		addRoadLink("DC");

		String line = AdjacencyListFormatter.formatAdjString(nodes, roadLinks);
		assertEquals("A;\nB;A,C\nC;A,B\nD;B,C", line);
	}

	@Test
	public void noOutgoingEdgesMiddleNodeTest() {
		addNodes("A", "B", "C", "D");
		addRoadLink("AC");
		addRoadLink("AD");
		addRoadLink("CB");
		addRoadLink("CD");
		addRoadLink("DB");
		addRoadLink("DC");

		String line = AdjacencyListFormatter.formatAdjString(nodes, roadLinks);
		assertEquals("A;C,D\nB;\nC;B,D\nD;B,C", line);
	}

	@Test
	public void noOutgoingEdgesFinalNodeTest() {
		addNodes("A", "B", "C", "D");
		addRoadLink("AB");
		addRoadLink("AC");
		addRoadLink("BC");
		addRoadLink("BD");
		addRoadLink("CB");
		addRoadLink("CD");

		String line = AdjacencyListFormatter.formatAdjString(nodes, roadLinks);
		assertEquals("A;B,C\nB;C,D\nC;B,D\nD;", line);
	}

	private void initializeNodesAndRoadLinks() {
		addNodes("A", "B", "C");

		addRoadLink("AB");
		addRoadLink("BC");
		addRoadLink("CA");
		addRoadLink("AC");
	}

	private void addNodes(String... ids) {
		for (String id : ids) {
			addNode(id);
		}
	}

	private Node addNode(String nodeId) {
		Node nodeA = new Node(nodeId);
		nodes.add(nodeA);
		return nodeA;
	}

	private RoadLink addRoadLink(String roadName) {
		String nodeFromId = "" + roadName.charAt(0);
		Node nodeFrom = findNode(nodeFromId);
		String nodeToId = "" + roadName.charAt(1);
		Node nodeTo = findNode(nodeToId);
		return addRoadLink(nodeFrom, nodeTo, roadName, BigDecimal.ONE);
	}

	private Node findNode(String nodeFromId) {
		return nodes.get(nodes.indexOf(new Node(nodeFromId)));
	}

	private RoadLink addRoadLink(Node nodeFrom, Node nodeTo, String roadName,
			BigDecimal cost) {
		RoadLink roadLink = new RoadLink(nodeFrom, nodeTo, roadName, cost);
		roadLinks.add(roadLink);
		return roadLink;
	}
}
