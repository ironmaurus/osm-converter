package osm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static osm.TestUtils.*;
import static osm.CommandFactory.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import osm.cleaner.Graph;
import osm.cleaner.RoadLink;

public class OsmMainTest {

	@Test
	public void readExperimentParamsNewPointTest() throws FileNotFoundException {
		ExperimentParams params = OsmMain
				.readExperimentParamsNewPoint(resource("experiment-params-espfnr.in"));
		assertEquals(resource("nodes-trinoma-diamond.txt"),
				params.getNodesFilename());
		assertEquals(resource("roadlinks-trinoma-diamond.txt"),
				params.getRoadlinksFilename());
		assertEquals(target("repair-output-espfnr.csv"),
				params.getRepairOutputFilename());
		assertEquals(target("result-output-espfnr.csv"),
				params.getResultOutputFilename());
		List<CaseParams> caseParams = params.getCaseParams();
		assertEquals(1, caseParams.size());
		CaseParams firstCase = caseParams.get(0);
		assertEquals("1", firstCase.getExperimentId());
		assertEquals("1", firstCase.getRadius());
		assertEquals("1", firstCase.getTargetThreshold());
		assertEquals("14.6525", firstCase.getLat());
		assertEquals("121.0327", firstCase.getLon());
	}

	@Test
	public void selectedNewPointNoRepairFromFileWithOutputTest()
			throws IOException, JAXBException {
		String command = "-espfnr";
		String experimentParams = resource("experiment-params-espfnr.in");
		File resultOutputFile = new File(target("result-output-espfnr.csv"));
		resultOutputFile.delete();
		String[] args = { command, experimentParams };
		OsmMain.main(args);
		assertFileEquals(resource("result-output-espfnr.csv"),
				target("result-output-espfnr.csv"));
	}

	@Test
	public void selectedNewPointNoRepairTest() {
		Graph graph = createTrinomaDiamond();
		String experimentId = "1";
		String radiusString = "1";
		String thresholdString = "1";
		String latString = "14.6525";
		String lonString = "121.0327";
		String actual = OsmMain.selectedNewPointNoRepair(graph.getNodes(),
				graph.getRoadLinks(), experimentId, radiusString,
				thresholdString, latString, lonString);
		// Rounding issues due to double, but not major enough to warrant
		// conversion to BigDecimal.
		String expected = "1.0,1.0,1,NR,4,NR,8.202300000000001,0.31999999999999995\n";
		assertEquals(expected, actual);
	}

	private Graph createTrinomaDiamond() {
		List<Node> nodes = new ArrayList<>();
		// Trinoma
		Node center = new Node("1", "14.6525517", "121.0327874");
		nodes.add(center);
		// ~2km east of Trinoma
		Node east = new Node("2", "14.6525517", "121.052");
		nodes.add(east);
		// ~2km west of Trinoma
		Node west = new Node("3", "14.6525517", "121.014");
		nodes.add(west);
		// ~2km north of Trinoma
		Node north = new Node("4", "14.671", "121.0327874");
		nodes.add(north);
		// ~2km south of Trinoma
		Node south = new Node("5", "14.634", "121.0327874");
		nodes.add(south);
		List<RoadLink> roadLinks = new ArrayList<>();
		roadLinks.add(new RoadLink(center, east));
		roadLinks.add(new RoadLink(east, center));
		roadLinks.add(new RoadLink(center, west));
		roadLinks.add(new RoadLink(west, center));
		roadLinks.add(new RoadLink(center, north));
		roadLinks.add(new RoadLink(north, center));
		roadLinks.add(new RoadLink(center, south));
		roadLinks.add(new RoadLink(south, center));
		roadLinks.add(new RoadLink(west, north));
		roadLinks.add(new RoadLink(north, west));
		roadLinks.add(new RoadLink(west, south));
		roadLinks.add(new RoadLink(south, west));
		roadLinks.add(new RoadLink(east, north));
		roadLinks.add(new RoadLink(north, east));
		roadLinks.add(new RoadLink(east, south));
		roadLinks.add(new RoadLink(south, east));
		Graph graph = new Graph(nodes, roadLinks);
		return graph;
	}

	@Test
	public void selectedNoRepairFromFileTest() throws IOException,
			JAXBException {
		String command = "-esfnr";
		String experimentParams = resource("experiment-params.in");
		File resultOutputFile = new File(target("result-output.csv"));
		resultOutputFile.delete();
		String[] args = { command, experimentParams };
		OsmMain.main(args);
		assertTrue(resultOutputFile.exists());
	}

	@Test
	public void readExperimentParamsTest() throws FileNotFoundException {
		ExperimentParams params = OsmMain
				.readExperimentParams(resource("experiment-params.in"));
		assertEquals(resource("nodes-nb-c-expected.txt"),
				params.getNodesFilename());
		assertEquals(resource("roadlinks-nb-c-expected.txt"),
				params.getRoadlinksFilename());
		assertEquals(target("repair-output.csv"),
				params.getRepairOutputFilename());
		assertEquals(target("result-output.csv"),
				params.getResultOutputFilename());
		List<CaseParams> caseParams = params.getCaseParams();
		assertEquals(1, caseParams.size());
		CaseParams firstCase = caseParams.get(0);
		assertEquals("1", firstCase.getExperimentId());
		assertEquals("1", firstCase.getRadius());
		assertEquals("0.005", firstCase.getTargetThreshold());
		assertEquals("1727286057", firstCase.getCenterId());
	}

	@Test
	public void selectedFromFileTest() throws IOException, JAXBException {
		String command = "-esf";
		String experimentParams = resource("experiment-params.in");
		File repairOutputFile = new File(target("repair-output.csv"));
		repairOutputFile.delete();
		File resultOutputFile = new File(target("result-output.csv"));
		resultOutputFile.delete();
		String[] args = { command, experimentParams };
		OsmMain.main(args);
		assertTrue(repairOutputFile.exists());
		assertTrue(resultOutputFile.exists());
	}

	@Test
	public void selectedTest() throws IOException, JAXBException {
		String command = "-es";
		String nodes = resource("nodes-nb-c-expected.txt");
		String roadlinks = resource("roadlinks-nb-c-expected.txt");
		String radius = "1";
		String threshold = "0.005";
		String experimentId = "1";
		String repairOutput = target("repair-output.csv");
		String resultOutput = target("result-output.csv");
		File repairOutputFile = new File(repairOutput);
		repairOutputFile.delete();
		File resultOutputFile = new File(resultOutput);
		resultOutputFile.delete();
		String centerId = "1727286057";
		String[] args = { command, nodes, roadlinks, repairOutput,
				resultOutput, experimentId, radius, threshold, centerId };
		OsmMain.main(args);
		assertTrue(repairOutputFile.exists());
		assertTrue(resultOutputFile.exists());
	}

	@Test
	public void fractalDimensionTest() throws IOException, JAXBException {
		String roadDetail = "m";
		String command = "-fd";
		String nodes = resource("nodes-nb-" + roadDetail + "-c.csv");
		String maxZoom = "12";
		String output = target("fractal-dimension-" + maxZoom + "-"
				+ roadDetail + ".csv");
		String expectedOutput = resource("fractal-dimension-" + maxZoom + "-"
				+ roadDetail + ".csv");
		String[] args = { command, nodes, output, maxZoom };
		OsmMain.main(args);
		assertFileEquals(expectedOutput, output);
	}

	@Test
	public void enhancedRandomFromFileTest() throws IOException, JAXBException {
		String command = "-erf";
		String nodes = resource("nodes-nb-c-expected.txt");
		String roadLinks = resource("roadlinks-nb-c-expected.txt");
		String inputParams = resource("inputParams.txt");
		String[] params = { command, nodes, roadLinks, inputParams };
		OsmMain.main(params);
	}
	
	@Test
	public void getDegreesTest() throws JAXBException, IOException {
		String command = GET_DEGREES;
		String inputNodesFilename = resource("nodes-02.txt");
		String inputRoadlinksFilename = resource("roadlinks-02.txt");
		String outputDegreesFilename = target("degrees.csv");
		String expectedOutput = resource("degrees-01.csv");
		String[] args = {command, inputNodesFilename,
				inputRoadlinksFilename, outputDegreesFilename};
		OsmMain.main(args);
		assertFileEquals(expectedOutput, outputDegreesFilename);
	}

	@Test
	public void repairedRoadLevelThreeAnalysisTest() throws IOException,
			JAXBException {
		String command = REPAIRED_ROAD_LEVEL_THREE_ANALYSIS;
		String repair = resource("repair.txt");
		String output = target("repair-output-LVL3.txt");
		String expectedOutput = resource("repair-output-LVL3.txt");
		String[] args = { command, repair, output };
		OsmMain.main(args);
		assertFileEquals(expectedOutput, output);
	}

	@Test
	public void repairedRoadGeneralAnalysisTest() throws IOException,
			JAXBException {
		String command = REPAIRED_ROAD_GENERAL_ANALYSIS;
		String repair = resource("repair.txt");
		String output = target("repair-output-G.txt");
		String expectedOutput = resource("repair-output-G.txt");
		String[] args = { command, repair, output };
		OsmMain.main(args);
		assertFileEquals(expectedOutput, output);
	}

	@Test
	public void straightenTest() throws IOException, JAXBException {
		String command = "-str";
		String nodes = resource("nodes-01.txt");
		String roadLinks = resource("roadlinks-01.txt");
		String expectedOutNodes = resource("nodes-01-str.txt");
		String expectedOutRoadLinks = resource("roadlinks-01-str.txt");
		String outNodes = target("nodes-01-str.txt");
		String outRoadLinks = target("roadlinks-01-str.txt");
		String[] args = { command, nodes, roadLinks, outNodes, outRoadLinks };
		OsmMain.main(args);
		assertFileEquals(expectedOutNodes, outNodes);
		assertFileEquals(expectedOutRoadLinks, outRoadLinks);
	}

	@Test
	public void deleteRoadLinksByNameTest() throws IOException {
		String command = "-i";
		String nodes = resource("nodes-01.txt");
		String roadLinks = resource("roadlinks-01.txt");
		String expectedUpdatedRoadLinks = resource("roadlinks-01-1.txt");
		String expectedBrokenRoads = resource("broken-roads-01-1.txt");
		String roadName = "\"Alpha One\"";
		String updatedRoadLinks = target("roadlinks-01-1.txt");
		String brokenRoads = target("broken-roads-01-1.txt");
		String commands = "deleteRoadLinksByName " + roadName + " "
				+ updatedRoadLinks + " " + brokenRoads + "\nexit";
		String[] args = { command, nodes, roadLinks };
		InputStream stream = new ByteArrayInputStream(
				commands.getBytes("UTF-8"));
		OsmMain.interactiveMode(stream, args);
		assertFileEquals(expectedUpdatedRoadLinks, updatedRoadLinks);
		assertFileEquals(expectedBrokenRoads, brokenRoads);
	}

	@Test
	public void findRoadLinksByNameTest() throws IOException {
		String command = "-i";
		String nodes = resource("nodes-01.txt");
		String roadLinks = resource("roadlinks-01.txt");
		String expectedOutFile = resource("found-roadlinks-01.txt");
		String roadName = "\"Alpha One\"";
		String outFile = target("found-roadlinks-01.txt");
		String commands = "findRoadLinksByName " + roadName + " " + outFile
				+ "\nexit";
		String[] args = { command, nodes, roadLinks };
		InputStream stream = new ByteArrayInputStream(
				commands.getBytes("UTF-8"));
		OsmMain.interactiveMode(stream, args);
		assertFileEquals(expectedOutFile, outFile);
	}

	@Test
	public void interactiveTest() throws IOException {
		String command = "-i";
		String nodes = resource("nodes-01.txt");
		String roadLinks = resource("roadlinks-01.txt");
		String[] args = { command, nodes, roadLinks };
		String commands = "status\nexit";
		InputStream stream = new ByteArrayInputStream(
				commands.getBytes("UTF-8"));
		OsmMain.interactiveMode(stream, args);
	}

	@Test
	public void repairTest() throws JAXBException, IOException {
		String command = "-r";
		String nodes = resource("nodes-01.txt");
		String roadLinks = resource("roadlinks-01.txt");
		String brokenRoads = resource("brokenroads-01.txt");
		String expectedRepairPriority = resource("repair-priority-01.txt");
		String repairPriority = target("repair-priority-01.txt");
		String[] args = { command, nodes, roadLinks, brokenRoads,
				repairPriority };
		OsmMain.main(args);
		assertFileEquals(expectedRepairPriority, repairPriority);
	}

	@Test
	public void cleanNoBuildingsConfigFolderTest() throws JAXBException,
			IOException {
		String command = CLEAN_NB;
		String inputOsm = resource("test-osm.osm");
		String expectedNodes = resource("nodes-nb-expected.csv");
		String outputNodes = target("nodes-nb.txt");
		String expectedRoadlinks = resource("roadlinks-nb-expected.csv");
		String outputRoadlinks = target("roadlinks-nb.txt");
		String expectedAdjList = resource("adj-list-nb-expected.csv");
		String outputAdjList = target("adj-list-nb.txt");
		String[] args = { command, inputOsm, outputNodes, outputRoadlinks,
				outputAdjList, CLEANER_CONFIG };
		OsmMain.main(args);
		assertFileEquals(expectedNodes, outputNodes);
		assertFileEquals(expectedRoadlinks, outputRoadlinks);
		assertFileEquals(expectedAdjList, outputAdjList);
	}

	@Test
	public void cleanWithBuildingsConfigFolderTest() throws JAXBException,
			IOException {
		String command = CLEAN_WB;
		String inputOsm = resource("test-osm.osm");
		String expectedNodes = resource("nodes-expected.csv");
		String outputNodes = target("nodes.txt");
		String expectedRoadlinks = resource("roadlinks-expected.csv");
		String outputRoadlinks = target("roadlinks.txt");
		String expectedAdjList = resource("adj-list-expected.csv");
		String outputAdjList = target("adj-list.txt");
		String[] args = { command, inputOsm, outputNodes, outputRoadlinks,
				outputAdjList, CLEANER_CONFIG };
		OsmMain.main(args);
		assertFileEquals(expectedNodes, outputNodes);
		assertFileEquals(expectedRoadlinks, outputRoadlinks);
		assertFileEquals(expectedAdjList, outputAdjList);
	}

	@Test
	public void purgeSmallerComponentsTest() throws JAXBException, IOException {
		String command = "-kp";
		String inputNodes = resource("nodes-nb-expected.csv");
		String inputRoadLinks = resource("roadlinks-nb-expected.csv");
		String outputNodes = target("nodes-nb-c.txt");
		String expectedOutputNodes = resource("nodes-nb-c-expected.txt");
		String outputRoadLinks = target("roadlinks-nb-c.txt");
		String expectedOutputRoadLinks = resource("roadlinks-nb-c-expected.txt");
		String[] args = { command, inputNodes, inputRoadLinks, outputNodes,
				outputRoadLinks };
		OsmMain.main(args);
		assertFileEquals(expectedOutputNodes, outputNodes);
		assertFileEquals(expectedOutputRoadLinks, outputRoadLinks);
	}

	@Test
	public void noBuildingPurgeConfigFolderTest() throws JAXBException,
			IOException {
		String command = "-skp";
		String inputOsm = resource("test-osm.osm");
		String expectedNodes = resource("nodes-nb-expected.csv");
		String outputNodes = target("nodes-nb.txt");
		String expectedRoadLinks = resource("roadlinks-nb-expected.csv");
		String outputRoadLinks = target("roadlinks-nb.txt");
		String expectedAdjList = resource("adj-list-nb-expected.csv");
		String outputAdjList = target("adj-list-nb.txt");
		String outputPurgedNodes = target("nodes-nb-c.txt");
		String expectedOutputPurgedNodes = resource("nodes-nb-c-expected.txt");
		String outputPurgedRoadLinks = target("roadlinks-nb-c.txt");
		String expectedOutputPurgedRoadLinks = resource("roadlinks-nb-c-expected.txt");
		String[] args = { command, inputOsm, outputNodes, outputRoadLinks,
				outputAdjList, outputPurgedNodes, outputPurgedRoadLinks,
				CLEANER_CONFIG };
		OsmMain.main(args);
		assertFileEquals(expectedNodes, outputNodes);
		assertFileEquals(expectedRoadLinks, outputRoadLinks);
		assertFileEquals(expectedAdjList, outputAdjList);
		assertFileEquals(expectedOutputPurgedNodes, outputPurgedNodes);
		assertFileEquals(expectedOutputPurgedRoadLinks, outputPurgedRoadLinks);
	}

	@Test(expected = FileNotFoundException.class)
	public void readRoadLinksFileNotFoundTest() throws FileNotFoundException {
		OsmMain.readRoadLinks(resource("roadlinks-FNF.txt"));
	}

}
