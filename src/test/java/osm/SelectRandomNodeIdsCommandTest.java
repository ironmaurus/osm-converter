package osm;

import static org.junit.Assert.*;
import static osm.TestUtils.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

public class SelectRandomNodeIdsCommandTest {
	@Before
	public void setUp() {
		deleteFileInTarget("nodeIds.csv");
	}

	@Test
	public void simpleTest() {
		String inputNodesFilename = resource("nodes.txt");
		int numToSelect = 5;
		String outputNodeIdsFilename = target("nodeIds.csv");
		SelectRandomNodeIdsCommand command = new SelectRandomNodeIdsCommand(
				inputNodesFilename, numToSelect, outputNodeIdsFilename);
		command.execute();
		File actualOutput = new File(outputNodeIdsFilename);
		assertTrue(actualOutput.exists());
	}

}
