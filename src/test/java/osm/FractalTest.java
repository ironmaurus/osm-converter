package osm;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class FractalTest {
	private Fractal fractal;

	@Before
	public void setUp() {
		fractal = new Fractal();
	}

	@Test
	public void trivialFractalTest() {
		List<Node> nodes = new ArrayList<>();
		nodes.add(new Node("1", "1.0", "1.0"));
		int[][] grid = fractal.fractal(nodes, 0);
		assertEquals(1, grid[0][0]);
	}

	@Test
	public void anotherTrivialFractalTest() {
		List<Node> nodes = new ArrayList<>();
		nodes.add(new Node("1", "1.0", "1.0"));
		nodes.add(new Node("2", "2.0", "2.0"));
		int[][] grid = fractal.fractal(nodes, 0);
		assertEquals(2, grid[0][0]);
	}

	@Test
	public void simpleFractalTest() {
		List<Node> nodes = new ArrayList<>();
		nodes.add(new Node("1", "1.0", "1.0"));
		nodes.add(new Node("2", "2.0", "2.0"));
		int[][] grid = fractal.fractal(nodes, 1);
		// 01
		// 10
		assertGridEquals("0110", grid);
	}

	@Test
	public void getCutTest() {
		double cut = fractal.cut(10.9, 10.8, 2);
		assertEquals(0.05, cut, 0.001);
	}

	@Test
	public void getCutsTest() {
		double[] cuts = fractal.getCuts(10.9, 10.8, 2);
		double[] expected = { 10.8, 10.85, 10.9 };
		assertArrayEquals(expected, cuts, 0.001);
	}

	@Test
	public void fractalTest() {
		List<Node> nodes = new ArrayList<>();
		nodes.add(new Node("1", "1.0", "1.0"));
		nodes.add(new Node("2", "1.0", "2.0"));
		nodes.add(new Node("3", "8.0", "1.0"));
		int[][] grid = fractal.fractal(nodes, 2);
		// 0100
		// 0000
		// 0000
		// 0110
		assertGridEquals("0100000000000110", grid);
		
		grid = fractal.fractal(nodes, 3);
		//00010000
		//00000000
		//00000000
		//00000000
		//00000000
		//00000000
		//00000000
		//00011000
		assertGridEquals("0001000000000000000000000000000000000000000000000000000000011000", grid);
	}
	
	private void assertGridEquals(String expected, int[][] grid) {
		int index = 0;
		for(int i = 0; i < grid.length; i++) {
			for(int j = 0; j < grid[i].length; j++) {
				int expectedValue = Character.getNumericValue(expected.charAt(index));
				assertEquals(expectedValue, grid[i][j]);
				index++;
			}
		}
	}
}
