package osm;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class RandomGeneratorTest {
	private RandomGenerator randomGenerator;

	@Before
	public void setUp() {
		randomGenerator = new RandomGenerator();
	}

	@Test
	public void generateUniqueRandomIndicesTest() {
		int size = 5;
		int numToGenerate = 3;
		List<Integer> uniqueRandomIndices = randomGenerator
				.generateUniqueRandomIndices(size, numToGenerate);
		Set<Integer> actual = new HashSet<>(uniqueRandomIndices);
		assertEquals(numToGenerate, actual.size());
	}

	@Test
	public void selectUniqueRandomTest() {
		List<Integer> values = Arrays.asList(101, 202, 303, 404, 505);
		int numToSelect = 3;
		List<Integer> uniqueRandomValues = randomGenerator
				.selectUniqueRandomValues(values, numToSelect);
		Set<Integer> actual = new HashSet<>(uniqueRandomValues);
		assertEquals(numToSelect, actual.size());
	}
}
