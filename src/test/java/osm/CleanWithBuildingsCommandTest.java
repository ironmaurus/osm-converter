package osm;

import static org.junit.Assert.*;
import static osm.TestUtils.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class CleanWithBuildingsCommandTest {
	@Before
	public void setUp() {
		deleteFileInTarget("nodes.txt");
		deleteFileInTarget("roadlinks.txt");
		deleteFileInTarget("adj-list.txt");
	}

	@Test
	public void simpleTest() throws FileNotFoundException {
		String inputOsm = resource("test-osm.osm");
		String expectedNodes = resource("nodes-expected.csv");
		String outputNodes = target("nodes.txt");
		String expectedRoadlinks = resource("roadlinks-expected.csv");
		String outputRoadlinks = target("roadlinks.txt");
		String expectedAdjList = resource("adj-list-expected.csv");
		String outputAdjList = target("adj-list.txt");

		CleanWithBuildingsCommand command = new CleanWithBuildingsCommand(inputOsm, outputNodes,
				outputRoadlinks, outputAdjList, CLEANER_CONFIG);
		command.execute();

		assertFileEquals(expectedNodes, outputNodes);
		assertFileEquals(expectedRoadlinks, outputRoadlinks);
		assertFileEquals(expectedAdjList, outputAdjList);
	}

	@Test
	public void defaultConfigTest() {
		String inputOsm = resource("test-osm.osm");
		String outputNodes = target("nodes.txt");
		String outputRoadlinks = target("roadlinks.txt");
		String outputAdjList = target("adj-list.txt");
		CleanWithBuildingsCommand command = new CleanWithBuildingsCommand(inputOsm, outputNodes,
				outputRoadlinks, outputAdjList);
		assertEquals("", command.getConfigFolder());
	}
}
