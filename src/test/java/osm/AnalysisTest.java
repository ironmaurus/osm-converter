package osm;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class AnalysisTest {
	private RepairedRoadsAnalyzer analyzer;

	@Before
	public void setUp() {
		analyzer = new RepairedRoadsAnalyzer();
	}

	@Test
	public void printGeneralInfoTest() {
		String test = case1() + case2();
		List<Analysis> analyses = analyzer.analyze(test);
		Analysis first = analyses.get(0);
		assertEquals("0.005;10.0;1;2;1;0.0105", first.printGeneralInfo());
		Analysis second = analyses.get(1);
		assertEquals("0.005;10.0;2;8;8;0.3934", second.printGeneralInfo());
	}

	@Test
	public void printLevelThreeTypeInfoTest() {
		String test = case1() + case2();
		List<Analysis> analyses = analyzer.analyze(test);
		Analysis first = analyses.get(0);
		assertEquals("0.005;10.0;1;0;0;0;0;1;0;0;0;0;0;0.0105;0",
				first.printLevelThreeTypeInfo());
		Analysis second = analyses.get(1);
		assertEquals("0.005;10.0;2;0;0;5;2;0;1;0;0;0.3597;0.0196;0;0.0141",
				second.printLevelThreeTypeInfo());
	}

	private String case1() {
		StringBuilder builder = new StringBuilder();
		builder.append(header("0.005", "10.0", "1"));
		builder.append(
				"32428546;596957912;M.H. Del Pilar Ave.;0.0105;2;PRIMARY")
				.append("\n");
		builder.append(
				"596957912;32428546;M.H. Del Pilar Ave.;0.0105;2;PRIMARY")
				.append("\n");
		builder.append(footer());
		return builder.toString();
	}

	private String case2() {
		StringBuilder builder = new StringBuilder();
		builder.append(header("0.005", "10.0", "2"));
		builder.append("312264226;1074549762;Katipunan Avenue;0.2163;2;TRUNK")
				.append("\n");
		builder.append(
				"2307304052;313463079;Guadalupe Cloverleaf to JP Rizal;0.0103;2;TRUNK_LINK")
				.append("\n");
		builder.append("2466216118;33466357;Commonwealth Avenue;0.0129;6;TRUNK")
				.append("\n");
		builder.append("31462129;31462130;;0.0614;2;TRUNK").append("\n");
		builder.append("2331992410;2331992413;Shaw Tunnel;0.0473;3;TRUNK")
				.append("\n");
		builder.append("503317062;503317146;;0.0141;2;PRIMARY_LINK").append(
				"\n");
		builder.append("1141832074;1141832062;;0.0093;2;TRUNK_LINK").append(
				"\n");
		builder.append(
				"2066765303;224865455;Epifanio de los Santos Avenue;0.0218;3;TRUNK")
				.append("\n");
		builder.append(footer());
		return builder.toString();
	}

	private String header(String threshold, String radius, String id) {
		StringBuilder builder = new StringBuilder();
		builder.append(threshold).append("\n");
		builder.append(radius).append("\n");
		builder.append(id).append("\n");
		builder.append("start;end;name;distance;lanes").append("\n");
		return builder.toString();
	}

	private String footer() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n");
		builder.append("==========").append("\n");
		return builder.toString();
	}

}
