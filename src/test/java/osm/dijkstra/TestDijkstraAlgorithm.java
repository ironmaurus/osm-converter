package osm.dijkstra;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.junit.Ignore;
import org.junit.Test;

public class TestDijkstraAlgorithm {
	private static final String ROADLINKS = "./roadlinks.txt";
	private static final String NODES_TXT = "./nodes.txt";
	private List<Vertex> nodes;
	private List<Edge> edges;

	@Test
	@Ignore
	public void testFromOsmOutput() throws IOException {
		nodes = new ArrayList<Vertex>();

		try (Scanner scanner = new Scanner(new File(NODES_TXT))) {
			scanner.nextLine();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] values = line.split("\t");
				nodes.add(new Vertex(values[0], values[0]));
			}
		}
		edges = new ArrayList<Edge>();
		try (Scanner scanner = new Scanner(new File(ROADLINKS))) {
			scanner.nextLine();
			int counter = 0;
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				int source = nodes.indexOf(new Vertex(values[0], values[0]));
				int target = nodes.indexOf(new Vertex(values[1], values[1]));
				BigDecimal shifted = new BigDecimal(values[3])
						.movePointRight(7);
				addLane("" + counter, source, target, shifted.intValue());
				counter++;
				// addLane("" + counter, target, source, shifted.intValue());
				// counter++;
			}
		}

		Graph graph = new Graph(nodes, edges);
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		// for (int i = 0; i < nodes.size(); i++) {
		// dijkstra.execute(nodes.get(i));
		// for (int j = 0; j < nodes.size(); j++) {
		// if (i != j) {
		// // System.out.printf("From %s to %s: ", nodes.get(i), nodes.get(j));
		// LinkedList<Vertex> path = dijkstra.getPath(nodes.get(j));
		// // assertNotNull(path);
		// // assertTrue(path.size() > 0);
		// if (path != null) {
		// // for (Vertex vertex : path) {
		// // System.out.printf("%s,", vertex);
		// // }
		//
		// } else {
		// System.out.printf("From %s to %s", nodes.get(i), nodes.get(j));
		// System.out.println();
		// }
		// // System.out.println();
		// }
		// }
		// }
		// dijkstra.execute(nodes.get(5155));
		// LinkedList<Vertex> path = dijkstra.getPath(nodes.get(5154));
		//
		// assertNotNull(path);

		dijkstra.execute(nodes.get(5155));
		for (int j = 0; j < nodes.size(); j++) {
			if (5155 != j) {
				// System.out.printf("From %s to %s: ", nodes.get(i),
				// nodes.get(j));
				LinkedList<Vertex> path = dijkstra.getPath(nodes.get(j));
				// assertNotNull(path);
				// assertTrue(path.size() > 0);
				if (path != null) {
					// for (Vertex vertex : path) {
					// System.out.printf("%s,", vertex);
					// }

				} else {
					System.out.printf("From %s to %s", nodes.get(5155),
							nodes.get(j));
					System.out.println();
				}
				// System.out.println();
			}
		}
	}

	private void addLane(String laneId, int sourceLocNo, int destLocNo,
			int duration) {
		Edge lane = new Edge(laneId, nodes.get(sourceLocNo),
				nodes.get(destLocNo), duration);
		edges.add(lane);
	}

}
